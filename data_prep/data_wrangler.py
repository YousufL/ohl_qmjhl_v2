import json
import math
import numpy as np
import pandas as pd
import sys
from data_cleaner import CleanData
from data_prep.fix_distributions import ShotDistributor
sys.path.extend(['data_prep'])
from data_prep.constants import POST1_COORD, POST2_COORD, NET_SIZE, SCORE_STATE_DICT

class WrangleData(CleanData):


    def clean_data(self):
        df, goalie_gp = ShotDistributor(league=self.league, df = self.df, nameplate=self.nameplate, teamplate = self.teamplate)()
        return df, goalie_gp

    @staticmethod
    def get_rebound_angle_change(df):
        '''
        Calculating the change in angle of a rebound from the first to the second shot.
        Calculates angles from normal
        '''

        df['rebound_angle_change'] = 0
        df['rebound_angle_change'] = np.where((df['is_rebound'] == 1)
                                              & (df['y_location'] > 150)
                                              & (df['y_location'].shift(1) > 150)
                                              & (df['shot_angle'] > df['shot_angle'].shift(1)),
                                              90 - (90 - df['shot_angle']) - df['shot_angle'].shift(1),
                                              np.where((df['is_rebound'] == 1)
                                                       & (df['y_location'] > 150)
                                                       & (df['y_location'].shift(1) > 150)
                                                       & (df['shot_angle'] <= df['shot_angle'].shift(1)),
                                                       90 - (90 - df['shot_angle'].shift(1)) - df['shot_angle'],
                                                       np.where((df['is_rebound'] == 1)
                                                                & (df['y_location'] < 150)
                                                                & (df['y_location'].shift(1) < 150)
                                                                & (df['shot_angle'] > df['shot_angle'].shift(1)),
                                                                90 - (90 - df['shot_angle']) - df['shot_angle'].shift(
                                                                    1),
                                                                np.where((df['is_rebound'] == 1)
                                                                         & (df['y_location'] < 150)
                                                                         & (df['y_location'].shift(-1) < 150)
                                                                         & (df['shot_angle'] <= df['shot_angle'].shift(
                                                                    1)),
                                                                         90 - (90 - df['shot_angle'].shift(1)) - df[
                                                                             'shot_angle'],
                                                                         np.where((df['is_rebound'] == 1)
                                                                                  & ~((df['y_location'] < 150) & (
                                                                                     df['y_location'].shift(-1) < 150))
                                                                                  & ~((df['y_location'] > 150) & (
                                                                                 df['y_location'].shift(-1) > 150)),

                                                                                  180 - (90 - df['shot_angle']) - (
                                                                                              90 - df[
                                                                                          'shot_angle'].shift(1)),
                                                                                  df['rebound_angle_change'])))))

        return df

    @staticmethod
    def calculate_angle_net_seen(df):
        '''
        Calculating what the shooter sees when taking a shot. The angle considers the players position
        with regards to the two posts to calculate the angle seen when taking a shot
        '''

        df['angle_net_seen'] = np.nan
        for i, row in df.iterrows():

            aa = ((df['x_location'][i] - 30) ** 2)
            bb = ((df['y_location'][i] - POST1_COORD) ** 2)
            length_a = np.sqrt(aa + bb)

            cc = ((df['x_location'][i] - 30) ** 2)
            dd = ((df['y_location'][i] - POST2_COORD) ** 2)
            length_b = np.sqrt(cc + dd)

            try:
                df['angle_net_seen'][i] = (math.acos(
                    (((NET_SIZE ** 2) - (length_a ** 2) - (length_b ** 2)) / (-2 * length_a * length_b)))) * (
                                                      180 / math.pi)
            except ValueError:
                print(i)
                df['angle_net_seen'][i] = 0

        return df

    @staticmethod
    def calculate_score_state(df):
        '''
        1. Find the unique two teams in each game
        2. calculated the cumulative total goals scored by each team as the game goes on
        3. Calculate the score difference for the team the shooter is on
        convert delta score into categorical variables i.e "up one", "down four" etc.
        '''

        df = df.merge(df.groupby('unique_game_key')['player_team_id'].unique().reset_index().rename(
            columns={'player_team_id': 'unique_team_ids'}),
                      how='left', on='unique_game_key')

        df['team_1'] = df['unique_team_ids'].apply(lambda x: x[0])
        df['team_2'] = df['unique_team_ids'].apply(lambda x: x[1])

        df['score_1'] = df[df.player_team_id == df.team_1].groupby(['unique_game_key', 'player_team_id'])[
            'event'].transform(lambda x: x.cumsum())
        df['score_2'] = df[df.player_team_id == df.team_2].groupby(['unique_game_key', 'player_team_id'])[
            'event'].transform(lambda x: x.cumsum())

        df['score_1'] = df.groupby(['unique_game_key', 'team_1'], sort=False)['score_1'].apply(
            lambda x: x.ffill().bfill(0))
        df['score_2'] = df.groupby(['unique_game_key', 'team_2'], sort=False)['score_2'].apply(
            lambda x: x.ffill().bfill(0))

        df['delta_score'] = np.where((df.player_team_id == df.team_1), df.score_1 - df.score_2, df.score_2 - df.score_1)
        df['delta_score'] = np.where((df.event == 1), df['delta_score'].shift(1), df['delta_score'])

        df['score_state'] = np.nan
        for score, key in SCORE_STATE_DICT.items():
            df['score_state'][df.delta_score == score] = key

        df['score_state'] = np.where(df['delta_score'] >= 4,
                                     "Up_Four",
                                     df['score_state'])
        df['score_state'] = np.where(df['delta_score'] <= -4,
                                     "Down_Four",
                                     df['score_state'])

        return df

    @staticmethod
    def dummify_variables(df):
        '''
        Dummifying categorical variables
        '''

        df = pd.concat([df,pd.get_dummies(df['game_state'], drop_first=True)],axis =1)
        df = pd.concat([df, pd.get_dummies(df['period_id'])], axis=1)
        df = pd.concat([df, pd.get_dummies(df['score_state'])], axis=1)
        df = pd.concat([df, pd.get_dummies(df['player_age_bin'], prefix='player')], axis=1)
        df = pd.concat([df, pd.get_dummies(df['goalie_age_bin'], prefix='goalie')], axis=1)

        df = df.rename(columns = {1:'1.0',2:'2.0',3:'3.0',4:'4.0'})

        return df

    @staticmethod
    def clean_for_sql(df):
        COLS = ['home_roster', 'away_roster', 'home_goalies',
                'away_goalies', 'plus', 'minus', 'goal_scorer',
                'assist1_player', 'assist2_player', 'goalie_out_info',
                'player_penalized_info', 'player', 'goalie', 'player_served']

        for col in COLS:
            print(col)
            df[col] = list(map(lambda x: json.dumps(x), df[col]))

        df['player_age_bin'] = df['player_age_bin'].astype(str)
        df['goalie_age_bin'] = df['goalie_age_bin'].astype(str)
        df['unique_team_ids'] = df['unique_team_ids'].astype(str)

        return df

    def __call__(self):

        df, goalie_gp = self.clean_data()
        df = self.calculate_angle_net_seen(df)
        df = self.get_rebound_angle_change(df)
        df = self.calculate_score_state(df)
        df = self.dummify_variables(df)
        df = self.clean_for_sql(df)

        return df, goalie_gp
