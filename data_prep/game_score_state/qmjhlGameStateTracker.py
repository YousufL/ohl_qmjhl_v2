## The following script records the game state and score state of every shot_type
# Import
import pandas as pd
import numpy as np
from datetime import timedelta
# from tqdm import tqdm

shots = pd.read_csv("../../qmjhl_data/master_qmjhl.csv", encoding='latin-1')

xp=shots.iloc[0:4000]
xp.to_csv('qmjhlSample.csv')
print(shots.head(50))
print(shots.columns)
# Preprocessing
# shots = shots[shots.loc[:, "event"] != "Faceoff"]
# shots = shots[shots.loc[:, "Goal_Type"] != "ERROR"]
# shots = shots[shots.loc[:, 'Goal_Type'] != "Penalty_Shot"]
# shots_2 = shots[shots.loc[:, "shot_quality_description"] != "Game Misconduct"]
# shots_2 = shots_2[shots.loc[:, "shot_quality_description"] != "Misconduct"]
# shots_2 = shots_2[shots.loc[:, "shot_type"] != "Fighting"]
# shots_2 = shots_2[shots.loc[:, "shot_type"] != "Fight-Inst. Upon"]
# shots = shots_2.reset_index().drop(['index', "Unnamed: 0"], axis=1)
# shots.loc[(shots['Goal_Type'] == "EN.SH"), 'Goal_Type'] = 'EN'
# shots.loc[(shots['Goal_Type'] == "EN.PP"), "Goal_Type"] = 'EN'


# My Functions
### These functions leverage 'timedelta' to perform math operations on time
def convert_tts(data, loop_counter):
  time_string = data.loc[loop_counter, "time_formatted"]  # extract time from data
  t = time_string.split(":")
  dt = timedelta(minutes=int(t[0]), seconds=int(t[1]))
  return dt.total_seconds()


def raw_gametime(data, loop_counter):  # Returns cummulative game time in seconds
  # convert period to minute
  p = data.loc[loop_counter, "period"]
  per = timedelta(minutes=int((p - 1) * 20))
  pt = convert_tts(data, loop_counter)
  return per.total_seconds() + pt


####################################################################