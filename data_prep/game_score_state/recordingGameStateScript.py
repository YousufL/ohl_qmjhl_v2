## The following script records the game state and score state of every shot_type
# Import
import pandas as pd
import numpy as np
from datetime import timedelta
from tqdm import tqdm

shots = pd.read_csv("2015-2020_OHLShots-clean.csv")
# Preprocessing
shots = shots[shots.loc[:, "event"] != "Faceoff"]
shots = shots[shots.loc[:, "Goal_Type"] != "ERROR"]
shots = shots[shots.loc[:, 'Goal_Type'] != "Penalty_Shot"]
shots_2 = shots[shots.loc[:, "shot_quality_description"] != "Game Misconduct"]
shots_2 = shots_2[shots.loc[:, "shot_quality_description"] != "Misconduct"]
shots_2 = shots_2[shots.loc[:, "shot_type"] != "Fighting"]
shots_2 = shots_2[shots.loc[:, "shot_type"] != "Fight-Inst. Upon"]
shots = shots_2.reset_index().drop(['index', "Unnamed: 0"], axis=1)
shots.loc[(shots['Goal_Type'] == "EN.SH"), 'Goal_Type'] = 'EN'
shots.loc[(shots['Goal_Type'] == "EN.PP"), "Goal_Type"] = 'EN'


# My Functions
### These functions leverage 'timedelta' to perform math operations on time
def convert_tts(data, loop_counter):
  time_string = data.loc[loop_counter, "time_formatted"]  # extract time from data
  t = time_string.split(":")
  dt = timedelta(minutes=int(t[0]), seconds=int(t[1]))
  return dt.total_seconds()


def raw_gametime(data, loop_counter):  # Returns cummulative game time in seconds
  # convert period to minute
  p = data.loc[loop_counter, "period"]
  per = timedelta(minutes=int((p - 1) * 20))
  pt = convert_tts(data, loop_counter)
  return per.total_seconds() + pt


####################################################################
# Here is where the the logic is contained


N = len(shots)
# N=5700       ### use an integer N for debugging and testing things out.
score_and_state = pd.DataFrame(np.zeros([N, 3]), columns=['game', 'state', 'score'])
i = 0  # for index of event
g = 0  # store game id
for i in tqdm(range(0, N - 1)):
  print(i)
  if shots.loc[i, "game_ID"] != g:
    print("New game")
    g = shots.loc[i, "game_ID"]  # this also takes care of the initialization !
    team0_score = 0
    team1_score = 0
    team0_players = 5
    team1_players = 5
    team0_minor = 0
    team0_minor2 = 0
    team1_minor = 0
    team1_minor2 = 0
    team0_dblminor = 0
    team1_dblminor = 0
    team0_major = 0
    team1_major = 0
    team0_major2 = 0
    team1_major2 = 0
    team0_penalty_offset = 0
    team1_penalty_offset = 0
    team0_offset = 0
    team1_offset = 0
    team0_penalty_time = [0, 0, 0, 0, 0, 0]
    team1_penalty_time = [0, 0, 0, 0, 0, 0]

  score_and_state.loc[i, "game"] = g

  # (1) Assess the game state prior to accounting for the result of the shot
  if (sum(team0_penalty_time) != 0) or (
          sum(team1_penalty_time) != 0):  # this is going to be a long set of statements duplicated for both teams
    if sum(team0_penalty_time) != 0:  # if team 0 has/had a penalty
      if team0_minor != 0:  # if team 0 had/has a minor
        if raw_gametime(shots, i) - team0_minor > 0:  # check if the penalty has expired
          team0_players = team0_players + 1  # if so, add back a player
          team0_minor = 0  # and reset the variable to 0

      if team0_minor2 != 0:
        if raw_gametime(shots, i) - team0_minor2 > 0:
          team0_players = team0_players + 1
          team0_minor2 = 0

      if team0_dblminor != 0:
        if raw_gametime(shots, i) - team0_dblminor > 0:
          team0_players = team0_players + 1
          team0_dblminor = 0

      if team0_penalty_offset != 0:
        if raw_gametime(shots, i) - team0_penalty_offset > 0:
          team0_penalty_offset = 0
          team0_offset = 0

      if team0_major != 0:
        if raw_gametime(shots, i) - team0_major > 0:
          team0_players = team0_players + 1
          team0_major = 0

      if team0_major2 != 0:
        if raw_gametime(shots, i) - team0_major2 > 0:
          team0_players = team0_players + 1
          team0_major2 = 0

    if sum(team1_penalty_time) != 0:  # if team 1 has/had a penalty
      if team1_minor != 0:  # if team 1 had/has a minor
        if raw_gametime(shots, i) - team1_minor > 0:
          team1_players = team1_players + 1
          team1_minor = 0

      if team1_minor2 != 0:
        if raw_gametime(shots, i) - team1_minor2 > 0:
          team1_players = team1_players + 1
          team1_minor2 = 0

      if team1_dblminor != 0:
        if raw_gametime(shots, i) - team1_dblminor > 0:
          team1_players = team1_players + 1
          team1_dblminor = 0

      if team1_penalty_offset != 0:
        if raw_gametime(shots, i) - team1_penalty_offset > 0:
          team1_penalty_offset = 0
          team1_offset = 0

      if team1_major != 0:
        if raw_gametime(shots, i) - team1_major > 0:
          team1_players = team1_players + 1
          team1_major = 0

      if team1_major2 != 0:
        if raw_gametime(shots, i) - team1_major2 > 0:
          team1_players = team1_players + 1
          team1_major2 = 0

  # Update team penalty time dataframes prior to assigning score or state
  print("updating penalties")
  team0_penalty_time = [team0_minor, team0_minor2, team0_dblminor, team0_major, team0_major2, team0_penalty_offset]
  team1_penalty_time = [team1_minor, team1_minor2, team1_dblminor, team1_major, team1_major2, team1_penalty_offset]

  # In this data, there are either shots or penalties - and far more shots than penalties
  if shots.loc[i, "event"] == "shot":  # if we have a shot
    print("we have a shot")
    # (2) Assign the PLAYERS ON ICE outputs based on which team took the shot
    if shots.loc[i, "home"] == 0:
      score_and_state.loc[i, "state"] = str(team0_players) + "v" + str(team1_players)
      score_and_state.loc[i, "score"] = str(team0_score) + " | " + str(team1_score)
    else:
      score_and_state.loc[i, "state"] = str(team1_players) + "v" + str(team0_players)
      score_and_state.loc[i, "score"] = str(team1_score) + " | " + str(team0_score)

    # (3)  What to do if a goal is scored (impacts future states, not that of the even i) *** Prep for next iteration
    # when goals are scored we need to update the score and see if we need to change the game state
    if shots.loc[i, "Goal_Type"] == "NONE":
      print("no goal")
      # continue
    else:  # For all goals
      print("we have a goal")
      time_of_goal = raw_gametime(shots, i)
      if shots.loc[i, "home"] == 0:  # Increment the score
        team0_score = team0_score + 1
      else:
        team1_score = team1_score + 1
      # (4) Determine if game states need to change
      if (((shots.loc[i, "Goal_Type"] == "EV") and (score_and_state.loc[i, "state"] == "5v5")) or (
              (shots.loc[i, "Goal_Type"] == "SH") and (
              score_and_state.loc[i, "state"] == "4v5"))):  # If 5v5  or short handed 5v6,
        print("goal without players returning to the ice")
        # continue
      elif ((shots.loc[i, "Goal_Type"] == "PP") or (
              (shots.loc[i, "Goal_Type"] == "EV") and (score_and_state.loc[i, "state"] == "4v4")) or (
                    (shots.loc[i, "Goal_Type"] == "SH") and (score_and_state.loc[i, "state"] != "4v5"))):
        if shots.loc[i, "home"] == 0:  ##### ( I ) : HOME TEAM PP GOAL
          if team1_minor != 0:  # (A) check if there was a first minor
            (q, r) = divmod((team1_minor - time_of_goal), 2 * 60)
            if ((sum(team1_penalty_time) - team1_minor) == 0):  # if there was only a team1_minor
              if int(q) == 1:  # this covers if it was a double minor
                team1_minor = time_of_goal + 2 * 60
              elif int(q) == 0:
                team1_players = team1_players + 1
                team1_minor = 0
              else:
                break
            elif team1_minor2 != 0:
              if team1_minor < team1_minor2:
                if int(q) == 1:
                  team1_minor = time_of_goal + 2 * 60
                elif int(q) == 0:
                  team1_players = team1_players + 1
                  team1_minor = 0
                else:
                  break
              else:
                (q, r) = divmod((team1_minor2 - time_of_goal), 2 * 60)
                if int(q) == 1:
                  team1_minor2 = time_of_goal + 2 * 60
                elif int(q) == 0:
                  team1_players = team1_players + 1
                  team1_minor2 = 0
                else:
                  break
            elif team1_dblminor != 0:
              if team1_minor < team1_dblminor:
                if int(q) == 1:
                  team1_minor = time_of_goal + 2 * 60
                elif int(q) == 0:
                  team1_players = team1_players + 1
                  team1_minor = 0
                else:
                  break
              else:
                (q, r) = divmod((team1_dblminor - time_of_goal), 2 * 60)
                if int(q) == 1:
                  team1_dblminor = time_of_goal + 2 * 60  ## Reduce the double minor to 2 minutes
                elif int(q) == 0:
                  team1_dblminor = 0
                  team1_players = team1_players + 1
                else:
                  continue
            else:  # If the other penalty is a major, minor player returns.
              team1_minor = 0
              team1_players = team1_players + 1

          # (B) check if there was a second minor
          elif team1_minor2 != 0:
            (q, r) = divmod((team1_minor2 - time_of_goal), 2 * 60)
            if (sum(team1_penalty_time) - team1_minor2 == 0):  # if there was only a minor2
              if int(q) == 1:
                team1_minor2 = time_of_goal + 2 * 60
              elif int(q) == 0:
                team1_players = team1_players + 1
                team1_minor2 = 0
              else:
                break
            elif team1_dblminor != 0:
              if team1_minor2 < team1_dblminor:
                if int(q) == 1:
                  team1_minor2 = time_of_goal + 2 * 60
                elif int(q) == 0:
                  team1_players = team1_players + 1
                  team1_minor2 = 0
                else:
                  break
              else:
                (q, r) = divmod((team1_dblminor - time_of_goal), 2 * 60)
                if int(q) == 1:
                  team1_dblminor = time_of_goal + 2 * 60
                elif int(q) == 0:
                  team1_players = team1_players + 1
                  team1_dblminor = 0
                else:
                  break
            else:
              team1_minor2 = 0
              team1_players = team1_players + 1
          # (C) check if there was a double minor
          elif team1_dblminor != 0:
            (q, r) = divmod((team1_dblminor - time_of_goal), 2 * 60)
            if int(q) == 1:
              team1_dblminor = time_of_goal + 2 * 60
            elif int(q) == 0:
              team1_players = team1_players + 1
              team1_dblminor = 0
            else:
              break
          # (D) check if there was a major
          else:
            continue  # Player(s) on a major do not leave the box if a goal is scored

        elif shots.loc[i, "home"] == 1:  #####( II ):
          # (A) check if there was a first minor
          if team0_minor != 0:
            (q, r) = divmod((team0_minor - time_of_goal), 2 * 60)
            if ((sum(team0_penalty_time) - team0_minor) == 0):  # if there was only a minor
              if int(q) == 1:
                team0_minor = time_of_goal + 2 * 60
              elif int(q) == 0:
                team0_players = team0_players + 1
                team0_minor = 0
              else:
                break
            elif team0_minor2 != 0:
              if team0_minor < team0_minor2:
                if int(q) == 1:
                  team0_minor = time_of_goal + 2 * 60
                elif int(q) == 0:
                  team0_players = team0_players + 1
                  team0_minor = 0
                else:
                  break
              else:
                (q, r) = divmod((team0_minor2 - time_of_goal), 2 * 60)
                if int(q) == 1:
                  team0_minor2 = time_of_goal + 2 * 60
                elif int(q) == 0:
                  team0_players = team0_players + 1
                  team0_minor2 = 0
                else:
                  break
            elif team0_dblminor != 0:
              if team0_minor < team0_dblminor:
                if int(q) == 1:
                  team0_minor = time_of_goal + 2 * 60
                elif int(q) == 0:
                  team0_players = team0_players + 1
                  team0_minor = 0
                else:
                  break
              else:
                (q, r) = divmod((team0_dblminor - time_of_goal), 2 * 60)
                if int(q) == 1:
                  team0_dblminor = time_of_goal + 2 * 60  ## Reduce the double minor to 2 minutes
                elif int(q) == 0:
                  team0_dblminor = 0
                  team0_players = team0_players + 1
                else:
                  continue
            ## If its a major, players dont return
            else:
              team0_minor = 0
              team0_players = team0_players + 1
          # (B) check if there was a second minor
          elif team0_minor2 != 0:
            (q, r) = divmod((team0_minor2 - time_of_goal), 2 * 60)
            if (sum(team0_penalty_time) - team0_minor2 == 0):  # if there was only a minor2
              if int(q) == 1:
                team0_minor2 = time_of_goal + 2 * 60
              elif int(q) == 0:
                team0_players = team0_players + 1
                team0_minor2 = 0
              else:
                break
            elif team0_dblminor != 0:
              if team0_minor2 < team0_dblminor:
                if int(q) == 1:
                  team0_minor2 = time_of_goal + 2 * 60
                elif int(q) == 0:
                  team0_players = team0_players + 1
                  team0_minor2 = 0
                else:
                  break
              else:
                (q, r) = divmod((team0_dblminor - time_of_goal), 2 * 60)
                if int(q) == 1:
                  team0_dblminor = time_of_goal + 2 * 60
                elif int(q) == 0:
                  team0_players = team0_players + 1
                  team0_dblminor = 0
                else:
                  break
            else:
              team0_minor2 = 0
              team0_players = team0_players + 1
          # (C) check if there was a double minor
          elif team0_dblminor != 0:
            (q, r) = divmod((team0_dblminor - time_of_goal), 2 * 60)
            if int(q) == 1:
              team0_dblminor = time_of_goal + 2 * 60
            elif int(q) == 0:
              team0_players = team0_players + 1
              team0_dblminor = 0
            else:
              break
          # (D) check if there was a major
          else:
            continue  # Player(s) on a major do not leave the box if a goal is scored
        else:
          print("Error with PP on the iteration : " + str(i))
          break

      elif shots.loc[i, "Goal_Type"] == "EN":  # add a player to the other team if an empty net goal is scored
        if shots.loc[i, "home"] == 0:
          score_and_state.loc[i, "state"] = str(team0_players) + "v" + str(team1_players + 1)
        else:
          score_and_state.loc[i, "state"] = str(team1_players) + "v" + str(team0_players + 1)
      # -- just making sure the inputs are as expected
      else:
        print("Error with Goal_Type on iteration: " + str(i))
        continue

      # Update the team penalty Dataframes after a PP goal is scored
      team0_penalty_time = [team0_minor, team0_minor2, team0_dblminor, team0_major, team0_major2, team0_penalty_offset]
      team1_penalty_time = [team1_minor, team1_minor2, team1_dblminor, team1_major, team1_major2, team1_penalty_offset]

  elif shots.loc[i, "event"] == "Penalty":
    print("we have a penalty")
    if shots.loc[i, "home"] == 0:  # for team 0
      score_and_state.loc[i, "state"] = str(team0_players) + "v" + str(
        team1_players)  # Asigning the score and state first
      score_and_state.loc[i, "score"] = str(team0_score) + " | " + str(team1_score)
      if shots.loc[i, "shot_quality_description"] == "Minor":
        if (((shots.loc[i + 1, "shot_quality_description"] == "Minor") and (
                raw_gametime(shots, i + 1) == raw_gametime(shots, i))) or (
                (shots.loc[i - 1, "shot_quality_description"] == "Minor") and (
                raw_gametime(shots, i - 1) == raw_gametime(shots, i)))):
          if ((shots.loc[i + 1, "shot_quality_description"] == shots.loc[i, "shot_quality_description"]) and (
                  raw_gametime(shots, i + 1) == raw_gametime(shots, i))):  # if the first minor
            if shots.loc[i + 1, "home"] == shots.loc[i, "home"]:  ## If its the first of the double minor
              team0_players = team0_players - 1
              if team0_minor == 0:
                team0_minor = raw_gametime(shots, i) + 2 * 60
              else:
                team0_minor2 = raw_gametime(shots, i) + 2 * 60
            else:  ## If its not a double minor
              ### Increase counter
              team0_offset = team0_offset + 1
              if team0_offset <= 1:  # its either an offsetting minor or an additonal minor
                team0_penalty_offset = (raw_gametime(shots, i) + 2 * 60)
              else:
                team0_players = team0_players - 1
                if team0_minor == 0:
                  team0_minor = raw_gametime(shots, i) + 2 * 60
                elif team0_minor2 == 0:
                  team0_minor2 = raw_gametime(shots, i) + 2 * 60
                else:
                  continue

          elif (shots.loc[i - 1, "shot_quality_description"] == shots.loc[i, "shot_quality_description"]) and (
                  raw_gametime(shots, i - 1) == raw_gametime(shots, i)):  # IF ITS THE SECOND MINOR
            if shots.loc[i - 1, "home"] == shots.loc[
              i, "home"]:  # do not remove a player if its the second of a double minor, he's already been removed
              if team0_minor == raw_gametime(shots, i - 1):
                team0_minor = team0_minor + 2 * 60
              elif team0_minor2 == raw_gametime(shots, i - 1):
                team0_minor2 = team0_minor2 + 2 * 60
              else:
                continue
            else:  ## If its not a double minor
              ### Increase counter
              team0_offset = team0_offset + 1
              if team0_offset <= 1:  # its either an offsetting minor or an additonal minor
                team0_penalty_offset = (raw_gametime(shots, i) + 2 * 60)
              else:
                team0_players = team0_players - 1
                if team0_minor == 0:
                  team0_minor = (raw_gametime(shots, i) + 2 * 60)
                elif team0_minor2 == 0:
                  team0_minor2 = (raw_gametime(shots, i) + 2 * 60)
                else:
                  continue
          else:
            continue

        else:
          team0_players = team0_players - 1  # if a minor, take off a player
          team0_minor_ref = raw_gametime(shots, i) + 2 * 60  # keep track of the time
          if team0_minor == 0:
            team0_minor = team0_minor_ref  # this will store the time at which the penalty would expire
          else:
            team0_minor2 = team0_minor_ref  # ditto

      elif shots.loc[i, "shot_quality_description"] == "Double Minor":
        team0_players = team0_players - 1
        team0_dblminor = raw_gametime(shots, i) + 4 * 60

      elif ((shots.loc[i, "shot_quality_description"] == "Major") or (
              shots.loc[i, "shot_quality_description"] == "Match")):
        if shots.loc[i, "shot_type"] != "Fighting":
          team0_players = team0_players - 1
          if ((raw_gametime(shots, i) > (60 - 5) * 60) and (((raw_gametime(shots, i) == raw_gametime(shots,
                                                                                                     i + 1)) and (
                                                                     shots.loc[i + 1, "event"] == "Penalty")) or ((
                                                                                                                          raw_gametime(
                                                                                                                                  shots,
                                                                                                                                  i) == raw_gametime(
                                                                                                                          shots,
                                                                                                                          i - 1)) and (
                                                                                                                          shots.loc[
                                                                                                                            i - 1, "event"] == "Penalty")))):
            if (raw_gametime(shots, i) == raw_gametime(shots, i + 1)) and (shots.loc[i + 1, "event"] == "Penalty"):
              if shots.loc[i + 1, "shot_quality_description"] != "Major":
                if shots.loc[i + 1, "shot_quality_description"] == "Minor":
                  team0_major_ref = raw_gametime(shots, i) + (5 - 2) * 60
                elif shots.loc[i + 1, "shot_quality_description"] == "Double Minor":
                  team0_major_ref = raw_gametime(shots, i) + (5 - 4) * 60
                else:
                  ## if something happens here we are FUCKED
                  print('if something happens here we are FUCKED')
                  break

            else:
              if shots.loc[i - 1, "shot_quality_description"] != "Major":
                if shots.loc[i - 1, "shot_quality_description"] == "Minor":
                  team0_major_ref = raw_gametime(shots, i) + (3) * 60
                elif shots.loc[i - 1, "shot_quality_description"] == "Double Minor":
                  team0_major_ref = raw_gametime(shots, i) + (5 - 4) * 60
                else:
                  ## if something happens here we are FUCKED
                  print('if something happens here we are FUCKED')
                  break

          else:
            team0_major_ref = raw_gametime(shots, i) + 5 * 60

          if team0_major == 0:
            team0_major = team0_major_ref  # this will store the time at which the penalty would expire
          else:
            team0_major2 = team0_major_ref  # ditto, in case there is already a major penalty

        else:
          print("we have a fighting penalty")
          break
      else:
        continue  # all other recorded penalty events do not have a time element associated with it (ex: misconduct for instigating)

      team0_penalty_time = [team0_minor, team0_minor2, team0_dblminor, team0_major, team0_major2, team0_penalty_offset]

    elif shots.loc[i, "home"] == 1:  # repeat for team 1
      score_and_state.loc[i, "state"] = str(team1_players) + "v" + str(team0_players)
      score_and_state.loc[i, "score"] = str(team1_score) + " | " + str(team0_score)
      if shots.loc[i, "shot_quality_description"] == "Minor":
        print("Offset??")
        if (((shots.loc[i + 1, "shot_quality_description"] == shots.loc[i, "shot_quality_description"]) and (
                raw_gametime(shots, i + 1) == raw_gametime(shots, i))) or (
                (shots.loc[i - 1, "shot_quality_description"] == shots.loc[i, "shot_quality_description"]) and (
                raw_gametime(shots, i - 1) == raw_gametime(shots, i)))):
          if ((shots.loc[i + 1, "shot_quality_description"] == shots.loc[i, "shot_quality_description"]) and (
                  raw_gametime(shots, i + 1) == raw_gametime(shots, i))):  # if the first minor
            if (shots.loc[i + 1, "home"] == shots.loc[i, "home"]):
              team1_players = team1_players - 1
              if team1_minor == 0:
                team1_minor = raw_gametime(shots, i) + 2 * 60
              else:
                team1_minor2 = raw_gametime(shots, i) + 2 * 60
            else:  ## If its not a double minor
              ### Increase counter
              team1_offset = team1_offset + 1
              if team1_offset <= 1:  # its either an offsetting minor or an additonal minor
                team1_penalty_offset = (raw_gametime(shots, i) + 2 * 60)
              else:
                team1_players = team1_players - 1
                if team1_minor == 0:
                  team1_minor = (raw_gametime(shots, i) + 2 * 60)
                elif team1_minor2 == 0:
                  team1_minor2 = (raw_gametime(shots, i) + 2 * 60)
                else:
                  continue

          elif (shots.loc[i - 1, "shot_quality_description"] == shots.loc[i, "shot_quality_description"]) and (
                  raw_gametime(shots, i - 1) == raw_gametime(shots, i)):  # IF ITS THE SECOND MINOR
            if shots.loc[i - 1, "home"] == shots.loc[
              i, "home"]:  # do not remove a player if its the second of a double minor, he's already been removed
              if team1_minor == raw_gametime(shots, i - 1):
                team1_minor = team1_minor + 2 * 60
              elif team1_minor2 == raw_gametime(shots, i - 1):
                team1_minor2 = team1_minor2 + 2 * 60
              else:
                continue
            else:  ## If its not a double minor
              ### Increase counter
              team1_offset = team1_offset + 1
              if team1_offset <= 1:  # its either an offsetting minor or an additonal minor
                team1_penalty_offset = (raw_gametime(shots, i) + 2 * 60)
              else:
                team1_players = team1_players - 1
                if team1_minor == 0:
                  team1_minor = (raw_gametime(shots, i) + 2 * 60)
                elif team1_minor2 == 0:
                  team1_minor2 = (raw_gametime(shots, i) + 2 * 60)
                else:
                  continue
          else:
            continue
        else:
          team1_players = team1_players - 1  # if a minor, take off a player
          team1_minor_ref = raw_gametime(shots, i) + 2 * 60  # keep track of the time
          if team1_minor == 0:
            team1_minor = team1_minor_ref  # this will store the time at which the penalty would expire
          else:
            team1_minor2 = team1_minor_ref  # ditto

      elif shots.loc[i, "shot_quality_description"] == "Double Minor":
        team1_players = team1_players - 1
        team1_dblminor = raw_gametime(shots, i) + 4 * 60

      elif ((shots.loc[i, "shot_quality_description"] == "Major") or (
              shots.loc[i, "shot_quality_description"] == "Match")):
        if shots.loc[i, "shot_type"] != "Fighting":
          team1_players = team1_players - 1
          if ((raw_gametime(shots, i) > (60 - 5) * 60) and (((raw_gametime(shots, i) == raw_gametime(shots,
                                                                                                     i + 1)) and (
                                                                     shots.loc[i + 1, "event"] == "Penalty")) or ((
                                                                                                                          raw_gametime(
                                                                                                                                  shots,
                                                                                                                                  i) == raw_gametime(
                                                                                                                          shots,
                                                                                                                          i - 1)) and (
                                                                                                                          shots.loc[
                                                                                                                            i - 1, "event"] == "Penalty")))):
            print("its about to get interesting")
            if (raw_gametime(shots, i) == raw_gametime(shots, i + 1)) and (shots.loc[i + 1, "event"] == "Penalty"):
              if shots.loc[i + 1, "shot_quality_description"] != "Major":
                if shots.loc[i + 1, "shot_quality_description"] == "Minor":
                  print("three min major")
                  team1_major_ref = raw_gametime(shots, i) + (3) * 60
                elif shots.loc[i + 1, "shot_quality_description"] == "Double Minor":
                  team1_major_ref = raw_gametime(shots, i) + (5 - 4) * 60
                else:
                  ## if something happens here we are FUCKED
                  print('if something happens here we are FUCKED')
                  break
            else:
              if shots.loc[i - 1, "shot_quality_description"] != "Major":
                if shots.loc[i - 1, "shot_quality_description"] == "Minor":
                  team1_major_ref = raw_gametime(shots, i) + (5 - 2) * 60
                elif shots.loc[i - 1, "shot_quality_description"] == "Double Minor":
                  team1_major_ref = raw_gametime(shots, i) + (5 - 4) * 60
                else:
                  ## if something happens here we are FUCKED
                  print('if something happens here we are FUCKED')
                  break
          else:
            team1_major_ref = raw_gametime(shots, i) + 5 * 60

          if team1_major == 0:
            team1_major = team1_major_ref  # this will store the time at which the penalty would expire
          else:
            team1_major2 = team1_major_ref  # ditto, in case there is already a major penalty

        else:
          print("we have a fighting penalty")
          break
      else:
        continue  # all other recorded penalty events do not have a time element associated with it (ex: misconduct for instigating)

      team1_penalty_time = [team1_minor, team1_minor2, team1_dblminor, team1_major, team1_major2, team1_penalty_offset]

    else:
      print("Houston, we have a problem with the penalty on iteration: " + str(i))

  else:
    print("Error at the end of the loop on iteration: " + str(i))
    break

sass = pd.concat([shots, score_and_state], axis=1)
sass.to_csv('sassFinalV1.csv')
print('DONE')
