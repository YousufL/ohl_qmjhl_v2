import numpy as np
import math
from constants import *
import pandas as pd
from data_prep.constants import BINS, FIRST, MONTH_DICT

class CleanData:
    def __init__(self, league, df, nameplate, teamplate):
        self.league = league
        self.df = df
        self.nameplate = nameplate
        self.teamplate = teamplate

    def load_in_data(self):
        '''
        Loads in data specific to each league.
        '''
        if self.league == 'ohl':

            df = self.df
            nameplate = self.nameplate
            teamplate = self.teamplate

        if self.league == 'qmjhl':

            df = self.df
            nameplate = self.nameplate
            teamplate = self.teamplate

        return df, nameplate, teamplate

    @staticmethod
    def calculate_total_game_seconds(df):
        '''
        Calculates cumulative game seconds irrespective of period of play
        '''

        df['s_period'] = df['s']
        df['s'] = np.where(df['period_id'] == 2,
                           df['s'] + 1200,
                           np.where(df['period_id'] == 3,
                                    df['s'] + 2400,
                                    np.where(df['period_id'] == 4, df['s'] + 3600, df['s'])))
        return df

    @staticmethod
    def drop_duplicate_events(df):
        '''
        Some events are recorded twice, so first creating a unique key based on total game seconds, game and event
        type, and then dropping duplicates of that
        '''

        df['unique_event_key'] = df['unique_game_key'].astype(str) + df['s'].astype(str) + df['event'].astype(str) + df['game_id'].astype(str) + df['period_id'].astype(str) + df['season_name'].astype(str) + df['player_id'].astype(str)
        df = df.drop_duplicates(subset=['unique_event_key'], keep='last').reset_index().drop(columns=['index'])
        df = df.reset_index(drop=True)
        return df

    @staticmethod
    def subset_goalie_gp_data(df):
        '''
        Now we dont need to refer to any other df's or events, so subsetting the data to only have shots and goals
        as events.
        '''
        goalie_gp = df[(df.event == 'goalie_change')][['event', 'goalie_in_id', 'season_name', 'team_id', 'game_id']]
        goalie_gp = goalie_gp.reset_index().drop(columns=['index'])

        return goalie_gp

    @staticmethod
    def subset_data(df):
        '''
        Now we dont need to refer to any other df's or events, so subsetting the data to only have shots and goals
        as events.
        '''
        df = df[(df.event == 'shot') | (df.event == 'goal')]
        df = df.reset_index().drop(columns=['index'])

        return df

    @staticmethod
    def ffill_goal_rows(df):
        '''
        For some reason when a goal is scored it is first recorded as a shot and then as a goal...and the row
        that indicates its a goal has all NA's for player_id etc, but they are recorded in the shot from before...
        so we ffill these values to repalce the NA's
        '''

        df['remove_shot'] = 0
        df['remove_shot'] = np.where(
            ((df['event'] == 'shot') & (  ((df['event'].shift(-1) == 'goal'))  & (df['s'] == df['s'].shift(-1)) & (df['period_id'] == df['period_id'].shift(-1)) )), 1, df['remove_shot'])

        df['player_id'] = df['player_id'].ffill(limit=1)
        df['goalie_id'] = df['goalie_id'].ffill(limit=1)
        df['goalie_team_code'] = df['goalie_team_code'].ffill(limit=1)
        df['player_team_id'] = df['player_team_id'].ffill(limit=1)
        df['player_team_code'] = df['player_team_code'].ffill(limit=1)

        df = df[df['remove_shot'] != 1]
        df = df.reset_index(drop=True)

        return df

    @staticmethod
    def track_rebounds_and_rebound_assists(df):
        '''
        Function that tracks rebounds and rebound assist. If  a shot is b the same team
        within 3 seconds of the previous shot it is defines as a rebound. For some reason the
        ration of shots/rebounds is much lower in the qmjhl
        # todo compare shot/rebound distributions
        '''

        df['is_rebound'] = np.where(((df['event'] == 'shot') | (df['event'] == 'goal'))
                                    & (df['event'].shift(1) == 'shot')
                                    & (df['team_id'] == df['team_id'].shift(1))
                                    & (df['game_id'] == df['game_id'].shift(1))
                                    & (df['period_id'] == df['period_id'].shift(1))
                                    & ((df['s'].astype(int) - df['s'].astype(int).shift(1)) <= 3.0),
                                    1, 0)

        df['is_rebound_assist'] = np.nan
        df['is_rebound_assist'] = df['is_rebound'].shift(-1)

        return df

    @staticmethod
    def merge_handedness_and_birthday(df, nameplate):
        '''
        For the shot orientation feature that is calculated in the wrangler we need the players handedness
        and goalies catching hand. We first merge the handedness on player id, and then merge it on goalie_id.

        This adds NA's for shots taken with no goalie which is addressed in the wrangler
        '''

        handedness = nameplate.drop_duplicates(subset=['player_id'], keep='first')

        df['player_id'] = df['player_id'].astype(float)
        df = pd.merge_ordered(df, handedness[['player_id', 'shoots', 'birthday']], how='left', left_on='player_id',
                              right_on='player_id')

        goalie_handedness = handedness[handedness.position == 'G'].rename(
            columns={"shoots": "catches", 'birthday': 'goalie_birthday', "player_id": "goalie_id"})

        goalie_handedness = goalie_handedness.drop_duplicates(subset=['goalie_id'], keep='first')

        df['goalie_id'] = df['goalie_id'].astype(str)
        goalie_handedness['goalie_id'] = goalie_handedness['goalie_id'].astype(str)
        df = pd.merge_ordered(df, goalie_handedness[['goalie_id', 'catches', 'goalie_birthday']], how='left',
                              left_on='goalie_id',
                              right_on='goalie_id')
        # Empty net indicator
        df['catches'] = df['catches'].fillna(0)

        df = df.sort_values(by=['unique_game_key', 's'], ascending=[False, True])

        return df

    @staticmethod
    def get_home_team_indicator(df, teamplate):
        '''
        Because we cant trust the data fully Im making sure the home and away team are referred to by their id rather
        than the team code. We then create a conditional to indicate if the shot was taken by the player on the home team or not.
        '''

        df['home_team'][df.home_team == 'SOO'] = 'SSM'

        df['home_team'] = df['home_team'].str.upper()
        df['away_team'] = df['away_team'].str.upper()

        teamplate['team_name'] = teamplate['team_name'].str.upper()

        df = df.merge(teamplate, how='left', left_on='home_team', right_on='team_name')


        df['home_team_indicator'] = np.where((df.player_team_id == df.team_id), 1, 0)

        return df

    @staticmethod
    def wrangle_player_goalie_age_bins_ohl(df):
        '''
        Annoyingly, the date format used between the ohl and qmjhl on their schedule are different, so getting
        player/goalie age has to be split into two functions. For the ohl some wrangling has to be done on the date
        variable first, then player birthday and goalie birthday are subtracted from it to get age. The age is
        then binned into five age bin defined in constants.py
        '''

        df['year_hold'] = np.nan

        df['year_hold'][df.date.astype(str).apply(lambda x: x.split(' ')[1]).isin(FIRST)] = df['year'].astype(
            str).apply(
            lambda x: x.split('-')[0])

        df['year_hold'][~df.date.astype(str).apply(lambda x: x.split(' ')[1]).isin(FIRST)] = df['year'].astype(
            str).apply(
            lambda x: x.split('-')[1])

        df['month_hold'] = np.nan
        for month, correct in MONTH_DICT.items():
            df['month_hold'][df.date.astype(str).apply(lambda x: x.split(' ')[1]) == month] = correct

        df['day_hold'] = df['date'].apply(lambda x: x.split(' ')[2])
        df['new_date'] = df['year_hold'].astype(str) + '-' + df['month_hold'].astype(str) + '-' + df['day_hold'].astype(
            str)

        df['player_age'] = (
                    (pd.to_datetime(df['new_date']) - pd.to_datetime(df['birthday'])) / np.timedelta64(1, 'Y')).astype(
            float)
        df['goalie_age'] = ((pd.to_datetime(df['new_date']) - pd.to_datetime(df['goalie_birthday'])) / np.timedelta64(1,
                                                                                                                      'Y')).astype(
            float)

        # Empty net indicator
        df['goalie_age'] = df['goalie_age'].fillna(0)
        df['goalie_age'][df.goalie_id == 0] = 0

        df['player_age_bin'] = pd.cut(df['player_age'], BINS)
        df['goalie_age_bin'] = pd.cut(df['goalie_age'], BINS)

        return df

    @staticmethod
    def wrangle_player_goalie_age_bins_qmjhl(df):
        '''
        For the Q this is much easier so we can directly use player birthday and goalie birthday and subtract it from date to get age.
        The age is then binned into five age bin defined in constants.py
        '''

        df['new_date'] = df['date'].apply(lambda x: x.split('\n')[0])

        df['player_age'] = ((pd.to_datetime(df['new_date'].astype(str)) - pd.to_datetime(
            df['birthday'].astype(str))) / np.timedelta64(1, 'Y')).astype(float)
        df['goalie_age'] = ((pd.to_datetime(df['new_date'].astype(str)) - pd.to_datetime(
            df['goalie_birthday'].astype(str))) / np.timedelta64(1, 'Y')).astype(float)

        # Empty net indicator
        df['goalie_age'] = df['goalie_age'].fillna(0)
        df['goalie_age'][df.goalie_id == 0] = 0

        df['player_age_bin'] = pd.cut(df['player_age'], BINS)
        df['goalie_age_bin'] = pd.cut(df['goalie_age'], BINS)

        return df

    @staticmethod
    def fix_description_error(df):
        '''
        Updating goals that are mislabelled as shots
        '''

        df['event'][df['shot_quality_description'] == 'Quality goal'] = 'goal'
        df['event'][df['shot_quality_description'] == ' Non quality goal'] = 'goal'

        return df

    @staticmethod
    def make_shots_same_direction(df):
        '''
        Adjusting shot x and y locations so that all shots are directed in the same direction
        '''

        df['x_location'] = np.where(df['x_location'] > 300,
                                    (df["x_location"] * -1 + 600),
                                    df['x_location'])
        df['y_location'] = np.where(df['x_location'] > 300,
                                    (df["y_location"] * -1 + 300),
                                    df['y_location'])

        return df

    @staticmethod
    def calculate_shot_distance(df):
        '''
        Calculates euclidean distance from player to net when shot is taken
        '''

        df['shot_distance'] = np.where((df['y_location'] > 150) | (df['y_location'] < 150),
                                       np.sqrt((df['x_location'] - 30) ** 2 + (150 - df['y_location']) ** 2),
                                       np.sqrt((df['x_location'] - 30) ** 2))

        return df

    @staticmethod
    def calculate_shot_angle(df):
        '''
        Calculates angle shot was taken at
        '''

        df['shot_angle'] = np.nan
        for i, row in df.iterrows():
            df['shot_angle'][i] = math.degrees(math.atan((abs(df['y_location'][i] - 150) / (df['x_location'][i] - 30))))

        return df

    @staticmethod
    def binarize_shot_variable(df):
        '''
        Converting goal/shot to binary variable
        '''

        df['event'][df['event'] == 'shot'] = 0
        df['event'][df['event'] == 'goal'] = 1

        return df

    def __call__(self):

        df, nameplate, teamplate = self.load_in_data()
        df = self.calculate_total_game_seconds(df)
        df = self.drop_duplicate_events(df)
        goalie_gp = self.subset_goalie_gp_data(df)
        df = self.subset_data(df)
        df = self.ffill_goal_rows(df)
        df = self.track_rebounds_and_rebound_assists(df)
        df = self.merge_handedness_and_birthday(df, nameplate)
        df = self.get_home_team_indicator(df, teamplate)

        if self.league == 'ohl':
            df = self.wrangle_player_goalie_age_bins_ohl(df)
        if self.league == 'qmjhl':
            df = self.wrangle_player_goalie_age_bins_qmjhl(df)

        df = self.fix_description_error(df)
        df = self.make_shots_same_direction(df)
        df = self.calculate_shot_distance(df)
        df = self.calculate_shot_angle(df)
        df = self.binarize_shot_variable(df)

        return df, goalie_gp