# Will add game_state as variable after game_state script is finalized
import pandas as pd

SHOTS = ['game_id', 'event', 'goalie_id', 'goalie_team_id', 'player_id', 'player_team_id',
         'quality', 's_', 'shot_quality_description', 'shot_type', 'shot_type_description',
         'time', 'time_formatted', 'x_location', 'y_location', 'game_ID', 'home', 'period', 'goal_type', 'home_team']

NET_SIZE = 6 * (300 / 85)
POST1_COORD = 150 - (NET_SIZE / 2)
POST2_COORD = 150 + (NET_SIZE / 2)

SCORE_STATE_DICT = {
    0: "Even",
    1: "Up_One",
    2: "Up_Two",
    3: "Up_Three",
    4: "Up_Four",
    -1: "Down_One",
    -2: "Down_Two",
    -3: "Down_Three",
    -4: "Down_Four",
}

FIRST = ['Sep', 'Oct', 'Nov', 'Dec']

MONTH_DICT = {
    "Jan": '01',
    "Feb": '02',
    "Mar": '03',
    "Apr": '04',
    "May": '05',
    "Jun": '06',
    "Jul": '07',
    "Aug": '08',
    "Sep": '09',
    "Oct": '10',
    "Nov": '11',
    "Dec": '12',
}

BINS = pd.IntervalIndex.from_tuples([(0, 5), (15, 16), (16.01, 17),
                                     (17.01, 18), (18.01, 19),
                                     (19.01, 20), (20.01, 21)])

FEATURE_COLS = [

    # 's',
    'x_location', 'y_location',
    'shot_location', 'angle_net_seen', 'shot_angle',
    'is_rebound', 'rebound_angle_change',
    2.0, 3.0, 4.0,
    "LLL", "LLR", "LRL", "LRR", "RLL", "RLR", "RRL", "RRR", "L0L", "L0R", "R0L", "R0R",
    'player_(15, 16]', 'player_(16.01, 17]', 'player_(17.01, 18]', 'player_(18.01, 19]', 'player_(19.01, 20]',
    'player_(19.01, 20]',
    'goalie_(0, 5]', 'goalie_(15, 16]', 'goalie_(16.01, 17]', 'goalie_(17.01, 18]', 'goalie_(18.01, 19]',
    'goalie_(19.01, 20]', 'goalie_(20.01, 21]',
    "Even", "Up_One", "Up_Two", "Up_Three", "Up_Four", "Down_One", "Down_Two", "Down_Three", "Down_Four",
    # "EV","SH","PP"
]
