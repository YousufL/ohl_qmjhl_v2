import pandas as pd
import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt
from data_cleaner import CleanData

class ShotDistributor(CleanData):
    def __init__(self, league, df, nameplate, teamplate):
        super().__init__(league, df, nameplate, teamplate)
        self.league = league
        self.df = df
        self.nameplate = nameplate
        self.teamplate = teamplate

    def get_constants(self):

        if self.league == 'ohl':
            df,goalie_gp = CleanData(league=self.league, df=self.df, nameplate=self.nameplate, teamplate = self.teamplate)()
            years = ['2015-2016', '2016-2017', '2017-2018', '2018-2019', '2019-2020']
            years_adjust = ['2015-2016', '2016-2017']
            num =4

        if self.league == 'qmjhl':
            df,goalie_gp = CleanData(league=self.league,df=self.df, nameplate=self.nameplate, teamplate = self.teamplate)()
            years = ['2014-2015','2015-2016', '2016-2017', '2017-2018', '2018-2019', '2019-2020','2020-2021']
            years_adjust = ['2014-2015','2015-2016', '2016-2017','2017-2018']
            num = 5

        return df, years, years_adjust,num, goalie_gp

    def plot_distributions(self,col, df):
        fig_dims = (9, 9)
        fig, ax = plt.subplots(figsize=fig_dims)
        ax = sns.violinplot(x=df["year"], y=df[col])
        sns.set(font_scale=1)
        ax.set_title(f'{self.league} Shot Coordinate Distribution', fontsize=16)
        ax.set_ylabel(col, fontsize=14)
        ax.set_xlabel('Season', fontsize=14)
        plt.show()

    @staticmethod
    def get_original_distributions(df,col, years):

        original_distribution = []
        for i in range(len(years)):
            counts, bin_edges = np.histogram(df[df.year == years[i]][col], bins=300, density=True)
            original_distribution.append(np.cumsum(counts) / np.sum(counts))

        return original_distribution

    @staticmethod
    def adjust_distribution(df,col,original_distribution,num,years_adjust):

        for i in range(0,len(df)):
            for j in range(len(years_adjust)):
                if df['year'][i] == years_adjust[j]:
                    try:
                        df[col][i] = np.argmin(np.abs(original_distribution[num] - original_distribution[j][df[col][i].astype(int)]))
                    except IndexError:
                        df[col][i] = df[col][i]

        return df


    def __call__(self):
        df, years, years_adjust,num, goalie_gp = self.get_constants()

        #self.plot_distributions('x_location',df)
        #self.plot_distributions('y_location', df)

        original_distribution_x = self.get_original_distributions(df,'x_location', years)
        original_distribution_y = self.get_original_distributions(df, 'y_location', years)

        df = self.adjust_distribution(df, 'x_location', original_distribution_x, num, years_adjust)
        df = self.adjust_distribution(df, 'y_location', original_distribution_y, num, years_adjust)

        #self.plot_distributions('x_location',df)
        #self.plot_distributions('y_location', df)

        return df, goalie_gp