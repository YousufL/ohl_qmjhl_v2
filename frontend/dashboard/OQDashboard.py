import dash
import dash_table
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output
import plotly.graph_objs as go
import pandas as pd
import numpy as np

data=pd.read_csv("OQDashboard_data_template.csv", encoding='latin-1')
# Helper Function #1 -- Transform data and calculate PCTL
def transform_data(raw_df=data, stat_labels_with_pctl=['Height', 'Age_at_cutoff', 'Draft_Position', 'Games_Played', 'G', 'A1', 'CF', 'PIM', 'FO%', 'xG', 'Finishing', 'GD%', 'G/60', 'A1/60', 'CF/60', 'PIM/60', 'xG/60']):
    """This function calculates the percentiles of each stat that we will have percentiles for"""
    rate_labels=['G/60', 'A1/60', 'CF/60', 'PIM/60', 'xG/60']
    # Subset the df using team stat_labels
    df=raw_df.copy()
    df[stat_labels_with_pctl]=df[stat_labels_with_pctl].astype('float64')
    df[rate_labels]=df[rate_labels].round(3)
    # New scale should be from 0 to 100.
    new_max = 100
    new_min = 0
    new_range = new_max - new_min
    for kpi in stat_labels_with_pctl:
        max_val = df[kpi].max()
        min_val = df[kpi].min()
        val_range = max_val - min_val
        val_range = max_val - min_val
        if kpi in ['+/-']:
          df[kpi + '_Pctl'] = df[kpi].apply(
              lambda x: (-1)*((((x - min_val) * new_range) / val_range) + new_min) + new_max + new_min)
        else:
          df[kpi + '_Pctl'] = df[kpi].apply(
            lambda x: (((x - min_val) * new_range) / val_range) + new_min)
    #Set Names as Index
    df.set_index('Player_Name', inplace=True)
    return df

master_data=transform_data(data)

t1_info=['Height', 'Age_at_cutoff', 'Draft_Position', 'Games_Played']
# Helper Function #2 -- select player attributes data, with Player Name as the input.
def prepare_player_attributes(player_name, data=master_data, attributes=t1_info):
    """ This function outputs a player's attributes, which includes their
    Age at some cutoff date (ie: beginning or end of most recent season),
    Height, Junior League draft position, and Number of games played in Juniors.
    Note that unlike some subsequent functions, there are no rate components to these attributes."""
    player_info=data.loc[player_name][attributes]
    table1_df = pd.DataFrame(list(player_info), columns=[''])
    table1_df.insert(0, 'Attributes', attributes)
    return table1_df
# print(prepare_player_attributes('Mitch Marner'))
t2_stats=['G', 'A1', 'CF', 'PIM', 'FO%']
# Helper Function #3 -- select player stats (not advanced), with Player Name as the input.
def prepare_player_stats(player_name, data=master_data, attributes=t2_stats):
    """ This function outputs a player's counting stats, which includes their
    ...
    and there are rate components to these attributes, other than FO%."""
    raw=data.loc[player_name][attributes]
    rate_labels=list(kpi + '/60' for kpi in attributes)
    rate=data.loc[player_name][rate_labels[:-1]]
    rate[rate_labels[-1]]= ""
    table2_df = pd.DataFrame({'Tot.':raw.values,
                              '*/60':rate})
    table2_df.insert(0, 'Counting Stat', attributes)
    return table2_df
# print(prepare_player_stats('Mitch Marner'))
t3_stats=['xG', 'Finishing', 'GD%']
# Helper Function #4 -- select player advanced stats, with Player Name as the input.
def prepare_player_advStats(player_name, data=master_data, attributes=t3_stats):
    """ This function outputs a player's advanced stats, which includes their
    Expected Goals (xG), Finishing Talent, and Goal Differential Percentage (GD%).
    Only xG has a rate component."""
    raw=data.loc[player_name][attributes]
    rate_labels=['xG/60']
    rate=data.loc[player_name][rate_labels]
    rate=[rate.values[0], "", ""]
    table3_df = pd.DataFrame({'Tot.':raw.values,
                              '*/60':rate})
    table3_df.insert(0, 'Advanced Stat', attributes)
    return table3_df
# print(prepare_player_advStats('Mitch Marner'))

# Helper Function #5
def create_scatterGrids(player_name, data=master_data, attributes={'info': t1_info, 'counting':t2_stats, 'advanced':t3_stats }, indicator=2):
    """This function produces all 3 scatter grid figure traces.
    The indicator variable will be used later with callbacks to determine which set of stats will be plotted"""
    table1Label_encoding = [(int(len(attributes['info'])) - 0.5 - i) for i in range(0, len(attributes['info']))]
    table2Label_encoding = [(int(len(attributes['counting'])) - 0.5 - i) for i in range(0, len(attributes['counting']))]
    table3Label_encoding = [(int(len(attributes['advanced'])) - 0.5 - i) for i in range(0, len(attributes['advanced']))]
    t1Pctl_labels = [info + '_Pctl' for info in attributes['info']]
    t2Pctl_labels = [kpi + '_Pctl' for kpi in attributes['counting']]
    t3Pctl_labels = [adv + '_Pctl' for adv in attributes['advanced']]
    infoPCTL = data.loc[player_name][t1Pctl_labels]
    prodRawPCTL = data.loc[player_name][attributes['counting']]
    prodRatePCTL = data.loc[player_name][t2Pctl_labels]
    advRawPCTL = data.loc[player_name][attributes['advanced']]
    advRatePCTL = data.loc[player_name][t3Pctl_labels]

    fig1 = go.Figure()

    fig1.add_trace(go.Scatter(
        x=infoPCTL.values,
        y=table1Label_encoding,
        name='Info PCTL',
        mode='markers',
        marker=dict(
            color='navy',
            line=dict(
                color='black',
                width=1),
            symbol='circle',
            size=13
        )
    ))

    fig1.update_xaxes(
        range=[0, 100],
        ticktext=['', '50th', '', ''],
        tickvals=[25, 50, 75, 100],
        tickfont_color='black',
        showticklabels=True,
        ticks='outside',
        tickfont=dict(color='black',
                      size=16),
        tickcolor='rgb(102, 102, 102)',
        side='top',
        showline=True,
        linewidth=2,
        linecolor='black',
        gridcolor='rgb(102, 102, 102)',
        showgrid=False
    )

    fig1.update_yaxes(
        range=[0, len(table1Label_encoding)],
        showline=False,
        showticklabels=False,
        linewidth=2,
        linecolor='rgb(102, 102, 102)',
        gridcolor='rgb(102, 102, 102)')

    fig1.update_layout(
        autosize=False,
        width=285,
        height=195,
        margin=dict(l=5, r=5, b=0, t=41),
        showlegend=False,
        paper_bgcolor='white',
        plot_bgcolor='white',
        hovermode='closest',
    )

    fig1.add_shape(
        # Vertical Line
        go.layout.Shape(
            type="line",
            x0=0.5,
            y0=0,
            x1=0.5,
            y1=4,
            xref='paper',
            yref='y',
            line=dict(
                color="Black",
                width=2
            )
        ))

    fig2 = go.Figure()

    fig2.add_trace(go.Scatter(
        x=prodRawPCTL.values,
        y=table2Label_encoding,
        name='Raw PCTL',
        mode='markers',
        marker=dict(
            color='navy',
            line=dict(
                color='black',
                width=1),
            symbol='circle',
            size=13
        )
    ))
    fig2.add_trace(go.Scatter(
        x=prodRatePCTL.values,
        y=table2Label_encoding,
        name='Rate PCTL',
        mode='markers',
        marker=dict(
            color='rgba(204, 204, 204, 0.95)',
            line=dict(
                color='black',
                width=1),
            symbol='x',
            size=13
        )
    ))
    fig2.update_xaxes(
        range=[0, 100],
        tickvals=[25, 50, 75, 100],
        tickfont_color='black',
        showticklabels=False,
        ticks='outside',
        tickfont=dict(color='black',
                      size=16),
        tickcolor='rgb(102, 102, 102)',
        side='top',
        showline=True,
        linewidth=2,
        linecolor='black',
        gridcolor='rgb(102, 102, 102)',
        showgrid=False
    )

    fig2.update_yaxes(
        range=[0, len(table2Label_encoding)],
        showline=False,
        showticklabels=False,
        linewidth=2,
        linecolor='rgb(102, 102, 102)',
        gridcolor='rgb(102, 102, 102)')

    fig2.update_layout(
        autosize=False,
        width=285,
        height=235,
        margin=dict(l=5, r=5, b=0, t=42),
        showlegend=False,
        paper_bgcolor='white',
        plot_bgcolor='white',
        hovermode='closest',
    )
    fig2.add_shape(
        # Vertical Line
        go.layout.Shape(
            type="line",
            x0=0.5,
            y0=0,
            x1=0.5,
            y1=7,
            xref='paper',
            yref='y',
            line=dict(
                color="Black",
                width=2
            )
    ))

    fig3 = go.Figure()

    fig3.add_trace(go.Scatter(
        x=advRawPCTL.values,
        y=table3Label_encoding,
        name='Raw PCTL',
        mode='markers',
        marker=dict(
            color='navy',
            line=dict(
                color='black',
                width=1),
            symbol='circle',
            size=13
        )
    ))
    fig3.add_trace(go.Scatter(
        x=advRatePCTL.values,
        y=table3Label_encoding,
        name='Rate PCTL',
        mode='markers',
        marker=dict(
            color='rgba(204, 204, 204, 0.95)',
            line=dict(
                color='black',
                width=1),
            symbol='x',
            size=13
        )
    ))
    fig3.update_xaxes(
        range=[0, 100],
        tickvals=[25, 50, 75, 100],
        tickfont_color='black',
        showticklabels=False,
        ticks='outside',
        tickfont=dict(color='black',
                      size=16),
        tickcolor='rgb(102, 102, 102)',
        side='top',
        showline=True,
        linewidth=2,
        linecolor='black',
        gridcolor='rgb(102, 102, 102)',
        showgrid=False
    )

    fig3.update_yaxes(
        range=[0, len(table3Label_encoding)],
        showline=False,
        showticklabels=False,
        linewidth=2,
        linecolor='rgb(102, 102, 102)',
        gridcolor='rgb(102, 102, 102)')

    fig3.update_layout(
        autosize=False,
        width=285,
        height=185,
        margin=dict(l=5, r=5, b=0, t=50),
        showlegend=False,
        paper_bgcolor='white',
        plot_bgcolor='white',
        hovermode='closest',
    )
    fig3.add_shape(
        # Vertical Line
        go.layout.Shape(
            type="line",
            x0=0.5,
            y0=0,
            x1=0.5,
            y1=7,
            xref='paper',
            yref='y',
            line=dict(
                color="Black",
                width=2
            )
        ))
    return [fig1, fig2, fig3]

player_list=master_data.index.unique()
team_list=master_data['Team'].unique()
leagues=master_data['League'].unique()

app = dash.Dash(external_stylesheets=[dbc.themes.BOOTSTRAP])

app.layout = html.Div([
                html.Div([
                    html.Div([
                        html.Div([
                            html.H1('OHL & QMJHL \n Dashboard')
                        ], style={'background-color':'#274490',
                                  'color':'white',
                                  'padding-top':'10px',
                                  'font-family': 'Montserrat',
                                  'font-size':'40px',
                                  'fontWeight':'bold',
                                  'text-align':'center'}),
                        html.Div([
                            html.Div([
                                html.H2("Player"),
                                dcc.Dropdown(
                                    id='player',
                                    options=[{'label': p, 'value': p} for p in player_list],
                                    value=player_list[0],
                                    style={'background-color':'white',
                                           'color':'#274490',
                                           'font-family': 'Montserrat',
                                           'font-size':'18px'})
                            ], style={'width':'345px',
                                      'padding-left':'20px',
                                      'padding-bottom':'20px'}),
                            html.Div([
                                html.H2("Team"),
                                # html.Label(["Team",
                                dcc.Dropdown(
                                    id='team',
                                    options=[{'label': t, 'value': t} for t in team_list],
                                    value=team_list[0],
                                    style={'background-color':'white',
                                           'color':'#274490',
                                           'font-family': 'Montserrat',
                                           'font-size':'18px'})
                            ], style={'width':'345px',
                                      'padding-left':'20px',
                                      'padding-bottom':'20px'}),
                            html.Div([
                                html.H2("League"),
                                dcc.Dropdown(
                                    id='league',
                                    options=[{'label': l, 'value': l} for l in leagues],
                                    value=leagues[0],
                                    style={'background-color':'white',
                                           'color':'#274490',
                                           'font-family': 'Montserrat',
                                           'font-size':'18px'})
                            ], style={'width':'130px',
                                      'padding-left': '20px',
                                        'padding-bottom':'20px'})
                        ], style={'display':'inline-flex',
                                  'background-color': '#274490',
                                  'color': 'white',
                                  'padding-top': '10px',
                                  'font-family': 'Montserrat',
                                  })
                    ],style={'background-color': '#274490'})
                ]),
                html.Div([
                    html.Div([
                            html.H2('Player Name: '+str(player_list[0]), id='header-player', style={'font-size':'24px'}),
                            html.H3('Team: '+str(team_list[0]), id='header-team', style={'font-size':'20px'}),
                            html.H3('League: '+str(leagues[0]), id='header-league', style={'font-size':'20px'})
                    ], style={'display':'inline-block',
                              'width':'50%'}),
                    html.Div([
                        dbc.ButtonGroup([
                                dbc.Button("Totals", color='primary'),
                                dbc.Button("Per 60", color='secondary')
                        ], size='lg')
                    ], style={'width': '50%',
                              'padding-left':'80px',
                              'padding-top':'20px'})
                ], style={'display':'inline-flex',
                          'padding':'20px'
                          }),
                html.Div([
                    html.Div([
                        dash_table.DataTable(id='info-table',
                                data=prepare_player_attributes(player_list[0]).to_dict('records'),
                                columns=[{'id': c, 'name': c} for c in prepare_player_attributes(player_list[0]).columns],
                                style_header={
                                        'fontSize':'24px',
                                        'color': 'navy',
                                        'backgroundColor': 'white',
                                        'font-family': 'Inter',
                                        'fontWeight': 'bold',
                                        'border':'none',
                                        'border-bottom': '2px solid black',
                                        'padding-bottom':'4px'},
                                style_cell={
                                    'font-family': 'Inter',
                                    'fontSize':'18px',
                                    'padding': '8px'},
                                    # 'border-bottom':'none'},
                                style_as_list_view=True,
                                style_cell_conditional=[
                                    {
                                        'if': {'column_id': c},
                                        'textAlign': 'left'
                                    } for c in ['Attributes']
                                ]
                            )], style={'width':'400px'}),
                        dcc.Graph(
                            id='info-scatter',
                            figure=create_scatterGrids(player_list[0])[0]
                        )
                    ], style={'display': 'inline-flex',
                              'padding-bottom':'20px',
                              'justify-content': 'center'}),
                html.Div([
                    html.Div([
                        dash_table.DataTable(id='counting-table',
                            data=prepare_player_stats(player_list[0]).to_dict('records'),
                            columns=[{'id': c, 'name': c} for c in prepare_player_stats(player_list[0]).columns],
                            style_header={
                                    'fontSize':'24px',
                                    'color': 'navy',
                                    'backgroundColor': 'white',
                                    'font-family': 'Inter',
                                    'fontWeight': 'bold',
                                    'border':'none',
                                    'border-bottom': '2px solid black',
                                    'padding-bottom':'4px'},
                            style_cell={
                                'font-family': 'Inter',
                                'fontSize':'18px',
                                'padding': '8px'},
                                # 'border-bottom':'none'},
                            style_as_list_view=True,
                            style_cell_conditional=[
                                {
                                    'if': {'column_id': c},
                                    'textAlign': 'left'
                                } for c in ['Counting Stat']
                            ]
                        )], style={'width':'400px'}),
                    dcc.Graph(
                        id='counting-scatter',
                        figure=create_scatterGrids(player_list[0])[1]
                    )
                ], style={'display': 'inline-flex',
                              'padding-bottom':'20px',
                              'justify-content': 'center'}),

                html.Div([
                    html.Div([
                        dash_table.DataTable(id='advanced-table',
                                data=prepare_player_advStats(player_list[0]).to_dict('records'),
                                columns=[{'id': c, 'name': c} for c in prepare_player_advStats(player_list[0]).columns],
                                style_header={
                                        'fontSize':'24px',
                                        'color':'navy',
                                        'backgroundColor': 'white',
                                        'font-family': 'Inter',
                                        'fontWeight': 'bold',
                                        'border':'none',
                                        'border-bottom': '2px solid black',
                                        'padding-bottom':'8px'},
                                style_cell={
                                    'font-family': 'Inter',
                                    'fontSize':'18px',
                                    'padding': '12px'},
                                    # 'border-bottom':'none'},
                                style_as_list_view=True,
                                style_cell_conditional=[
                                    {
                                        'if': {'column_id': c},
                                        'textAlign': 'left'
                                    } for c in ['Advanced Stat']
                                ]
                            )], style={'width':'400px'}),
                        dcc.Graph(
                            id='advanced-scatter',
                            figure=create_scatterGrids(player_list[0])[2]
                        )
                    ], style={'display': 'inline-flex',
                              'padding-bottom':'20px',
                              'justify-content': 'center'})

            ], style={'width':'900px',
                      'margin': 'auto',
                      'display': 'grid',
                      'font-family': 'Montserrat'})

## Building the basics of the callback

@app.callback(
    [Output('info-table', 'data'), Output('info-scatter', 'figure'),
     Output('counting-table','data'), Output('counting-scatter', 'figure'),
     Output('advanced-table','data'), Output('advanced-scatter', 'figure'),
     Output('header-player', 'value'), Output('header-team', 'value'), Output('header-league', 'value')],
    [Input('player','value')]) #, Input('team','value'), Input('league','value')])

def updateWinProb(player_name):
    head_player=player_name
    head_team=master_data[player_name]['Team']
    head_league=master_data[player_name]['League']
    info_table=prepare_player_attributes(player_name)
    countingStats_table=prepare_player_stats(player_name)
    advStats_table=prepare_player_advStats(player_name)
    fig_traces = create_scatterGrids(player_name)
    return [info_table.to_dict('records'),
            fig_traces[0],
            countingStats_table.to_dict('records'),
            fig_traces[1],
            advStats_table.to_dict('records'),
            fig_traces[2],
            head_player,
            head_team,
            head_league]


if __name__ == '__main__':
    app.run_server()