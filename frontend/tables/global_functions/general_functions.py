import pandas as pd
import numpy as np
import re
regex = re.compile('[^a-zA-Z]')

def to_1D(series):
 return pd.Series([x for _list in series for x in _list])

def get_draft_ohl(nameplate: pd.DataFrame) -> pd.DataFrame:
    nameplate['draft'] = nameplate['draft'].fillna(0)
    nameplate['s'] = nameplate['draft'].apply(
        lambda x: ("Round:" in str(x)) & ("OHL" in str(x)) & ("FA" not in str(x)) & ("AP" not in str(x)))
    nameplate['q'] = nameplate['draft'].apply(lambda x: "Import" in str(x))

    nameplate['Draft'] = 0
    nameplate['year'] = 0
    nameplate['round'] = 0
    nameplate['pick'] = 0
    nameplate['team'] = 0

    nameplate.loc[(nameplate['q'] == True), "Draft"] = "Import"
    nameplate.loc[(nameplate['s'] == True), "Draft"] = "OHL"

    nameplate.loc[(nameplate.q == True), 'year'] = nameplate[nameplate.q == True]['draft'].apply(
        lambda x: x.split("Import - ")[1].split("(")[1].split(")")[0].strip())
    nameplate.loc[(nameplate.q == True), 'round'] = nameplate[nameplate.q == True]['draft'].apply(
        lambda x: x.split("Import - ")[1].split("Round:")[1].split("(")[0].strip())
    nameplate.loc[(nameplate.q == True), 'pick'] = nameplate[nameplate.q == True]['draft'].apply(
        lambda x: x.split("Import - ")[1].split("Round:")[1].split("(#")[1].split(")")[0].strip())
    nameplate.loc[(nameplate.q == True), 'team'] = nameplate[nameplate.q == True]['draft'].apply(
        lambda x: x.split("Import - ")[1].split("(")[0].strip())

    nameplate.loc[(nameplate.s == True), 'year'] = nameplate[nameplate.s == True]['draft'].apply(
        lambda x: x.split("OHL - ")[1].split("(")[1].split(")")[0].strip())
    nameplate.loc[(nameplate.s == True), 'round'] = nameplate[nameplate.s == True]['draft'].apply(
        lambda x: x.split("OHL - ")[1].split("Round:")[1].split("(")[0].strip())
    nameplate.loc[(nameplate.s == True), 'pick'] = nameplate[nameplate.s == True]['draft'].apply(
        lambda x: x.split("OHL - ")[1].split("Round:")[1].split("(#")[1].split(")")[0].strip())
    nameplate.loc[(nameplate.s == True), 'team'] = nameplate[nameplate.s == True]['draft'].apply(
        lambda x: x.split("OHL - ")[1].split("(")[0].strip())

    nameplate = nameplate.drop(columns=['s', 'q', 'draft']).reset_index(drop=True)
    nameplate = nameplate.fillna(0)
    nameplate.loc[(nameplate['year'] == "WHL"), 'year'] = 2013

    return nameplate

def get_draft_qmjhl(nameplate: pd.DataFrame) -> pd.DataFrame:

    nameplate['draft'] = nameplate['draft'].fillna(0)
    nameplate['s'] = nameplate['draft'].apply(lambda x: "QMJHL - Drafted:" in str(x))
    nameplate['q'] = nameplate['draft'].apply(lambda x: "IMPORT" in str(x))

    nameplate['Draft'] = 0
    nameplate['year'] = 0
    nameplate['round']= 0
    nameplate['pick'] = 0
    nameplate['team'] = 0

    nameplate.loc[(nameplate['q'] == True), "Draft"] = "Import"
    nameplate.loc[(nameplate['s'] == True), "Draft"] = "QMJHL"

    nameplate.loc[(nameplate.q == True), 'year'] = nameplate[nameplate.q == True]['draft'].apply(
        lambda x: x.split("IMPORT - Drafted:")[1].strip()[0:4])

    nameplate.loc[(nameplate.q == True), 'round'] = nameplate[nameplate.q == True]['draft'].apply(
        lambda x: x.split("IMPORT - Drafted:")[1].split("Round: ")[1].strip()[0:2])

    nameplate.loc[(nameplate.q == True), 'pick'] = nameplate[nameplate.q == True]['draft'].apply(
        lambda x: x.split("IMPORT - Drafted:")[1].split("(#")[1].split(')')[0].strip())

    nameplate.loc[(nameplate.q == True), 'team'] = nameplate[nameplate.q == True]['draft'].apply(
        lambda x: x.split("IMPORT - Drafted:")[1].split("(#")[1].split(")")[1].strip().split(',')[0:3])

    nameplate.loc[(nameplate.s == True), 'year'] = nameplate[nameplate.s == True]['draft'].apply(
        lambda x: x.split("QMJHL - Drafted:")[1].strip()[0:4])

    nameplate.loc[(nameplate.s == True), 'round'] = nameplate[nameplate.s == True]['draft'].apply(
        lambda x: x.split("QMJHL - Drafted:")[1].split("Round: ")[1].strip()[0:2])

    nameplate.loc[(nameplate.s == True), 'pick'] = nameplate[nameplate.s == True]['draft'].apply(
        lambda x: x.split("QMJHL - Drafted:")[1].split("(#")[1].split(')')[0].strip())

    nameplate.loc[(nameplate.s == True), 'team'] = nameplate[nameplate.s == True]['draft'].apply(
        lambda x: x.split("QMJHL - Drafted:")[1].split("(#")[1].split(")")[1].strip())

    nameplate = nameplate.drop(columns=['s','q', 'draft']).reset_index(drop=True)
    nameplate = nameplate.fillna(0)

    return nameplate


def get_season_and_season_type(df: pd.DataFrame):

    df['season_name'] = df['season_name'].apply(lambda x: x.replace("|", ""))
    df['season_name'] = df['season_name'].apply(lambda x: x.split(" "))

    df['season_type'] = np.where((df.season_name.str.len() == 3),
                                 df.season_name.str[1] + ' ' + df.season_name.str[2],
                                 np.where((df.season_name.str.len() == 2),
                                          df.season_name.str[1],
                                          np.where((df.season_name.str.len() == 4),
                                                   df.season_name.str[2] + ' ' + df.season_name.str[3],
                                                   np.where((df.season_name.str.len() == 3),
                                                            df.season_name.str[2],
                                                            df.season_name))))

    df['season_type'] = df['season_type'].apply(lambda x: x.strip())
    df['season'] = df.season_name.apply(lambda x: x[0])

    CLEAN_DICT = {
        '2015': '2014-15',
        '2016': '2015-16',
        '2017': '2016-17',
        '2018': '2017-18',
        '2019': '2018-19',
        '2020': '2019-20',
        '2021': '2020-21',
    }

    for wrong, correct in CLEAN_DICT.items():
        df.loc[(df.season == wrong), 'season'] = correct

    return df

def fix_league_and_team(df: pd.DataFrame):

    import ipdb
    ipdb.set_trace()

    df.league = df.league.str.upper()
    df.team = df.team.astype(str)
    df.team = df.team.apply(lambda x: regex.sub('', x))

    correct_leagues = ['Cha', 'CHA', 'She', 'BLB', 'SNB', 'Que', 'Qu', 'KIT', 'ER', 'PLY', 'OSH', 'GUE', 'SBY', 'BRA',
                       '', 'MISS', 'WSR',
                       'SAR', 'BAR', 'OS', 'BELV', 'KGN', 'SSM', 'EDM', 'NIAG', 'OTT',
                       'BRAM', 'LDN', 'SAG', 'PBO', 'NB', 'FLNT', 'HAM', 'Miss', 'Sar',
                       'Bat', 'BaC', 'Hal', 'Dru', 'Gat', 'Lew', 'Mon', 'PEI', 'Rou',
                       'Sha', 'VdO', 'Vic', 'Rim', 'Mtl', 'Chi', 'Cap']

    df.loc[~(df.team.isin(correct_leagues)), 'team'] = ''
    df.team = df.team.str.upper()


    if 'goalie_team_code' in df.columns:
        df.goalie_team_code = df.goalie_team_code.str.upper()

    if 'player_team_code' in df.columns:
        df.player_team_code = df.player_team_code.str.upper()

    return df


def fix_height(df: pd.DataFrame):

    df = df.dropna(subset=['height'])
    df.loc[(df.height == '6'), 'height'] = '6.00'
    df.height = df.height.astype(str)

    df.height = df.height.apply(lambda x: x.replace("\'", ".").split('"')[0].split(".")[0] + "'" +
                                          x.replace("\'", ".").split('"')[0].split(".")[1] + '"')

    df.loc[(df.height == '5\'1"' ), 'height'] = '5\'10"'

    return df


