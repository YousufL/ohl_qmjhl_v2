from sqlalchemy import create_engine

from goalie_constants import OG_COLS, RENAMED_COLS
import pandas as pd
import sys

sys.path.extend(['frontend/tables/global_functions'])
sys.path.extend(['frontend/tables/goalies'])
sys.path.extend(['frontend/tables/players/data_prep'])
sys.path.extend(['frontend/tables/players'])
from player_table_functions import PlayerTable

class GoalieTable(PlayerTable):

    def load_goalie_data(self):

        engine = create_engine('postgresql://super:super123@localhost:9999/postgres')

        query_1 = "SELECT * FROM ohl.raw_ohl;"
        df_ohl = pd.read_sql(query_1, engine)

        query_2 = "SELECT * FROM ohl.goalie_gp_ohl;"
        goalie_gp_ohl = pd.read_sql(query_2, engine)

        query_3 = "SELECT * FROM ohl.nameplate_ohl;"
        nameplate_ohl = pd.read_sql(query_3, engine)

        query_4 = "SELECT * FROM ohl.teamplate_ohl;"
        teamplate_ohl = pd.read_sql(query_4, engine)

        query_5 = "SELECT * FROM qmjhl.raw_qmjhl;"
        df_qmjhl = pd.read_sql(query_5, engine)

        query_6 = "SELECT * FROM qmjhl.goalie_gp_qmjhl;"
        goalie_gp_qmjhl = pd.read_sql(query_6, engine)

        query_7 = "SELECT * FROM qmjhl.nameplate_qmjhl;"
        nameplate_qmjhl = pd.read_sql(query_7, engine)

        query_8 = "SELECT * FROM qmjhl.teamplate_qmjhl;"
        teamplate_qmjhl = pd.read_sql(query_8, engine)

        return goalie_gp_ohl, df_ohl, nameplate_ohl, teamplate_ohl, goalie_gp_qmjhl, df_qmjhl, nameplate_qmjhl, teamplate_qmjhl

    @staticmethod
    def goalie_fix_columns_and_upper(df):

        df = df[OG_COLS]
        df.columns = RENAMED_COLS

        return df

    @staticmethod
    def goalie_clean_prep(df):

        df = df.dropna()
        df = df.reset_index(drop=True)
        df = df.round(3)
        df = df.sort_values(by="Name", ascending=False)

        df.loc[(df['Draft Type'] == 0, 'Draft Type')] = None
        df.loc[(df['Draft Year'] == 0, 'Draft Year')] = None
        df.loc[(df['Draft Team'] == 0, 'Draft Team')] = None
        df.loc[(df['Round'] == 0, 'Round')] = None
        df.loc[(df['Pick'] == 0, 'Pick')] = None

        return df

    @staticmethod
    def to_1D(series):
     return pd.Series([x for _list in series for x in _list])

    @staticmethod
    def goalie_get_gp_total(master: pd.DataFrame) -> pd.DataFrame:

        master_copy = master.copy()

        master_copy = master_copy[master_copy.event == 'goalie_change']
        master_copy = master_copy[['event', 'goalie_in_id', 'season_name', 'team_id', 'game_id']]
        master_copy = master_copy.dropna(subset=['goalie_in_id']).reset_index(drop=True)
        master_copy = master_copy.drop_duplicates()

        goalie_gp_totals = master_copy.groupby(['goalie_in_id', 'season_name', 'team_id'])['event'].count().reset_index()
        goalie_gp_totals = goalie_gp_totals.rename(columns={"event": "GP"})

        return goalie_gp_totals

    @staticmethod
    def get_goalie_table_df(df: pd.DataFrame, nameplate: pd.DataFrame, goalie_gp_totals: pd.DataFrame, teamplate: pd.DataFrame) -> pd.DataFrame:

        df = df.copy()

        df['goalie_team_code'] = df['goalie_team_code'].str.upper()
        teamplate['team_name'] = teamplate['team_name'].str.upper()

        shots_ = df.groupby(['goalie_id', 'season_name','goalie_team_code', 'game_state'])['event'].count().reset_index()
        shots_AS = shots_.groupby(['goalie_id', 'season_name','goalie_team_code'])['event'].sum().reset_index()
        shots_AS['game_state'] = 'AS'
        shots = pd.concat([shots_, shots_AS], axis=0)
        shots = shots.rename(columns={"event": 'shots against'})

        goals_ = df.groupby(['goalie_id', 'season_name','goalie_team_code', 'game_state'])['xG', 'event'].sum().reset_index()
        goals_AS = goals_.groupby(['goalie_id', 'season_name','goalie_team_code'])['xG', 'event'].sum().reset_index()
        goals_AS['game_state'] = 'AS'
        goals = pd.concat([goals_, goals_AS], axis=0)
        goals = goals.rename(columns={"event": 'goals against', "xG": "xGA"})

        final = shots.merge(goals, how='left', on=['goalie_id', 'season_name', 'goalie_team_code','game_state'])

        final['GSAX'] = final['xGA'] - final['goals against']
        final['xGA/GA'] = final['xGA'] / final['goals against']

        final['xsv%'] = ((final['shots against'] - final['xGA']) / final['shots against'])
        final['sv%'] = ((final['shots against'] - final['goals against']) / final['shots against'])
        final['delta sv%'] = final['sv%'] - final['xsv%']

        final = final[final.goalie_id != 0.0]
        final['goalie_id'] = final['goalie_id'].astype(float)
        nameplate['player_id'] = nameplate['player_id'].astype(float)

        final = final.merge(nameplate[['player_id','name', 'position', 'shoots', 'height', 'birthday', 'Draft','year','round','pick','team']], how='left',
                            left_on='goalie_id', right_on='player_id')

        final = final.merge(teamplate[['id', 'team_name']], how='left', left_on='goalie_team_code', right_on='team_name')
        final = final.drop(columns =['team_name'])
        final = final.rename(columns = {"id":'team_id'})

        goalie_gp_totals = goalie_gp_totals.rename(columns = {"goalie_in_id":"goalie_id"})

        goalie_gp_totals['goalie_id'] = goalie_gp_totals['goalie_id'].astype(float)
        final['goalie_id'] = final['goalie_id'].astype(float)
        goalie_gp_totals['team_id'] = goalie_gp_totals['team_id'].astype(float)
        final['team_id'] = final['team_id'].astype(float)
        final = final.merge(goalie_gp_totals, how='left', on = ['goalie_id', 'season_name', 'team_id'] )
        final = final.drop(columns=['player_id'])

        return final

    def __call__(self):

        master_ohl, df_ohl, nameplate_ohl, teamplate_ohl, master_qmjhl, df_qmjhl, nameplate_qmjhl, teamplate_qmjhl = self.load_goalie_data()
        goalie_gp_totals_qmjhl = self.goalie_get_gp_total(master_qmjhl)
        goalies_qmjhl = self.get_goalie_table_df(df_qmjhl, nameplate_qmjhl, goalie_gp_totals_qmjhl, teamplate_qmjhl)
        goalies_qmjhl['league'] = 'qmjhl'

        goalie_gp_totals_ohl = self.goalie_get_gp_total(master_ohl)
        goalies_ohl = self.get_goalie_table_df(df_ohl, nameplate_ohl, goalie_gp_totals_ohl, teamplate_ohl)
        goalies_ohl['league'] = 'ohl'

        goalies_final= pd.concat([goalies_ohl,goalies_qmjhl],axis=0)
        goalies_final = self.get_season_and_season_type(goalies_final)
        goalies_final = self.fix_league_and_team(goalies_final)
        goalies_final = self.fix_height(goalies_final)
        goalies_final = self.goalie_fix_columns_and_upper(goalies_final)
        goalies_final = self.goalie_clean_prep(goalies_final)
        goalies_final = goalies_final.dropna(subset=['Team'])

        return goalies_final
