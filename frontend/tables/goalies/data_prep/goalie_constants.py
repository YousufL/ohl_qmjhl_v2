


REG_COLS = [ 'Name','League', 'Pos','Season Type','Season','Team','GP',
             'Game State','Shots Against', 'xGA', 'GA', 'GSAX', 'xGA/GA', 'Expected sv%', 'sv%', 'Delta sv%']


INFO_COLS = ['Height', 'Birthday', 'Catches','Draft Type', 'Draft Year', 'Draft Team', 'Round',
    'Pick']


SUB_COLS = ['Name','League', 'Pos','Season Type','Season','Team','GP','Game State']


OG_COLS = [
    'goalie_team_code',
    'game_state',
    'shots against',
    'xGA',
    'goals against',
    'GSAX',
    'xGA/GA',
    'xsv%',
       'sv%',
    'delta sv%',
    'name',
    'position',
    'shoots',
    'height',
    'birthday',
    'Draft',
    'year',
    'round',
    'pick',
    'team',
    'GP',
    'league',
    'season_type',
    'season']

RENAMED_COLS = ['Team', 'Game State', 'Shots Against', 'xGA','GA','GSAX','xGA/GA','Expected sv%', 'sv%','Delta sv%',
                'Name', 'Pos', 'Catches','Height',
                'Birthday','Draft Type','Draft Year','Round','Pick','Draft Team','GP','League','Season Type','Season']