import pandas as pd
from helper_css_functions import discrete_background_color_bins

def goalie_dropdown_lists(df):

    Teams_ = df.Team.unique().tolist()
    Players_ = df['Name'].unique().tolist()
    Seasons_ = df.Season.unique().tolist()
    Season_Types_ = df['Season Type'].unique().tolist()
    Leagues_ = df.League.unique().tolist()
    Situations_ = df['Game State'].unique().tolist()

    return Teams_, Players_, Seasons_, Season_Types_, Leagues_, Situations_


def goalie_dynamic_dropdown_output(df_master,
           Situations,
           Teams,
           Players,
           Season_Types,
           Seasons,
           Leagues, Situations_, Teams_, Players_, Season_Types_, Seasons_, Leagues_):

    df_master_copy = df_master.copy()
    Player_hold = Players

    filters = [Situations, Teams, Players, Season_Types, Seasons, Leagues]
    cols = ['Game State', 'Team', 'Name', 'Season Type','Season', 'League']

    filters_ = [Situations_, Teams_, Players_, Season_Types_, Seasons_, Leagues_]

    for i in range(len(filters)):
        if filters[i]:
            df_master_copy = df_master_copy[df_master_copy[cols[i]].isin(filters[i])]
            filters[i] = [{'label': x, 'value': x} for x in filters_[i]]

    for i in range(len(filters)):
        if not filters[i]:
            filters[i] = [{'label': x, 'value': x} for x in df_master_copy[cols[i]].unique().tolist()]

    if (not Player_hold):
        (xg_styles, legend) = discrete_background_color_bins(df_master_copy, columns=['GSAX'])

    if Player_hold:
        (xg_styles, legend) = discrete_background_color_bins(df_master, columns=['GSAX'])


    Situations = filters[0]
    Teams = filters[1]
    Players = filters[2]
    Season_Types = filters[3]
    Seasons = filters[4]
    Leagues = filters[5]

    return Situations, Teams, Players, Season_Types, Seasons, Leagues, df_master_copy, xg_styles, legend


def get_goalie_totals(df_2):

    TOTALS_COLS_HOLD = ['Name','Pos','Catches','Height','League','Game State','Season Type']
    SUM_COLS = ['GP','Shots Against', 'xGA','GA']
    MERGE_COLS = ['Name','Pos','Catches','Height','League','Game State','Season Type']
    INFO_COLS_HOLD = ['Draft Type', 'Draft Year', 'Draft Team','Round', 'Pick', 'Birthday']
    MERGE_COLS_HOLD = TOTALS_COLS_HOLD + INFO_COLS_HOLD

    df_hold_1 = df_2.groupby(TOTALS_COLS_HOLD)[SUM_COLS].sum().reset_index()
    df_hold_2 = df_2.groupby(TOTALS_COLS_HOLD)['Team'].unique().reset_index()

    df_2['Season'] = df_2['Season'].apply(lambda x: x + ',')
    df_hold_3 = df_2.groupby(TOTALS_COLS_HOLD)['Season'].unique().reset_index()
    merge_1 = df_hold_1.merge(df_hold_2, how='left', on = MERGE_COLS)
    merge_1_A = merge_1.merge(df_hold_3, how='left', on = MERGE_COLS)
    merge_2 = merge_1_A.merge(df_2[MERGE_COLS_HOLD], how='left', on = MERGE_COLS)


    merge_2['GSAX'] = merge_2['xGA'] - merge_2['GA']
    merge_2['xGA/GA'] = merge_2['xGA'] /merge_2['GA']

    merge_2['Expected sv%'] = ((merge_2['Shots Against'] - merge_2['xGA']) / merge_2['Shots Against'])
    merge_2['sv%'] = ((merge_2['Shots Against'] - merge_2['GA']) / merge_2['Shots Against'])
    merge_2['Delta sv%'] = merge_2['sv%'] - merge_2['Expected sv%']

    merge_2 = pd.DataFrame(merge_2)
    merge_2['GSAX'] = merge_2['GSAX'].round(2)
    merge_2['xGA/GA'] = merge_2['xGA/GA'].round(2)
    merge_2['sv%'] = merge_2['sv%'].round(2)
    merge_2['Delta sv%'] = merge_2['Delta sv%'].round(2)
    merge_2['Expected sv%'] = merge_2['Expected sv%'].round(2)
    merge_2['xGA'] = merge_2['xGA'].round(2)

    merge_2.drop_duplicates(subset=TOTALS_COLS_HOLD, inplace=True)
    df_2 = merge_2

    df_2['Season'] = df_2['Season'].astype(str)
    df_2['Team'] = df_2['Team'].astype(str)

    df_2['Season'] = df_2['Season'].apply(lambda x: x.replace('[',""))
    df_2['Season'] = df_2['Season'].apply(lambda x: x.replace(' ',""))
    df_2['Season'] = df_2['Season'].apply(lambda x: x.replace(']',"").strip())
    df_2['Season'] = df_2['Season'].apply(lambda x: x.replace("'","").strip())
    df_2['Season'] = df_2['Season'].apply(lambda x: x[:-1])

    df_2['Team'] = df_2['Team'].apply(lambda x: x.replace('[',""))
    df_2['Team'] = df_2['Team'].apply(lambda x: x.replace(' ',","))
    df_2['Team'] = df_2['Team'].apply(lambda x: x.replace(']',"").strip())
    df_2['Team'] = df_2['Team'].apply(lambda x: x.replace("'","").strip())

    return df_2



