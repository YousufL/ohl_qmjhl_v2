import dash_table
import pandas as pd
import dash
from dash.dependencies import Input, Output
import dash_html_components as html
import dash_bootstrap_components as dbc
import sys

from sqlalchemy import create_engine

sys.path.extend(['frontend/tables/global_functions'])
sys.path.extend(['frontend/tables/teams/data_prep'])
from constants import TEAM_COLS
from team_table_functions import TeamTable
from team_table_children import row_1, row_2
from data_prep.team_helper_functions import team_dropdown_lists,team_dynamic_dropdown_output, get_team_totals

import sys
sys.path.extend(['frontend/tables/global_functions'])
from helper_css_functions import discrete_background_color_bins, create_conditional_style

external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"]

engine = create_engine('postgresql://super:super123@localhost:9999/postgres')
df = pd.read_sql('teams', con=engine)
Teams_, Seasons_, Season_Types_, Leagues_, Situations_ = team_dropdown_lists(df)
df_master  = df.copy()

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP],
                meta_tags=[{'name': 'viewport',
                            'content': 'width=device-width, initial-scale=1.0, maximum-scale=1.2, minimum-scale=0.5'}]
                )

app.layout = html.Div([html.Div(id='table_title', children=[
    html.H4('Ranking every team in the OHL & QMJHL by expected goals (xG)')]),
                       row_1,
                       row_2,
                       #row_3,
                       html.Div(id='line'),
                       html.Div(id='output-data-upload')])


@app.callback([Output('output-data-upload', 'children'),
              Output('Situations', 'options'),
              Output('Teams', 'options'),
              Output('Season_Types', 'options'),
              Output('Seasons', 'options'),
              Output('Leagues',  'options'),
              Output('legend2', 'children')
              ],
			 [
              Input('Situations', 'value'),
              Input('Teams', "value"),
              Input('Season_Types', "value"),
              Input('Seasons', "value"),
              Input('Leagues', "value"),
              Input('groupby', 'value'),
              ]
              )

def update_output(
                  Situations,
                  Teams,
                  Season_Types,
                  Seasons,
                  Leagues,
                  groupby,
                  ):


    Situations, Teams, Season_Types, Seasons, Leagues, df_2, xg_styles, legend = team_dynamic_dropdown_output(
        df_master,
        Situations,
        Teams,
        Season_Types,
        Seasons,
        Leagues,
        Situations_,
        Teams_,
        Season_Types_,
        Seasons_,
        Leagues_
    )

    if groupby == ['totals']:
         df_2 = get_team_totals(df_2)
         df_2 = df_2[TEAM_COLS]
         (xg_styles, legend) = discrete_background_color_bins(df_2, columns=['xGF'])


    standard = [
                {'if': {'column_id': 'Season'}, 'border-right': '1px solid black'},
                {'if': {'column_id': 'Team'}, 'border-left': '1px solid black'},
                {'if': {'column_id': 'Height'}, 'border-left': '1px solid black'},
                {'if': {'column_id': 'SF%'}, 'border-right': '1px solid black'}
                ]


    styles = create_conditional_style(df_2) + standard + xg_styles

    return dash_table.DataTable(id='table', columns=[{"name": i, "id": i} for i in df_2.columns],
                                page_current=0,
                                page_size=30,
                                page_action='native',
                                sort_action='native',
                                sort_mode='multi',
                                sort_by=[],
                                style_table={'padding-top': '5px',
                                             'height': '1000',
                                             'overflow-y': 'visible'},
                                fixed_rows={'headers': True},
                                fixed_columns={'headers': True, 'data': 1},

                                style_header={'border-bottom': '2px solid black',
                                              'text-align': 'center',
                                              'font-size': '14px',
                                              'font-weight': 'bold',
                                              'backgroundColor': '#ffffff'},
                                style_data={'text-align': 'center',
                                            'font-size': '14px',
                                            'padding-left': '3px',
                                            'padding-right': '3px',
                                            'border-bottom': '1px solid grey',
                                            'whiteSpace': 'normal'},
                                style_cell={'textAlign': 'left',
                                            'whiteSpace': 'no-wrap',
                                            'textOverflow': 'ellipsis',
                                            'border': '1px solid white'},
                                style_data_conditional=styles,
                                style_header_conditional=(
                                    {
                                        'if': {'column_id': 'xG'},
                                        'backgroundColor': '#e6e6e6'
                                    },
                                ),
                                data=df_2.to_dict(
                                    'rows')), Situations, Teams ,Season_Types, Seasons, Leagues, legend

if __name__ == '__main__':
    app.run_server(debug=True)