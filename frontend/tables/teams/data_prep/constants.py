

TEAM_OG_COLS = ['GP','game_state', 'shots against',
       'shots', 'xGA', 'goals against', 'xGF', 'goals', 'xGF%', 'GF%', 'SF%',
       'team_name', 'league', 'season_type', 'season']

TEAM_RENAMED_COLS = ['GP','Game State', 'Shots Against',
       'Shots', 'xGA', 'GA', 'xGF', 'GF', 'xGF%', 'GF%', 'SF%',
       'Team', 'League', 'Season Type', 'Season']


TEAM_COLS = ['Team','League', 'Season Type',
             'Season','GP','Game State','xGF',
             'xGA','xGF%','GF','GA','GF%',
             'Shots','Shots Against','SF%']