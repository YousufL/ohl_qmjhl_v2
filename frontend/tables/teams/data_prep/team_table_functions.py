import pandas as pd
from sqlalchemy import create_engine

from general_functions import get_season_and_season_type
import sys

sys.path.extend(['frontend/tables/global_functions'])
sys.path.extend(['frontend/tables/teams/data_prep'])
sys.path.extend(['frontend/tables/players/data_prep'])
sys.path.extend(['frontend/tables/players'])

from player_table_functions import PlayerTable
from constants import TEAM_OG_COLS, TEAM_COLS, TEAM_RENAMED_COLS

class TeamTable(PlayerTable):

    def load_team_data(self):

        engine = create_engine('postgresql://super:super123@localhost:9999/postgres')
        #query = "SELECT * FROM raw_ohl WHERE season_name ='2020-2021 Regular Season;"

        query_1 = "SELECT * FROM ohl.raw_ohl;"
        df_ohl = pd.read_sql(query_1, engine)

        query_4 = "SELECT * FROM ohl.teamplate_ohl;"
        teamplate_ohl = pd.read_sql(query_4, engine)

        query_5 = "SELECT * FROM qmjhl.raw_qmjhl;"
        df_qmjhl = pd.read_sql(query_5, engine)

        query_8 = "SELECT * FROM qmjhl.teamplate_qmjhl;"
        teamplate_qmjhl = pd.read_sql(query_8, engine)

        return df_ohl, teamplate_ohl, df_qmjhl, teamplate_qmjhl

    @staticmethod
    def to_1D(series):
     return pd.Series([x for _list in series for x in _list])

    @staticmethod
    def get_team_table_df(df: pd.DataFrame, teamplate: pd.DataFrame) -> pd.DataFrame:

        shots_ = df.groupby(['goalie_team_id', 'season_name', 'game_state'])['event'].count().reset_index()
        shots_AS = shots_.groupby(['goalie_team_id', 'season_name'])['event'].sum().reset_index()
        shots_AS['game_state'] = 'AS'
        shots = pd.concat([shots_, shots_AS], axis=0)
        shots = shots.rename(columns={"event": 'shots against', "goalie_team_id": "team_id"})

        shots_for = df.groupby(['player_team_id', 'season_name', 'game_state'])['event'].count().reset_index()
        shots_AS_for = shots_for.groupby(['player_team_id', 'season_name'])['event'].sum().reset_index()
        shots_AS_for['game_state'] = 'AS'
        shots_for_ = pd.concat([shots_for, shots_AS_for], axis=0)
        shots_for_ = shots_for_.rename(columns={"event": 'shots', "player_team_id": "team_id"})

        goals_ = df.groupby(['goalie_team_id', 'season_name', 'game_state'])['xG', 'event'].sum().reset_index()
        goals_AS = goals_.groupby(['goalie_team_id', 'season_name'])['xG', 'event'].sum().reset_index()
        goals_AS['game_state'] = 'AS'
        goals = pd.concat([goals_, goals_AS], axis=0)
        goals = goals.rename(columns={"xG": "xGA", "event": 'goals against', "goalie_team_id": "team_id"})

        goals_for = df.groupby(['player_team_id', 'season_name', 'game_state'])['xG', 'event'].sum().reset_index()
        goals_AS_for = goals_for.groupby(['player_team_id', 'season_name'])['xG', 'event'].sum().reset_index()
        goals_AS_for['game_state'] = 'AS'
        goals_for_ = pd.concat([goals_for, goals_AS_for], axis=0)
        goals_for_ = goals_for_.rename(columns={"xG": "xGF", "event": 'goals', "player_team_id": "team_id"})

        final = shots.merge(shots_for_, how='left', on=['team_id', 'season_name', 'game_state'])
        final = final.merge(goals, how='left', on=['team_id', 'season_name', 'game_state'])
        final = final.merge(goals_for_, how='left', on=['team_id', 'season_name', 'game_state'])

        final['xGF%'] = final['xGF'] / (final['xGF'] + final['xGA'])
        final['GF%'] = final['goals'] / (final['goals'] + final['goals against'])
        final['SF%'] = final['shots'] / (final['shots'] + final['shots against'])

        final = final.merge(teamplate, how='left', left_on='team_id', right_on='id')
        final = final[final.team_name != "GAT"]

        final.sort_values(by=["game_state", "GF%"], ascending=[True, False]).head(20)
        return final

    @staticmethod
    def get_team_GP(df):

        df.team_name = df.team_name.str.upper()
        df.home_team = df.home_team.str.upper()
        df.away_team = df.away_team.str.upper()

        df.loc[(df.team_name == 'SOO'), 'team_name'] = 'SSM'
        df.loc[(df.home_team == 'SOO'), 'home_team'] = 'SSM'
        df.loc[(df.away_team == 'SOO'), 'away_team'] = 'SSM'

        hold = df.groupby(['home_team', 'season_name'])['game_id'].unique().reset_index()
        hold['game_id'] = hold['game_id'].str.len()
        hold = hold.rename(columns={"game_id": 'GP', 'home_team': 'away_team'})

        hold_2 = df.groupby(['away_team', 'season_name'])['game_id'].unique().reset_index()
        hold_2['game_id'] = hold_2['game_id'].str.len()
        hold_2 = hold_2.rename(columns={"game_id": 'GP'})

        hold = hold.merge(hold_2, how='left', on=['away_team', 'season_name'])
        hold['GP'] = hold['GP_x'] + hold['GP_y']

        hold = get_season_and_season_type(hold)
        hold = hold.rename(columns = {"away_team":"team_name"})

        return hold

    @staticmethod
    def fix_league_and_team(df: pd.DataFrame):
        df.league = df.league.str.upper()
        df.team_name = df.team_name.str.upper()

        return df

    @staticmethod
    def team_clean_prep(df):
        df = df.dropna()
        df = df.reset_index(drop=True)
        df = df.round(3)
        df = df.sort_values(by="Team", ascending=False)

        df['xGF%'] = df['xGF%'] * 100
        df['xGF%'] = df['xGF%'].round(2)

        df['GF%'] = df['GF%'] * 100
        df['GF%'] = df['GF%'].round(2)

        df['SF%'] = df['SF%'] * 100
        df['SF%'] = df['SF%'].round(2)

        return df

    @staticmethod
    def team_fix_columns_and_upper(df):
        df = df[TEAM_OG_COLS]
        df.columns = TEAM_RENAMED_COLS
        df = df[TEAM_COLS]

        return df

    def __call__(self):

        df_ohl, teamplate_ohl, df_qmjhl, teamplate_qmjhl = self.load_team_data()

        hold_qmjhl = self.get_team_GP(df_qmjhl)
        hold_ohl = self.get_team_GP(df_ohl)

        teams_qmjhl = self.get_team_table_df(df_qmjhl, teamplate_qmjhl)
        teams_qmjhl['league'] = 'qmjhl'

        teams_ohl = self.get_team_table_df(df_ohl, teamplate_ohl)
        teams_ohl['league'] = 'ohl'

        teams_ohl = get_season_and_season_type(teams_ohl)
        teams_qmjhl = get_season_and_season_type(teams_qmjhl)

        teams_ohl.team_name = teams_ohl.team_name.str.upper()
        teams_qmjhl.team_name = teams_qmjhl.team_name.str.upper()

        teams_qmjhl = teams_qmjhl.merge(hold_qmjhl, how='left', on = ['team_name','season','season_type'])
        teams_ohl = teams_ohl.merge(hold_ohl, how='left', on = ['team_name','season','season_type'])

        teams_final = pd.concat([teams_ohl,teams_qmjhl],axis=0)
        teams_final = self.fix_league_and_team(teams_final)

        teams_final = self.team_fix_columns_and_upper(teams_final)
        teams_final = self.team_clean_prep(teams_final)

        return teams_final

