import pandas as pd
from helper_css_functions import discrete_background_color_bins


def team_dropdown_lists(df):

    Teams_ = df.Team.unique().tolist()
    Seasons_ = df.Season.unique().tolist()
    Season_Types_ = df['Season Type'].unique().tolist()
    Leagues_ = df.League.unique().tolist()
    Situations_ = df['Game State'].unique().tolist()

    return Teams_, Seasons_, Season_Types_, Leagues_, Situations_


def team_dynamic_dropdown_output(df_master,
           Situations,
           Teams,
           Season_Types,
           Seasons,
           Leagues, Situations_, Teams_, Season_Types_, Seasons_, Leagues_):

    df_master_copy = df_master.copy()

    filters = [Situations, Teams,Season_Types, Seasons, Leagues]
    cols = ['Game State', 'Team','Season Type','Season', 'League']

    filters_ = [Situations_, Teams_, Season_Types_, Seasons_, Leagues_]

    for i in range(len(filters)):
        if filters[i]:
            df_master_copy = df_master_copy[df_master_copy[cols[i]].isin(filters[i])]
            filters[i] = [{'label': x, 'value': x} for x in filters_[i]]

    for i in range(len(filters)):
        if not filters[i]:
            filters[i] = [{'label': x, 'value': x} for x in df_master_copy[cols[i]].unique().tolist()]


    (xg_styles, legend) = discrete_background_color_bins(df_master_copy, columns=['xGF'])

    Situations = filters[0]
    Teams = filters[1]
    Season_Types = filters[2]
    Seasons = filters[3]
    Leagues = filters[4]

    return Situations, Teams, Season_Types, Seasons, Leagues, df_master_copy, xg_styles, legend



def get_team_totals(df_2):

    TOTALS_COLS_HOLD = ['Team','League', 'Season Type','Game State']
    SUM_COLS =    ['GP','xGF','xGA','GF','GA','Shots','Shots Against']
    MERGE_COLS = ['Team','League', 'Season Type','Game State']
    MERGE_COLS_HOLD = TOTALS_COLS_HOLD

    df_hold_1 = df_2.groupby(TOTALS_COLS_HOLD)[SUM_COLS].sum().reset_index()

    df_2['Season'] = df_2['Season'].apply(lambda x: x + ',')
    df_hold_3 = df_2.groupby(TOTALS_COLS_HOLD)['Season'].unique().reset_index()
    merge_1_A = df_hold_1.merge(df_hold_3, how='left', on = MERGE_COLS)
    merge_2 = merge_1_A.merge(df_2[MERGE_COLS_HOLD], how='left', on = MERGE_COLS)

    merge_2['xGF%'] = merge_2['xGF'] / (merge_2['xGF'] + merge_2['xGA'])
    merge_2['GF%'] = merge_2['GF'] / (merge_2['GF'] + merge_2['GA'])
    merge_2['SF%'] = merge_2['Shots'] / (merge_2['Shots'] + merge_2['Shots Against'])

    merge_2 = pd.DataFrame(merge_2)
    merge_2['xGF'] = merge_2['xGF'].round(2)
    merge_2['xGA'] = merge_2['xGA'].round(2)
    merge_2['xGF%'] = merge_2['xGF%']*100
    merge_2['GF%']  = merge_2['GF%']*100
    merge_2['SF%'] = merge_2['SF%']*100
    merge_2['xGF%'] = merge_2['xGF%'].round(2)
    merge_2['GF%']  = merge_2['GF%'] .round(2)
    merge_2['SF%'] = merge_2['SF%'].round(2)
    merge_2.drop_duplicates(subset=TOTALS_COLS_HOLD, inplace=True)
    df_2 = merge_2

    df_2['Season'] = df_2['Season'].astype(str)
    df_2['Season'] = df_2['Season'].apply(lambda x: x.replace('[',""))
    df_2['Season'] = df_2['Season'].apply(lambda x: x.replace(' ',""))
    df_2['Season'] = df_2['Season'].apply(lambda x: x.replace(']',"").strip())
    df_2['Season'] = df_2['Season'].apply(lambda x: x.replace("'","").strip())
    df_2['Season'] = df_2['Season'].apply(lambda x: x[:-1])

    return df_2







