import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html

row_1 = html.Div(id='row_1', children=[
    dbc.Row([
        dbc.Col([
            html.Label(['SELECT SEASON',
                        dcc.Checklist(id='Seasons',
                                      labelStyle={'display': 'inline-block', 'color': 'black',
                                                  'font-size': '13px'},
                                      value=["2019-20", "2020-21"]
                                      )])],
            xs=12, sm=12, md=12, lg=4, xl=4),
        dbc.Col([
            html.Label(['SELECT SEASON TYPE',
                        dcc.Dropdown(className="dropdown--style",
                                     id='Season_Types',
                                     multi=True,
                                     placeholder="Season Type")])]
            , xs=12, sm=12, md=12, lg=4, xl=4),

        dbc.Col([
            html.Label(['SELECT STRENGTH',
                        dcc.Dropdown(className="dropdown--style",
                                     id='Situations',
                                     multi=True,
                                     value=["EV"],
                                     placeholder="Game Strength")])]
            , xs=12, sm=12, md=12, lg=4, xl=4),
    ], justify="center")])

row_2 = html.Div(id='row_2', children=[
    dbc.Row([
        dbc.Col([
            html.Label(['SELECT A LEAGUE',
                        dcc.Dropdown(className="dropdown--style",
                                     id='Leagues',
                                     multi=True,
                                     placeholder="Leagues")])]
            , xs=10, sm=8, md=5, lg=6, xl=3),
        dbc.Col([
            html.Label(['SELECT A TEAM',
                        dcc.Dropdown(className="dropdown--style",
                                     id='Teams',
                                     multi=True,
                                     placeholder="Teams")])]
            , xs=10, sm=8, md=5, lg=6, xl=3),

        dbc.Col([
            html.Label(['SELECT A PLAYER',
                        dcc.Dropdown(className="dropdown--style",
                                     id='Players',
                                     multi=True,
                                     placeholder="Players")])]
            , xs=10, sm=8, md=5, lg=6, xl=3),
        dbc.Col([
            html.Label(['SELECT A POSITION',
                        dcc.Dropdown(className="dropdown--style",
                                     id='Positions',
                                     multi=True,
                                     placeholder="Position")])]
            , xs=12, sm=12, md=12, lg=4, xl=3),
    ], )])

row_3 = html.Div(id='row_3', children=[
    dbc.Row([
        dbc.Col([
            html.Label(["xG COLOR SCALE",
                        html.Div(id='legend2')])],
            xs=12, sm=12, md=12, lg=6, xl=4),
        dbc.Col([
            html.Label(['ADD ADDITIONAL COLUMNS',
                        dcc.Checklist(id='Table_type',
                                      options=[{'label': 'Player Information', 'value': 'Yes'},
                                               {'label': 'On-Ice', 'value': 'On-Ice'}],
                                      labelStyle={'display': 'inline-block', 'color': 'black',
                                                  'font-size': '13px'},
                                      )], style={})],
            xs=12, sm=12, md=12, lg=6, xl=4),
        dbc.Col([
            html.Label(['SELECT DISPLAY',
                        dcc.Checklist(id='groupby',
                                      options=[{'label': 'Totals', 'value': 'totals'}],
                                      labelStyle={'display': 'inline-block', 'color': 'black',
                                                  'font-size': '13px'},

                                      )], style={})],
            xs=12, sm=12, md=12, lg=6, xl=4)
    ])
])