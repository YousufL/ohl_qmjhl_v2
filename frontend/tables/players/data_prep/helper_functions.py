from helper_css_functions import discrete_background_color_bins
import pandas as pd


def dropdown_lists(df):

    Teams_ = df.Team.unique().tolist()
    Players_ = df['Name'].unique().tolist()
    Positions_ = df.Pos.unique().tolist()
    Seasons_ = df.Season.unique().tolist()
    Season_Types_ = df['Season Type'].unique().tolist()
    Leagues_ = df.League.unique().tolist()
    Situations_ = df['Game State'].unique().tolist()

    return Teams_, Players_, Positions_, Seasons_, Season_Types_, Leagues_, Situations_

def dynamic_dropdown_output(df_master,
           Situations,
           Teams,
           Players,
           Season_Types,
           Positions,
           Seasons,
           Leagues, Situations_, Teams_, Players_, Season_Types_, Positions_, Seasons_, Leagues_):

    df_master_copy = df_master.copy()
    Player_hold = Players

    filters = [Situations, Teams, Players, Season_Types, Positions, Seasons, Leagues]
    cols = ['Game State', 'Team', 'Name', 'Season Type', 'Pos', 'Season', 'League']

    filters_ = [Situations_, Teams_, Players_, Season_Types_, Positions_, Seasons_, Leagues_]

    for i in range(len(filters)):
        if filters[i]:
            df_master_copy = df_master_copy[df_master_copy[cols[i]].isin(filters[i])]
            filters[i] = [{'label': x, 'value': x} for x in filters_[i]]

    for i in range(len(filters)):
        if not filters[i]:
            filters[i] = [{'label': x, 'value': x} for x in df_master_copy[cols[i]].unique().tolist()]

    if (not Player_hold):
        (xg_styles, legend) = discrete_background_color_bins(df_master_copy, columns=['xG'])

    if Player_hold:
        (xg_styles, legend) = discrete_background_color_bins(df_master, columns=['xG'])


    Situations = filters[0]
    Teams = filters[1]
    Players = filters[2]
    Season_Types = filters[3]
    Positions = filters[4]
    Seasons = filters[5]
    Leagues = filters[6]

    return Situations, Teams, Players, Season_Types, Positions, Seasons, Leagues, df_master_copy, xg_styles, legend



def get_totals(df_2):

    TOTALS_COLS_HOLD = ['Name','Pos','Shoots','Height','League','Game State','Season Type']
    SUM_COLS = ['GP','xG', 'G', 'A1', 'A2', 'Points','Shots', 'FO Wins','FO Attempts','+/-', 'Pim', 'On-Ice G', 'On-Ice GA','Off-Ice G', 'Off-Ice GA', 'Points']
    MERGE_COLS = ['Name','Pos','Shoots','Height','League','Game State','Season Type']
    INFO_COLS_HOLD = ['Draft Type', 'Draft Year', 'Draft Team','Round', 'Pick', 'Birthday']
    MERGE_COLS_HOLD = TOTALS_COLS_HOLD + INFO_COLS_HOLD

    df_hold_1 = df_2.groupby(TOTALS_COLS_HOLD)[SUM_COLS].sum().reset_index()
    df_hold_2 = df_2.groupby(TOTALS_COLS_HOLD)['Team'].unique().reset_index()

    df_2['Season'] = df_2['Season'].apply(lambda x: x + ',')
    df_hold_3 = df_2.groupby(TOTALS_COLS_HOLD)['Season'].unique().reset_index()
    merge_1 = df_hold_1.merge(df_hold_2, how='left', on = MERGE_COLS)
    merge_1_A = merge_1.merge(df_hold_3, how='left', on = MERGE_COLS)
    merge_2 = merge_1_A.merge(df_2[MERGE_COLS_HOLD], how='left', on = MERGE_COLS)

    merge_2['On-Ice GD'] = merge_2['On-Ice G'] - merge_2['On-Ice GA']
    merge_2['Off-Ice GD'] = merge_2['Off-Ice G'] - merge_2['Off-Ice GA']
    merge_2['FO %'] = merge_2['FO Wins'] / (merge_2['FO Wins'] + merge_2['FO Attempts'])
    merge_2["FO %"] = merge_2["FO %"] * 100
    merge_2["FO %"] = merge_2["FO %"].round(2)
    merge_2['On vs Off Differential'] = merge_2["On-Ice GD"] - merge_2['Off-Ice GD']
    merge_2['xG'] = merge_2['xG'].round(2)
    merge_2 = pd.DataFrame(merge_2)
    merge_2.drop_duplicates(subset=TOTALS_COLS_HOLD, inplace=True)
    df_2 = merge_2

    df_2['Season'] = df_2['Season'].astype(str)
    df_2['Team'] = df_2['Team'].astype(str)

    df_2['Season'] = df_2['Season'].apply(lambda x: x.replace('[',""))
    df_2['Season'] = df_2['Season'].apply(lambda x: x.replace(' ',""))
    df_2['Season'] = df_2['Season'].apply(lambda x: x.replace(']',"").strip())
    df_2['Season'] = df_2['Season'].apply(lambda x: x.replace("'","").strip())
    df_2['Season'] = df_2['Season'].apply(lambda x: x[:-1])

    df_2['Team'] = df_2['Team'].apply(lambda x: x.replace('[',""))
    df_2['Team'] = df_2['Team'].apply(lambda x: x.replace(' ',","))
    df_2['Team'] = df_2['Team'].apply(lambda x: x.replace(']',"").strip())
    df_2['Team'] = df_2['Team'].apply(lambda x: x.replace("'","").strip())

    return df_2


