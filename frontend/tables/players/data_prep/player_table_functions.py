import random
import re
import ast
from sqlalchemy import create_engine
from general_functions import to_1D

import sys
sys.path.extend(['frontend/tables/players'])

OG_COLS = ['name', 'position', 'shoots', 'height' ,'season_type' ,'season' ,'player_team_code' ,'league' ,'GP',
            'game_state',
            'xG',
            'goals' ,'A1' ,'A2',
            'shots',
            'faceoff_wins', 'faceoff_attempts',"FO %",
            'plusminus', 'pim',
            'on_ice_goals', 'on_ice_goals_against',
            'on_ice_GD',
            'off_ice_team_goals', 'off_ice_team_goals_against',
            'off_ice_GD', 'on_vs_off_diff',
            'Draft', 'year' ,'team', 'round',
            'pick', 'birthday'
           ]

RENAMED_COLS = ['Name', 'Pos', 'Shoots', 'Height', 'Season Type', 'Season', 'Team', 'League', 'GP',
                'Game State',
                'xG',
                'G', 'A1', 'A2',
                'Shots',
                'FO Wins', 'FO Attempts', "FO %",
                '+/-', 'Pim',
                'On-Ice G', 'On-Ice GA', 'On-Ice GD',
                'Off-Ice G', 'Off-Ice GA',
                'Off-Ice GD', 'On vs Off Differential',
                'Draft Type', 'Draft Year', 'Draft Team', 'Round',
                'Pick', 'Birthday'
                ]

import pandas as pd
import numpy as np

regex = re.compile('[^a-zA-Z]')



class PlayerTable:
    def load_data(self):

        engine = create_engine('postgresql://super:super123@localhost:9999/postgres')
        #query = "SELECT * FROM raw_ohl WHERE season_name ='2020-2021 Regular Season;"

        query_1 = "SELECT * FROM ohl.raw_ohl;"
        df_ohl = pd.read_sql(query_1, engine)

        query_2 = "SELECT * FROM ohl.stats_ohl;"
        static_ohl = pd.read_sql(query_2, engine)

        query_3 = "SELECT * FROM ohl.nameplate_ohl;"
        nameplate_ohl = pd.read_sql(query_3, engine)

        query_4 = "SELECT * FROM ohl.teamplate_ohl;"
        teamplate_ohl = pd.read_sql(query_4, engine)

        query_5 = "SELECT * FROM qmjhl.raw_qmjhl;"
        df_qmjhl = pd.read_sql(query_5, engine)

        query_6 = "SELECT * FROM qmjhl.stats_qmjhl;"
        static_qmjhl = pd.read_sql(query_6, engine)

        query_7 = "SELECT * FROM qmjhl.nameplate_qmjhl;"
        nameplate_qmjhl = pd.read_sql(query_7, engine)

        query_8 = "SELECT * FROM qmjhl.teamplate_qmjhl;"
        teamplate_qmjhl = pd.read_sql(query_8, engine)

        return df_ohl, static_ohl, nameplate_ohl, teamplate_ohl, df_qmjhl, static_qmjhl, nameplate_qmjhl, teamplate_qmjhl

    @staticmethod
    def get_faceoff_percentage(df: pd.DataFrame):

        df['FO %'] = df['faceoff_wins']/ (df['faceoff_attempts'] + df['faceoff_wins'])
        df["FO %"] = df["FO %"]*100
        df["FO %"] = df["FO %"].round(2)

        return df

    @staticmethod
    def get_on_vs_off_ice(df: pd.DataFrame, teamplate: pd.DataFrame):

        df = df.copy()
        hold1 = df[df.event==1].reset_index(drop=True)

        hold1['plus'] = hold1['plus'].apply(lambda x: ast.literal_eval(x))
        hold1['minus'] = hold1['minus'].apply(lambda x: ast.literal_eval(x))

        hold1['plus'] = hold1['plus'].apply(lambda x: [y['player_id'] for y in x])
        hold1['minus'] = hold1['minus'].apply(lambda x: [y['player_id'] for y in x])

        hold1['home_roster'] = hold1['home_roster'].apply(lambda x: ast.literal_eval(x))
        hold1['away_roster'] = hold1['away_roster'].apply(lambda x: ast.literal_eval(x))

        hold1['home_roster'] = hold1['home_roster'].apply(lambda x: [int(y) for y in x])
        hold1['away_roster'] = hold1['away_roster'].apply(lambda x: [int(y) for y in x])

        teamplate['team_name'] = teamplate['team_name'].str.upper()

        list_ = []
        for game_id, sub_df in hold1.groupby('game_id'):
            for game_state in ['EV', 'PP', 'SH']:


                d = []
                d.append({'team_name': sub_df['home_team'].unique()[0]})
                d.append({'team_name': sub_df['away_team'].unique()[0]})

                hold = pd.DataFrame(d)
                hold['game_state'] = game_state

                hold = hold.merge(teamplate, how='left', on = 'team_name')

                sub_df_ = sub_df[sub_df.game_state == game_state]
                if len(sub_df_) != 0:

                    sub_df_hold = sub_df_.groupby('player_team_id')['event'].sum().reset_index()
                    sub_df_hold = sub_df_hold.rename(columns = {'event':'goals','player_team_id':'id'})
                    sub_df_hold =  hold.merge(sub_df_hold, how='left', on = 'id')
                    sub_df_hold['game_id'] = game_id

                    sub_df_hold['season_name'] = sub_df['season_name'].unique()[0]
                    sub_df_hold = sub_df_hold.fillna(0)

                    try:
                        away_goals = sub_df_hold[sub_df_hold.team_name != sub_df_['home_team'].unique()[0]]['goals'].values
                        home_goals = sub_df_hold[sub_df_hold.team_name != sub_df_['away_team'].unique()[0]]['goals'].values
                    except IndexError:
                        away_goals = 0
                        home_goals = 0

                    sub_df_hold['goals_against'] = 0
                    sub_df_hold.loc[(sub_df_hold.team_name == sub_df['home_team'].unique()[0]), 'goals_against' ]= away_goals
                    sub_df_hold.loc[(sub_df_hold.team_name == sub_df['away_team'].unique()[0]), 'goals_against' ]= home_goals

                    plus = to_1D(sub_df_['plus']).value_counts().reset_index()
                    plus = plus.rename(columns = {"index":"player_id", 0:'plus'})
                    plus['player_id'] = plus['player_id'].astype(int)

                    minus = to_1D(sub_df_['minus']).value_counts().reset_index()
                    minus = minus.rename(columns = {"index":"player_id", 0:'minus'})
                    minus['player_id'] = minus['player_id'].astype(int)

                    away_roster = pd.DataFrame(sub_df.reset_index()['away_roster'][0])
                    away_roster = away_roster.rename(columns = {0:"player_id"})
                    away_roster['team_name'] = sub_df['away_team'].unique()[0]
                    away_roster = away_roster.merge(teamplate, how='left',on='team_name')
                    away_roster = away_roster.merge(plus,how='left',on='player_id')
                    away_roster = away_roster.merge(minus,how='left',on='player_id')

                    home_roster = pd.DataFrame(sub_df.reset_index()['home_roster'][0])
                    home_roster = home_roster.rename(columns = {0:"player_id"})
                    home_roster['team_name'] = sub_df['home_team'].unique()[0]
                    home_roster = home_roster.merge(teamplate, how='left',on='team_name')
                    home_roster = home_roster.merge(plus,how='left',on='player_id')
                    home_roster = home_roster.merge(minus,how='left',on='player_id')

                    final = pd.concat([home_roster,away_roster],axis=0)
                    final = final.merge(sub_df_hold[['id','goals','goals_against','game_id','season_name','game_state']],how='left', on='id')
                    final = final.fillna(0)

                    list_.append(final)

        on_df = pd.concat(list_)
        on_df_ = on_df.groupby(['player_id', 'season_name','team_name','game_state'])['plus','minus','goals','goals_against'].sum().reset_index()

        on_df_AS = on_df_.groupby(['player_id', 'season_name', 'team_name'])['plus','minus','goals','goals_against'].sum().reset_index()
        on_df_AS['game_state'] = 'AS'
        on_df_ = pd.concat([on_df_, on_df_AS], axis=0)

        on_df_['on_ice_GD'] = on_df_['plus'] - on_df_['minus']
        on_df_['off_ice_GD'] = on_df_['goals'] - on_df_['goals_against']

        on_df_['on_vs_off_diff'] = on_df_['on_ice_GD'] - on_df_['off_ice_GD']
        on_df_.sort_values('on_vs_off_diff', ascending=True)

        on_df_ = on_df_.rename(columns = {'plus':'on_ice_goals', 'minus':'on_ice_goals_against', 'goals':'off_ice_team_goals', 'goals_against': 'off_ice_team_goals_against'})

        return on_df_

    @staticmethod
    def player_get_gp_total(df: pd.DataFrame, teamplate: pd.DataFrame) -> pd.DataFrame:

        df = df.copy()

        roster_df = df[['game_id', 'season_name', 'home_roster', 'away_roster', 'home_team',
                        'away_team']].drop_duplicates().reset_index(drop=True)
        roster_df['home_roster'] = roster_df['home_roster'].apply(lambda x: ast.literal_eval(x))
        roster_df['away_roster'] = roster_df['away_roster'].apply(lambda x: ast.literal_eval(x))

        seasons = list(roster_df['season_name'].unique())
        teams = list(roster_df['home_team'].unique())

        d = []
        for k in range(len(seasons)):
            hold_roster_df = roster_df[roster_df.season_name == seasons[k]].reset_index(drop=True)

            for b in range(len(teams)):
                team_home_roster_df = hold_roster_df[hold_roster_df.home_team == teams[b]].reset_index(drop=True)
                z = to_1D(team_home_roster_df['home_roster']).value_counts().reset_index()

                if len(z) > 0:
                    z['team'] = teams[b]
                    z['season_name'] = seasons[k]
                    d.append(z)

                team_away_roster_df = hold_roster_df[hold_roster_df.away_team == teams[b]].reset_index(drop=True)
                g = to_1D(team_away_roster_df['away_roster']).value_counts().reset_index()

                if len(g) > 0:
                    g['team'] = teams[b]
                    g['season_name'] = seasons[k]
                    d.append(g)

        data = pd.concat(d)
        data.reset_index(drop=True, inplace=True)
        data = data.rename(columns={"index": 'player_id', 0: "GP"})

        teamplate['team_name'] = teamplate['team_name'].str.upper()
        data['team'] = data['team'].str.upper()

        data = data.merge(teamplate, left_on="team", right_on="team_name")
        data = data.drop(columns=['team'])

        gp_totals = data.groupby(['player_id', 'season_name', 'id'])['GP'].sum().reset_index()
        gp_totals = gp_totals.rename(columns = {"id":'team_id'})

        return gp_totals

    @staticmethod
    def get_player_table_df(df: pd.DataFrame, static: pd.DataFrame, nameplate: pd.DataFrame,
                            player_gp_totals: pd.DataFrame, teamplate: pd.DataFrame, on_vs_off_ice_df: pd.DataFrame) -> pd.DataFrame:
        df = df.copy()
        df['player_team_code'] = df['player_team_code'].str.upper()
        teamplate['team_name'] = teamplate['team_name'].str.upper()

        shots_ = df.groupby(['player_id', 'season_name','player_team_code', 'game_state'])['event'].count().reset_index()
        shots_AS = shots_.groupby(['player_id', 'season_name','player_team_code'])['event'].sum().reset_index()
        shots_AS['game_state'] = 'AS'
        shots = pd.concat([shots_, shots_AS], axis=0)
        shots = shots.rename(columns={"event": 'shots'})

        goals_ = df.groupby(['player_id', 'season_name','player_team_code', 'game_state'])['xG', 'event'].sum().reset_index()
        goals_AS = goals_.groupby(['player_id', 'season_name','player_team_code'])['xG', 'event'].sum().reset_index()
        goals_AS['game_state'] = 'AS'
        goals = pd.concat([goals_, goals_AS], axis=0)
        goals = goals.rename(columns={"event": 'goals'})

        assist2 = df[df.event == 1].groupby(['season_name', 'player_team_code', 'game_state'])['assist2_player_id'].value_counts().reset_index(level=0)
        assist2= assist2.rename(columns = {"assist2_player_id":"A2"})
        assist2 = assist2.reset_index()
        assist2 = assist2.rename(columns={"assist2_player_id": "player_id"})

        assist2_AS = assist2.groupby(['player_id', 'season_name', 'player_team_code'])['A2'].sum().reset_index()
        assist2_AS['game_state'] = 'AS'
        assist2 = pd.concat([assist2, assist2_AS], axis=0)

        assist1 = df[df.event == 1].groupby(['season_name', 'player_team_code', 'game_state'])['assist1_player_id'].value_counts().reset_index(level=0)
        assist1 = assist1.rename(columns = {"assist1_player_id":"A1"})
        assist1 = assist1.reset_index()
        assist1 = assist1.rename(columns={"assist1_player_id": "player_id"})

        assist1_AS = assist1.groupby(['player_id', 'season_name', 'player_team_code'])['A1'].sum().reset_index()
        assist1_AS['game_state'] = 'AS'
        assist1 = pd.concat([assist1, assist1_AS], axis=0)

        assist = assist1.merge(assist2, how = 'left', on = ['season_name','player_team_code','game_state','player_id'])
        assist = assist.fillna(0)

        goals['player_id'] = goals['player_id'].astype(float)
        assist['player_id'] = assist['player_id'].astype(float)
        on_vs_off_ice_df['player_id'] =on_vs_off_ice_df['player_id'].astype(float)
        static['player_id'] = static['player_id'].astype(float)

        final = shots.merge(goals, how='left', on=['player_id', 'season_name', 'player_team_code','game_state'])

        final['player_id'] = final['player_id'].astype(float)
        final = final.merge(assist, how='left', on=['player_id', 'season_name','player_team_code', 'game_state'])

        on_vs_off_ice_df = on_vs_off_ice_df.rename(columns = {"team_name":"player_team_code"})
        on_vs_off_ice_df['player_id'] = on_vs_off_ice_df['player_id'].astype(float)

        final['player_id'] = final['player_id'].astype(float)
        final = final.merge(on_vs_off_ice_df, how='left', on=['player_id', 'season_name', 'player_team_code', 'game_state'])

        seasons = df[['season_name', 'season_id']]
        seasons = seasons.drop_duplicates()
        static = static.merge(seasons, on='season_id')

        static['faceoff_wins'] = static['faceoff_wins'].astype(int)
        static['faceoff_attempts'] = static['faceoff_attempts'].astype(int)
        static['plusminus'] = static['plusminus'].astype(int)
        static['hits'] = static['hits'].astype(int)
        static['shots'] = static['shots'].astype(int)
        static['shots_on'] = static['shots_on'].astype(int)

        static = static.groupby(['player_id', 'season_name','team_id'])['plusminus',
                                                              'pim',
                                                              'faceoff_wins',
                                                              'faceoff_attempts',
                                                              'hits', 'shots',
                                                              'shots_on'].sum().reset_index()

        static = static.rename(columns={'shots': 'iCF'})

        final = final.merge(teamplate[['id', 'team_name']], how='left', left_on='player_team_code', right_on='team_name')
        final = final.drop(columns =['team_name'])
        final = final.rename(columns = {"id":'team_id'})

        final = final.merge(static, how='left', on=['player_id', 'season_name','team_id'])

        final = final.merge(nameplate[['player_id', 'name','position', 'shoots', 'height', 'birthday', 'Draft','year','round','pick','team']], how='left',
                            on='player_id')

        player_gp_totals['player_id'] = player_gp_totals['player_id'].astype(float)
        final['player_id'] = final['player_id'].astype(float)

        final = final.merge(player_gp_totals, how='left', on=['player_id', 'season_name','team_id'])
        final['A1'] = final['A1'].fillna(0)
        final['A2'] = final['A2'].fillna(0)

        return final

    @staticmethod
    def fix_columns_and_upper(df):

        df = df[OG_COLS]
        df.columns = RENAMED_COLS
        df['Points'] = df['G'] + df['A1'] +df['A2']

        return df

    @staticmethod
    def clean_prep(df):

        df = df.dropna()
        df = df.reset_index(drop=True)
        df = df.round(3)
        df = df[df.Pos != 'G']
        df = df.sort_values(by="Name", ascending=False)
        df.xG = df.xG.round(2)

        df.loc[(df['Draft Type'] == 0, 'Draft Type')] = None
        df.loc[(df['Draft Year'] == 0, 'Draft Year')] = None
        df.loc[(df['Draft Team'] == 0, 'Draft Team')] = None
        df.loc[(df['Round'] == 0, 'Round')] = None
        df.loc[(df['Pick'] == 0, 'Pick')] = None

        return df

    @staticmethod
    def to_1D(series):
        return pd.Series([x for _list in series for x in _list])

    @staticmethod
    def get_season_and_season_type(df: pd.DataFrame):

        df['season_name'] = df['season_name'].apply(lambda x: x.replace("|", ""))
        df['season_name'] = df['season_name'].apply(lambda x: x.split(" "))

        df['season_type'] = np.where((df.season_name.str.len() == 3),
                                     df.season_name.str[1] + ' ' + df.season_name.str[2],
                                     np.where((df.season_name.str.len() == 2),
                                              df.season_name.str[1],
                                              np.where((df.season_name.str.len() == 4),
                                                       df.season_name.str[2] + ' ' + df.season_name.str[3],
                                                       np.where((df.season_name.str.len() == 3),
                                                                df.season_name.str[2],
                                                                df.season_name))))

        df['season_type'] = df['season_type'].apply(lambda x: x.strip())
        df['season'] = df.season_name.apply(lambda x: x[0])

        CLEAN_DICT = {
            '2015': '2014-15',
            '2016': '2015-16',
            '2017': '2016-17',
            '2018': '2017-18',
            '2019': '2018-19',
            '2020': '2019-20',
            '2021': '2020-21',
        }

        for wrong, correct in CLEAN_DICT.items():
            df.loc[(df.season == wrong), 'season'] = correct

        return df

    @staticmethod
    def fix_league_and_team(df: pd.DataFrame):


        df.league = df.league.str.upper()
        df.team = df.team.astype(str)
        df.team = df.team.apply(lambda x: regex.sub('', x))

        correct_leagues = ['Cha', 'CHA', 'She', 'BLB', 'SNB', 'Que', 'Qu', 'KIT', 'ER', 'PLY', 'OSH', 'GUE', 'SBY',
                           'BRA',
                           '', 'MISS', 'WSR',
                           'SAR', 'BAR', 'OS', 'BELV', 'KGN', 'SSM', 'EDM', 'NIAG', 'OTT',
                           'BRAM', 'LDN', 'SAG', 'PBO', 'NB', 'FLNT', 'HAM', 'Miss', 'Sar',
                           'Bat', 'BaC', 'Hal', 'Dru', 'Gat', 'Lew', 'Mon', 'PEI', 'Rou',
                           'Sha', 'VdO', 'Vic', 'Rim', 'Mtl', 'Chi', 'Cap']

        df.loc[~(df.team.isin(correct_leagues)), 'team'] = ''
        df.team = df.team.str.upper()

        if 'goalie_team_code' in df.columns:
            df.goalie_team_code = df.goalie_team_code.str.upper()

        if 'player_team_code' in df.columns:
            df.player_team_code = df.player_team_code.str.upper()

        return df

    @staticmethod
    def fix_height(df: pd.DataFrame):

        df = df.dropna(subset=['height'])
        df.loc[(df.height == '6'), 'height'] = '6.00'
        df.height = df.height.astype(str)

        df.height = df.height.apply(lambda x: x.replace("\'", ".").split('"')[0].split(".")[0] + "'" +
                                              x.replace("\'", ".").split('"')[0].split(".")[1] + '"')

        df.loc[(df.height == '5\'1"'), 'height'] = '5\'10"'

        return df

    @staticmethod
    def remove_latest_and_replace(df):

        engine = create_engine('postgresql://super:super123@localhost:9999/postgres')
        query_qmjhl = "DELETE FROM player_table WHERE season ='2020-21' AND season_type = 'Regular Season'; "

        #engine.execute(query)


        return

    def __call__(self):

        df_ohl, static_ohl, nameplate_ohl, teamplate_ohl, df_qmjhl, static_qmjhl, nameplate_qmjhl, teamplate_qmjhl = self.load_data()
        player_gp_totals_qmjhl = self.player_get_gp_total(df_qmjhl, teamplate_qmjhl)
        on_ice_df_qmjhl = self.get_on_vs_off_ice(df_qmjhl, teamplate_qmjhl)
        players_qmjhl = self.get_player_table_df(df_qmjhl, static_qmjhl, nameplate_qmjhl, player_gp_totals_qmjhl, teamplate_qmjhl, on_ice_df_qmjhl)
        players_qmjhl['league']= 'qmjhl'

        player_gp_totals_ohl =self.player_get_gp_total(df_ohl, teamplate_ohl)
        on_ice_df_ohl = self.get_on_vs_off_ice(df_ohl, teamplate_ohl)
        players_ohl = self.get_player_table_df(df_ohl, static_ohl, nameplate_ohl, player_gp_totals_ohl, teamplate_ohl, on_ice_df_ohl)
        players_ohl['league'] = 'ohl'

        players_final = pd.concat([players_ohl,players_qmjhl],axis=0)
        players_final = self.get_season_and_season_type(players_final)
        players_final = self.fix_league_and_team(players_final)
        players_final = self.fix_height(players_final)
        players_final = self.get_faceoff_percentage(players_final)
        players_final = players_final.dropna()
        players_final = self.fix_columns_and_upper(players_final)
        players_final = self.clean_prep(players_final)

        return players_final

