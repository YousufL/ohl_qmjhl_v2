
OG_COLS = ['name', 'position', 'shoots', 'height' ,'season_type' ,'season' ,'player_team_code' ,'league' ,'GP',
            'game_state',
            'xG',
            'goals' ,'A1' ,'A2',
            'shots',
            'faceoff_wins', 'faceoff_attempts',"FO %",
            'plusminus', 'pim',
            'on_ice_goals', 'on_ice_goals_against',
            'on_ice_GD',
            'off_ice_team_goals', 'off_ice_team_goals_against',
            'off_ice_GD', 'on_vs_off_diff',
            'Draft', 'year' ,'team', 'round',
            'pick', 'birthday'
           ]

RENAMED_COLS = ['Name', 'Pos', 'Shoots', 'Height', 'Season Type', 'Season', 'Team', 'League', 'GP',
                'Game State',
                'xG',
                'G', 'A1', 'A2',
                'Shots',
                'FO Wins', 'FO Attempts', "FO %",
                '+/-', 'Pim',
                'On-Ice G', 'On-Ice GA', 'On-Ice GD',
                'Off-Ice G', 'Off-Ice GA',
                'Off-Ice GD', 'On vs Off Differential',
                'Draft Type', 'Draft Year', 'Draft Team', 'Round',
                'Pick', 'Birthday'
                ]


REG_COLS = [ 'Name','League', 'Pos','Season Type','Season','Team','GP',
    'Game State',
    'xG',
    'G','A1','A2','Points',
    'Shots',
    'FO Wins', 'FO Attempts', "FO %",
    '+/-', 'Pim']


INFO_COLS = ['Height', 'Birthday', 'Shoots','Draft Type', 'Draft Year', 'Draft Team', 'Round',
    'Pick']


ON_ICE = [ 'On-Ice G', 'On-Ice GA', 'On-Ice GD',
           'Off-Ice G', 'Off-Ice GA',
           'Off-Ice GD', 'On vs Off Differential']



SUB_COLS = ['Name','League', 'Pos','Season Type','Season','Team','GP','Game State']