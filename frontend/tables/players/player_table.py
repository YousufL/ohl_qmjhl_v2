import dash_table
import pandas as pd
import dash
from dash.dependencies import Input, Output
import dash_html_components as html
import dash_bootstrap_components as dbc
import sys

from sqlalchemy import create_engine

sys.path.extend(['frontend/tables/global_functions'])
sys.path.extend(['frontend/tables/players/data_prep'])
sys.path.extend(['frontend/tables/players/data_prep'])

from constants import REG_COLS, INFO_COLS, ON_ICE
from helper_css_functions import create_conditional_style, data_bars_diverging, discrete_background_color_bins
from player_table_children import row_1, row_2, row_3
from data_prep.helper_functions import dynamic_dropdown_output, dropdown_lists, get_totals
external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"]


engine = create_engine('postgresql://super:super123@localhost:9999/postgres')
df = pd.read_sql('players', con=engine)

Teams_, Players_, Positions_, Seasons_, Season_Types_, Leagues_, Situations_ = dropdown_lists(df_master)

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP],
                meta_tags=[{'name': 'viewport',
                            'content': 'width=device-width, initial-scale=1.0, maximum-scale=1.2, minimum-scale=0.5'}]
                )

# todo copy over to other tables

app.layout = html.Div([html.Div(id='table_title', children=[
    html.H4('Ranking every player in the OHL & QMJHL by expected goals (xG)')]),
                     row_1,
                       row_2,
                        row_3,
                       html.Div(id='line'),
                       html.Div(id='output-data-upload')])


@app.callback([Output('output-data-upload', 'children'),
              Output('Situations', 'options'),
              Output('Teams', 'options'),
              Output('Players','options'),
              Output('Season_Types', 'options'),
              Output('Positions',  'options'),
              Output('Seasons', 'options'),
              Output('Leagues',  'options'),
              Output('legend2', 'children')
              ],
			 [
              Input('Situations', 'value'),
              Input('Teams', "value"),
              Input('Players', "value"),
              Input('Season_Types', "value"),
              Input('Positions', "value"),
              Input('Seasons', "value"),
              Input('Leagues', "value"),
              Input('Table_type', "value"),
              Input('groupby', 'value'),
              ]
              )

def update_output(
                  Situations,
                  Teams,
                  Players,
                  Season_Types,
                  Positions,
                  Seasons,
                  Leagues,
                  Table_type,
                  groupby,
                  ):

    Situations, Teams, Players, Season_Types, Positions, Seasons, Leagues, df_2, xg_styles, legend = dynamic_dropdown_output(
        df_master,
        Situations,
        Teams,
        Players,
        Season_Types,
        Positions,
        Seasons,
        Leagues,
        Situations_,
        Teams_,
        Players_,
        Season_Types_,
        Positions_,
        Seasons_,
        Leagues_
    )

    if groupby == ['totals']:
        import ipdb
        ipdb.set_trace()

        df_2 = get_totals(df_2)
        (xg_styles, legend) = discrete_background_color_bins(df_2, columns=['xG'])

    styles3 = data_bars_diverging(df_2, 'On-Ice GD')
    styles4 = data_bars_diverging(df_2, 'On vs Off Differential')


    standard = [{'if': {'column_id': 'GP'}, 'border-left': '1px solid black'},
                {'if': {'column_id': 'Shots'}, 'border-right': '1px solid black'},
                {'if': {'column_id': 'League'}, 'border-left': '1px solid black'},
                {'if': {'column_id': 'Pos'}, 'border-left': '1px solid black'},
                {'if': {'column_id': 'On-Ice G'}, 'border-left': '1px solid black'},
                {'if': {'column_id': 'On-Ice G'}, 'border-left': '1px solid black'},
                {'if': {'column_id': 'On vs Off Differential'}, 'width': '180px', 'border-right': '1px solid black'},
                {'if': {'column_id': 'Name'}, 'border-left': '1px solid black'},
                {'if': {'column_id': 'Pim'}, 'border-right': '1px solid black'},
                {'if': {'column_id': 'Pick'}, 'border-right': '1px solid black'}
                ]

    if not Table_type:
        df_2 = df_2[REG_COLS]

    if Table_type == ['Yes']:
        COLS = REG_COLS + INFO_COLS
        df_2 = df_2[COLS]

    if Table_type == ['On-Ice']:
        COLS = REG_COLS + ON_ICE
        df_2 = df_2[COLS]

    if (Table_type == ['On-Ice', 'Yes']) | (Table_type == ['Yes', 'On-Ice']):
        COLS = REG_COLS + ON_ICE + INFO_COLS
        df_2 = df_2[COLS]

    styles = create_conditional_style(df_2) + standard + xg_styles + styles3 + styles4

    return dash_table.DataTable(id='table', columns=[{"name": i, "id": i} for i in df_2.columns],
                                page_current=0,
                                page_size=30,
                                page_action='native',
                                sort_action='native',
                                sort_mode='multi',
                                sort_by=[],
                                style_table={'padding-top': '5px',
                                             'height': '1000',
                                             'overflow-y': 'visible'},
                                fixed_rows={'headers': True},
                                fixed_columns={'headers': True, 'data': 1},

                                style_header={'border-bottom': '2px solid black',
                                              'text-align': 'center',
                                              'font-size': '14px',
                                              'font-weight': 'bold',
                                              'backgroundColor': '#ffffff'},
                                style_data={'text-align': 'center',
                                            'font-size': '14px',
                                            'padding-left': '3px',
                                            'padding-right': '3px',
                                            'border-bottom': '1px solid grey',
                                            'whiteSpace': 'normal'},
                                style_cell={'textAlign': 'left',
                                            'whiteSpace': 'no-wrap',
                                            'textOverflow': 'ellipsis',
                                            'border': '1px solid white'},
                                style_data_conditional=styles,
                                style_header_conditional=(
                                    {
                                        'if': {'column_id': 'xG'},
                                        'backgroundColor': '#e6e6e6'
                                    },
                                ),
                                data=df_2.to_dict(
                                    'rows')), Situations, Teams, Players, Season_Types, Positions, Seasons, Leagues, legend

if __name__ == '__main__':
    app.run_server(debug=True)