from matplotlib import pyplot
from sklearn.calibration import calibration_curve
from sklearn.model_selection import KFold
import numpy as np
from xgboost import XGBClassifier
import seaborn as sns
import pandas as pd

model = XGBClassifier(learning_rate=0.02,
                      n_estimators=600,
                      min_child_weight=5,
                      max_depth=5,
                      objective='binary:logistic',
                      silent=True,
                      nthread=1)

#### Get Calibration Curve #####
kf = KFold(n_splits=5, shuffle=True, random_state=1234)
xg_kfold_probability = []
kfold_true_label = []

# Calculate probabilities in each k-fold
for train_index, validate_index in kf.split(df):
    kfold_train, kfold_validate = df.iloc[train_index], df.iloc[validate_index]

    train_features = kfold_train[FEATURE_COLS]
    train_labels = kfold_train['event']
    validate_features = kfold_validate[FEATURE_COLS]
    validate_labels = kfold_validate['event']

    xg_model = model.fit(X=train_features, y=train_labels)
    xg_kfold_probability.append(xg_model.predict_proba(validate_features)[:, 1])
    kfold_true_label.append(validate_labels)

xg_kfold_probability_stacked = np.hstack(xg_kfold_probability)
kfold_true_label_stacked = np.hstack(kfold_true_label)
xg_y, xg_x = calibration_curve(kfold_true_label_stacked, xg_kfold_probability_stacked, n_bins=20)

### Calibration curve plot #####
plt.plot(xg_x, xg_y, marker='o', linewidth=3, label='Even Strength')
x = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
y = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
plt.plot(x, y, linewidth=2, label='Perfectly Calibrated')


### Feature Importances ###
feature_name = []
feature_scores = []
for col, score in zip(train_features.columns, model.feature_importances_):
    feature_name.append(col)
    feature_scores.append(score)

Feature_Scores = pd.concat([feature_name,feature_scores],axis=1)

### Feature Importances Plot ###
fig, ax = pyplot.subplots(figsize=(13,13))
ax = sns.barplot(y="feature_name_EV", x="feature_scores_EV", hue="feature_state", data=Feature_Scores)
ax.set_title("EV,PP and SH Feature Importances",fontsize = 16)
ax.set_xlabel("Feature Scores",fontsize = 14)
ax.set_ylabel("Feature Name",fontsize = 14)




