import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
df = pd.read_csv('../prelim_cleaned_data/test_model_ohl.csv')
df_2 = pd.read_csv('../test_model_qmjhl.csv')



pd.concat([((df_2.groupby(['season_name'])['is_rebound'].sum()/ df_2.groupby(['season_name'])['is_rebound'].count())*100).reset_index(),
           ((df.groupby(['season_name'])['is_rebound'].sum()/ df.groupby(['season_name'])['is_rebound'].count())*100).reset_index()],axis=1)

qmjhl_rebounds = ((df_2.groupby(['season_name'])['is_rebound'].sum()/ df.groupby(['season_name'])['is_rebound'].count())*100).reset_index()
ohl_rebounds = ((df.groupby(['season_name'])['is_rebound'].sum()/ df.groupby(['season_name'])['is_rebound'].count())*100).reset_index()

hold = df_2.groupby(['season_name ','game_id'])['event'].count().reset_index()
qmjhl_means = hold.groupby(['season_name'])['event'].mean()

hold = df.groupby(['season_name','game_id'])['event'].count().reset_index()
ohl_means = hold.groupby(['season_name'])['event'].mean()

list_ =[]
list_2 = []
for game_id, sub_df in df_2.groupby('game_id'):

    list_.append(sub_df['s'].diff())
    list_2.append(sub_df['season_name'])

seconds = pd.concat(list_)
seasons = pd.concat(list_2)

final = pd.concat([seconds, seasons], axis=1)
final['league'] = 'qmjhl'


list_3 = []
list_4 = []
for game_id, sub_df in df.groupby('game_id'):
    list_3.append(sub_df['s'].diff())
    list_4.append(sub_df['season_name'])


seconds2 = pd.concat(list_3)
seasons2 = pd.concat(list_4)

final2 = pd.concat([seconds2, seasons2], axis=1)
final2['league'] = 'ohl'
final = pd.concat([final,final2],axis=0)


listtt  =['2014-15 | Regular Season',
       '2015-16 | Regular Season',
       '2016-17 | Regular Season',
       '2017-18 | Regular Season',
       '2018-19 | Regular Season',
       '2019-20 | Regular Season', '2020-21 | Regular Season',
       '2015-16 Regular Season',
       '2016-17 Regular Season',
       '2017-18 Regular Season',
       '2018-19 Regular Season',
       '2019-20 Regular Season']

final = final[final.season_name.isin(listtt)]
clean_dict = {
    "2015-16 | Regular Season": "2015-16 Regular Season",
    "2016-17 | Regular Season": "2016-17 Regular Season",
    "2017-18 | Regular Season": "2017-18 Regular Season",
    "2018-19 | Regular Season": "2018-19 Regular Season",
    "2019-20 | Regular Season": "2019-20 Regular Season",
    "2020-21 | Regular Season": "2020-21 Regular Season",
    "2014-15 | Regular Season": "2014-15 Regular Season",
}

for wrong, right in clean_dict.items():
    final.loc[(final.season_name == wrong), 'season_name'] = right

sns.displot(data=final, x="s", hue="league", kind="kde")
plt.show()
#
# sns.displot(data=final[final.league == 'ohl'], x="s", hue="season_name", kind="kde")
# plt.show()
#
# sns.displot(data=final[final.league == 'qmjhl'], x="s", hue="season_name", kind="kde")
# plt.show()

final['key'] = final['season_name'] + final['league']
sns.displot(data=final, x="s", hue="key", kind="kde")
plt.show()


import ipdb
ipdb.set_trace()
