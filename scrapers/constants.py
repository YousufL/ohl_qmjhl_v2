import pandas as pd

COLS = ['home_roster','away_roster','home_goalies',
                             'away_goalies','plus','minus','goal_scorer',
                             'assist1_player','assist2_player','goalie_out_info',
                             'player_penalized_info','player','goalie','player_served']
START_2 = '&game_id='
END = '&lang_code=en&fmt=json&tab=pxpverbose'

SHOTS = ['game_id', 'event', 'goalie_id', 'goalie_team_id', 'player_id', 'player_team_id',
         'quality', 's_', 'shot_quality_description', 'shot_type', 'shot_type_description',
         'time', 'time_formatted', 'x_location', 'y_location', 'game_ID', 'home', 'period', 'goal_type']

NETWORKS = ['team_id', 'goal_scorer_id', 'assist1_player_id', 'assist2_player_id', 'power_play', 'empty_net',
            'penalty_shot', 'short_handed', 'insurance_goal', 'game_winning', 'game_tieing']

URLS_OHL = [
             'https://ontariohockeyleague.com/schedule/68',
            #  'https://ontariohockeyleague.com/schedule/63',
            # 'https://ontariohockeyleague.com/schedule/66',
            # 'https://ontariohockeyleague.com/schedule/60',
            # 'https://ontariohockeyleague.com/schedule/61',
            # 'https://ontariohockeyleague.com/schedule/56',
            # 'https://ontariohockeyleague.com/schedule/58',
            # 'https://ontariohockeyleague.com/schedule/54',
            # 'https://ontariohockeyleague.com/schedule/55'
            ]
URLS_QMJHL = [
'https://theqmjhl.ca/schedule/197',
              'https://theqmjhl.ca/schedule/196',
              # 'https://theqmjhl.ca/schedule/193',
              # 'https://theqmjhl.ca/schedule/190',
              # 'https://theqmjhl.ca/schedule/191',
              # 'https://theqmjhl.ca/schedule/187',
              # 'https://theqmjhl.ca/schedule/188',
              # 'https://theqmjhl.ca/schedule/184',
              # 'https://theqmjhl.ca/schedule/185',
              # 'https://theqmjhl.ca/schedule/181',
              # 'https://theqmjhl.ca/schedule/182',
              # 'https://theqmjhl.ca/schedule/178',
              # 'https://theqmjhl.ca/schedule/179'
              ]

URLS_OHL_NEW = ['https://ontariohockeyleague.com/schedule/68']
URLS_QMJHL_NEW = ['https://theqmjhl.ca/schedule/196']

RAW = ['player', 'goalie', 'goal_scorer', 'assist1_player', 'assist2_player']

APPEND_ID = '_id'
APPEND_FIRST_NAME = '_first_name'
APPEND_LAST_NAME = '_last_name'
APPEND_JERSEY = '_jersey_number'
APPEND_TEAM_ID = '_team_id'
APPEND_TEAM_CODE = "_team_code"

IDS = [sub + APPEND_ID for sub in RAW]
FIRST_NAMES = [sub + APPEND_FIRST_NAME for sub in RAW]
LAST_NAMES = [sub + APPEND_LAST_NAME for sub in RAW]
JERSEY = [sub + APPEND_JERSEY for sub in RAW]
TEAM_ID = [sub + APPEND_TEAM_ID for sub in RAW]
TEAM_CODE = [sub + APPEND_TEAM_CODE for sub in RAW]

PLUS = ['plus_1', 'plus_2', 'plus_3', 'plus_4', 'plus_5', 'plus_6', 'plus_7']
MINUS = ['minus_1', 'minus_2', 'minus_3', 'minus_4', 'minus_5', 'minus_6', 'minus_7']

PLUS_ID = [sub + APPEND_ID for sub in PLUS]
PLUS_FIRST_NAME = [sub + APPEND_FIRST_NAME for sub in PLUS]
PLUS_LAST_NAME = [sub + APPEND_LAST_NAME for sub in PLUS]
PLUS_TEAM_CODE = [sub + APPEND_TEAM_CODE for sub in PLUS]

MINUS_ID = [sub + APPEND_ID for sub in MINUS]
MINUS_FIRST_NAME = [sub + APPEND_FIRST_NAME for sub in MINUS]
MINUS_LAST_NAME = [sub + APPEND_LAST_NAME for sub in MINUS]
MINUS_TEAM_CODE = [sub + APPEND_TEAM_CODE for sub in MINUS]

BINS = pd.IntervalIndex.from_tuples([(0, 16), (16.01, 17),
                                     (17.01, 18), (18.01, 19),
                                     (19.01, 20)])