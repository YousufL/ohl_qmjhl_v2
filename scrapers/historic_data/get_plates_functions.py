import time
from selenium import webdriver
import re
import pandas as pd

options = webdriver.ChromeOptions()
options.add_experimental_option('excludeSwitches', ['enable-logging'])

class PlateScraper:

    @staticmethod
    def get_handedness_qmjhl(nameplate, hand):

        nameplate['name'] = nameplate['first_name'].astype(str) + ' ' +nameplate['last_name'].astype(str)
        d = []


        for i in range(len(nameplate)):
            print(nameplate['name'][i])

            for x in range(0, 100):  # try 4 times
                try:
                    wd = webdriver.Chrome(executable_path=r'C:\Users\Yousuf LaHaye\chromedriver.exe',options=options)
                    wd.get(hand + str(int(nameplate['player_id'][i])))
                    div = wd.find_elements_by_xpath('//*[@class="info-con-table01"]')
                    div2 = wd.find_elements_by_xpath('//*[@class="info-con-table02"]')
                    div3 = wd.find_elements_by_xpath('//*[@class="name03"]')
                    div4 = wd.find_elements_by_xpath('//*[@class="name01"]')
                    # import ipdb
                    # ipdb.set_trace()
                except div:
                    pass
                if not div:
                    time.sleep(20)
                    print('TRY AGAIN')  # wait for 10 seconds before trying to fetch the url  again
                else:
                    break


            if (len(str(div[0].text.split(' ')[0].split(':')[1])) == 11) & (div[0].text.split(' ')[4] == "Height:"):
                try:
                    d.append({
                        "name": nameplate['name'][i],
                        "player_id": nameplate["player_id"][i],
                        "position": div[0].text.split(' ')[2],
                        "shoots": "",
                        "height": div[0].text.split(' ')[5],
                        "weight": div[0].text.split(' ')[7].split("\n")[0],
                        "birthday": div[0].text.split(' ')[8].split("\n")[0],
                        "draft": div2[0].text
                    })

                except IndexError:
                    d.append({
                        "name": nameplate['name'][i],
                        "player_id": nameplate["player_id"][i],
                        "position": "NA",
                        "shoots": "NA",
                        "height": "NA",
                        "weight": "NA",
                        "birthday": "NA",
                        "draft": "NA"
                    })

            if (div3[0].text == "- Ottawa, 67's (OHL) -") & (len(str(div[0].text.split(' ')[0].split(':')[1])) == 8) & (div[0].text.split(' ')[6] != "Height:"):
                try:
                    d.append({
                        "name": nameplate['name'][i],
                        "player_id": nameplate["player_id"][i],
                        "position": div[0].text.split(' ')[4],
                        "shoots": div[0].text.split(' ')[6],
                        "height": div[0].text.split(' ')[8],
                        "weight": div[0].text.split(' ')[10].split("\n")[0],
                        "birthday": div[0].text.split(' ')[11].split("\n")[0],
                        "draft": ""

                    })
                except IndexError:
                    d.append({
                        "name": nameplate['name'][i],
                        "player_id": nameplate["player_id"][i],
                        "position": "NA",
                        "shoots": "NA",
                        "height": "NA",
                        "weight": "NA",
                        "birthday": "NA",
                        "draft": "NA"
                    })

            if (div3[0].text != "- Ottawa, 67's (OHL) -") & (len(str(div[0].text.split(' ')[0].split(':')[1])) == 8) & (div[0].text.split(' ')[6] != "Height:"):
                if div2:
                    try:
                        d.append({
                            "name": nameplate['name'][i],
                            "player_id": nameplate["player_id"][i],
                            "position": div[0].text.split(' ')[4],
                            "shoots": div[0].text.split(' ')[6],
                            "height": div[0].text.split(' ')[8],
                            "weight": div[0].text.split(' ')[10].split("\n")[0],
                            "birthday": div[0].text.split(' ')[11].split("\n")[0],
                            "draft": div2[0].text

                        })
                    except IndexError:
                        d.append({
                            "name": nameplate['name'][i],
                            "player_id": nameplate["player_id"][i],
                            "position": "NA",
                            "shoots": "NA",
                            "height": "NA",
                            "weight": "NA",
                            "birthday": "NA",
                            "draft": "NA"
                        })

            if (div3[0].text != "- Ottawa, 67's (OHL) -") & (len(str(div[0].text.split(' ')[0].split(':')[1])) == 8) & (
                    div[0].text.split(' ')[6] != "Height:"):
                if not div2:
                    try:
                        d.append({
                            "name": nameplate['name'][i],
                            "player_id": nameplate["player_id"][i],
                            "position": div[0].text.split(' ')[4],
                            "shoots": div[0].text.split(' ')[6],
                            "height": div[0].text.split(' ')[8],
                            "weight": div[0].text.split(' ')[10].split("\n")[0],
                            "birthday": div[0].text.split(' ')[11].split("\n")[0],
                            "draft": ""

                        })
                    except IndexError:
                        d.append({
                            "name": nameplate['name'][i],
                            "player_id": nameplate["player_id"][i],
                            "position": "NA",
                            "shoots": "NA",
                            "height": "NA",
                            "weight": "NA",
                            "birthday": "NA",
                            "draft": "NA"
                        })


            if len(str(div[0].text.split(' ')[0].split(':')[1])) == 5 & (div[0].text.split(' ')[5] == "Height:"):
                try:
                    d.append({
                        "name": nameplate['name'][i],
                        "player_id": nameplate["player_id"][i],
                        "position": div[0].text.split(' ')[3],
                        "shoots": "",
                        "height": div[0].text.split(' ')[6],
                        "weight": div[0].text.split(' ')[8].split("\n")[0],
                        "birthday": div[0].text.split(' ')[9].split("\n")[0],
                        "draft": div2[0].text
                    })
                except IndexError:
                    d.append({
                        "name": nameplate['name'][i],
                        "player_id": nameplate["player_id"][i],
                        "position": "NA",
                        "shoots": "NA",
                        "height": "NA",
                        "weight": "NA",
                        "birthday": "NA",
                        "draft": "NA"
                    })

            if (len(str(div[0].text.split(' ')[0].split(':')[1])) == 5) & (div[0].text.split(' ')[5] != "Height:"):
                if div2:
                    try:
                        d.append({
                            "name": nameplate['name'][i],
                            "player_id": nameplate["player_id"][i],
                            "position": div[0].text.split(' ')[3],
                            "shoots": div[0].text.split(' ')[5],
                            "height": div[0].text.split(' ')[7],
                            "weight": div[0].text.split(' ')[9].split("\n")[0],
                            "birthday": div[0].text.split(' ')[10].split("\n")[0],
                            "draft": div2[0].text
                        })
                    except IndexError:
                        d.append({
                            "name": nameplate['name'][i],
                            "player_id": nameplate["player_id"][i],
                            "position": "NA",
                            "shoots": "NA",
                            "height": "NA",
                            "weight": "NA",
                            "birthday": "NA",
                            "draft": "NA"
                        })

            if (len(str(div[0].text.split(' ')[0].split(':')[1])) == 5) & (div[0].text.split(' ')[5] != "Height:") & (not div2):
                try:
                    d.append({
                        "name": nameplate['name'][i],
                        "player_id": nameplate["player_id"][i],
                        "position": div[0].text.split(' ')[3],
                        "shoots": div[0].text.split(' ')[5],
                        "height": div[0].text.split(' ')[7],
                        "weight": div[0].text.split(' ')[9].split("\n")[0],
                        "birthday": div[0].text.split(' ')[10].split("\n")[0],
                        "draft": ""
                    })
                except IndexError:
                    d.append({
                        "name": nameplate['name'][i],
                        "player_id": nameplate["player_id"][i],
                        "position": "NA",
                        "shoots": "NA",
                        "height": "NA",
                        "weight": "NA",
                        "birthday": "NA",
                        "draft": "NA"
                    })

            if (len(str(div[0].text.split(' ')[0].split(':')[1])) == 11) & (div[0].text.split(' ')[4] != "Height:"):
                try:
                    d.append({
                        "name": nameplate['name'][i],
                        "player_id": nameplate["player_id"][i],
                        "position": div[0].text.split(' ')[2],
                        "shoots": div[0].text.split(' ')[4],
                        "height": div[0].text.split(' ')[6],
                        "weight": div[0].text.split(' ')[8].split("\n")[0],
                        "birthday": div[0].text.split(' ')[9].split("\n")[0],
                        "draft": div2[0].text
                    })

                except IndexError:
                    d.append({
                        "name": nameplate['name'][i],
                        "player_id": nameplate["player_id"][i],
                        "position": "NA",
                        "shoots": "NA",
                        "height": "NA",
                        "weight": "NA",
                        "birthday": "NA",
                        "draft": "NA"
                    })

            if (len(str(div[0].text.split(' ')[0].split(':')[1])) == 8) & (div[0].text.split(' ')[6] == "Height:"):
                if div2:
                    try:
                        d.append({
                            "name": nameplate['name'][i],
                            "player_id": nameplate["player_id"][i],
                            "position": div[0].text.split(' ')[4],
                            "shoots": "",
                            "height": div[0].text.split(' ')[7],
                            "weight": div[0].text.split(' ')[9].split("\n")[0],
                            "birthday": div[0].text.split(' ')[10].split("\n")[0],
                            "draft": div2[0].text
                        })
                    except IndexError:
                        d.append({
                            "name": nameplate['name'][i],
                            "player_id": nameplate["player_id"][i],
                            "position": "NA",
                            "shoots": "NA",
                            "height": "NA",
                            "weight": "NA",
                            "birthday": "NA",
                            "draft": "NA"
                        })


            if (len(str(div[0].text.split(' ')[0].split(':')[1])) == 8) & (div[0].text.split(' ')[6] == "Height:"):
                if not div2:
                    try:
                        d.append({
                            "name": nameplate['name'][i],
                            "player_id": nameplate["player_id"][i],
                            "position": div[0].text.split(' ')[4],
                            "shoots": "",
                            "height": div[0].text.split(' ')[7],
                            "weight": div[0].text.split(' ')[9].split("\n")[0],
                            "birthday": div[0].text.split(' ')[10].split("\n")[0],
                            "draft": ""
                        })
                    except IndexError:
                        d.append({
                            "name": nameplate['name'][i],
                            "player_id": nameplate["player_id"][i],
                            "position": "NA",
                            "shoots": "NA",
                            "height": "NA",
                            "weight": "NA",
                            "birthday": "NA",
                            "draft": "NA"
                        })

            if (len(str(div[0].text.split(' ')[0].split(':')[1])) == 5) & (div[0].text.split(' ')[4] == "Catches:"):
                if div2:
                    if (div[0].text.split(' ')[7] != "Weight:"):
                        try:
                            d.append({
                                "name": nameplate['name'][i],
                                "player_id": nameplate["player_id"][i],
                                "position": div[0].text.split(' ')[4],
                                "shoots": "",
                                "height": div[0].text.split(' ')[7],
                                "weight": div[0].text.split(' ')[9].split("\n")[0],
                                "birthday": div[0].text.split(' ')[10].split("\n")[0],
                                "draft": div2[0].text
                            })
                        except IndexError:
                            d.append({
                                "name": nameplate['name'][i],
                                "player_id": nameplate["player_id"][i],
                                "position": "NA",
                                "shoots": "NA",
                                "height": "NA",
                                "weight": "NA",
                                "birthday": "NA",
                                "draft": "NA"
                            })


            if (len(str(div[0].text.split(' ')[0].split(':')[1])) == 5) & (div[0].text.split(' ')[4] == "Catches:"):
                if div2:
                    if (div[0].text.split(' ')[7] == "Weight:"):
                        try:
                            d.append({
                                "name": nameplate['name'][i],
                                "player_id": nameplate["player_id"][i],
                                "position": div[0].text.split(' ')[3],
                                "shoots": "",
                                "height": div[0].text.split(' ')[6],
                                "weight": div[0].text.split(' ')[8].split("\n")[0],
                                "birthday": div[0].text.split(' ')[9].split("\n")[0],
                                "draft": div2[0].text
                            })
                        except IndexError:
                            d.append({
                                "name": nameplate['name'][i],
                                "player_id": nameplate["player_id"][i],
                                "position": "NA",
                                "shoots": "NA",
                                "height": "NA",
                                "weight": "NA",
                                "birthday": "NA",
                                "draft": "NA"
                            })


            if (len(str(div[0].text.split(' ')[0].split(':')[1])) == 5) & (div[0].text.split(' ')[4] == "Catches:"):
                if not div2:
                    if (div[0].text.split(' ')[7] == "Weight:"):
                        try:
                            d.append({
                                "name": nameplate['name'][i],
                                "player_id": nameplate["player_id"][i],
                                "position": div[0].text.split(' ')[3],
                                "shoots": "",
                                "height": div[0].text.split(' ')[6],
                                "weight": div[0].text.split(' ')[8].split("\n")[0],
                                "birthday": div[0].text.split(' ')[9].split("\n")[0],
                                "draft": ""
                            })
                        except IndexError:
                            d.append({
                                "name": nameplate['name'][i],
                                "player_id": nameplate["player_id"][i],
                                "position": "NA",
                                "shoots": "NA",
                                "height": "NA",
                                "weight": "NA",
                                "birthday": "NA",
                                "draft": "NA"
                            })



            if (len(str(div[0].text.split(' ')[0].split(':')[1])) == 5) & (div[0].text.split(' ')[4] == "Catches:"):
                if not div2:
                    if (div[0].text.split(' ')[7] != "Weight:"):
                        try:
                            d.append({
                                "name": nameplate['name'][i],
                                "player_id": nameplate["player_id"][i],
                                "position": div[0].text.split(' ')[3],
                                "shoots": "",
                                "height": div[0].text.split(' ')[6],
                                "weight": div[0].text.split(' ')[8].split("\n")[0],
                                "birthday": div[0].text.split(' ')[9].split("\n")[0],
                                "draft": ""
                            })
                        except IndexError:
                            d.append({
                                "name": nameplate['name'][i],
                                "player_id": nameplate["player_id"][i],
                                "position": "NA",
                                "shoots": "NA",
                                "height": "NA",
                                "weight": "NA",
                                "birthday": "NA",
                                "draft": "NA"
                            })

            if (len(str(div[0].text.split(' ')[0].split(':')[1])) == 5) & (div[0].text.split(' ')[4] == "Catches:"):
                if not div2:
                    if (div[0].text.split(' ')[7] != "Weight:"):
                        try:
                            d.append({
                                "name": nameplate['name'][i],
                                "player_id": nameplate["player_id"][i],
                                "position": div[0].text.split(' ')[4],
                                "shoots": "",
                                "height": div[0].text.split(' ')[7],
                                "weight": div[0].text.split(' ')[9].split("\n")[0],
                                "birthday": div[0].text.split(' ')[10].split("\n")[0],
                                "draft": ""
                            })
                        except IndexError:
                            d.append({
                                "name": nameplate['name'][i],
                                "player_id": nameplate["player_id"][i],
                                "position": "NA",
                                "shoots": "NA",
                                "height": "NA",
                                "weight": "NA",
                                "birthday": "NA",
                                "draft": "NA"
                            })

            if (len(str(div[0].text.split(' ')[0].split(':')[1])) == 5) & (div[0].text.split(' ')[5] == "Height:"):
                try:
                    d.append({
                        "name": nameplate['name'][i],
                        "player_id": nameplate["player_id"][i],
                        "position": div[0].text.split(' ')[3],
                        "shoots": "",
                        "height": div[0].text.split(' ')[6],
                        "weight": div[0].text.split(' ')[8].split("\n")[0],
                        "birthday": div[0].text.split(' ')[9].split("\n")[0],
                        "draft": div2[0].text
                    })
                except IndexError:
                    d.append({
                        "name": nameplate['name'][i],
                        "player_id": nameplate["player_id"][i],
                        "position": "NA",
                        "shoots": "NA",
                        "height": "NA",
                        "weight": "NA",
                        "birthday": "NA",
                        "draft": "NA"
                    })

        d = pd.DataFrame(d)

        return d

    @staticmethod
    def get_handedness_ohl(nameplate, hand):
        nameplate['name'] = nameplate['first_name'].astype(str) +' ' + nameplate['last_name'].astype(str)
        nameplate['name'] = nameplate['name'].apply(lambda x: (' ' * 1).join(x.split()))
        nameplate.loc[nameplate.name == 'Vladislav Dvurchenskii', 'player_id'] = 8298
        nameplate.loc[nameplate.name == 'Luca Del Bel Belluz', 'name'] = 'Luca DelBelBelluz'

        d = []
        for i in range(len(nameplate)):
            print(hand + str(int(nameplate['player_id'][i])))
            print(nameplate['name'][i])
            for x in range(0, 100):  # try 4 times
                try:
                    wd = webdriver.Chrome(executable_path=r'C:\Users\Yousuf LaHaye\chromedriver.exe',options=options)
                    wd.get(hand + str(int(nameplate['player_id'][i])))

                    div = wd.find_elements_by_xpath('//*[@class="player-profile-secondary"]')
                    div2 = wd.find_elements_by_xpath('//*[@class="player-profile-primary"]')
                    # import ipdb
                    # ipdb.set_trace()

                except div:
                    pass
                if not div:
                    time.sleep(20)
                    print('TRY AGAIN')  # wait for 10 seconds before trying to fetch the url  again
                else:
                    break

            try:

                d.append({
                    "name": nameplate['name'][i],
                    "player_id": nameplate['player_id'][i],
                    "position": re.sub('[^a-zA-Z]+', '',
                                       div2[0].text.split(' ')[(len(nameplate['name'][i].split(' ')) - 1)].split(' ')[
                                           0].split('\n')[1]),
                    "shoots": div[0].text.split(' ')[1].split("\n")[0],
                    "height": div[0].text.split(' ')[2].split("\n")[0],
                    "weight": div[0].text.split(' ')[3].split("\n")[0],
                    "birthday": div[0].text.split(' ')[4].split("\n")[0],
                    "draft": div[0].text.split('Draft:')[1]
                })
            except IndexError:
                d.append({
                    "name": nameplate['name'][i],
                    "player_id": nameplate['player_id'][i],
                    "position": "NA",
                    "shoots": "NA",
                    "height": "NA",
                    "weight": "NA",
                    "birthday": "NA",
                    "draft": "NA"
                })

        d = pd.DataFrame(d)

        return d

    @staticmethod
    def get_draft_qmjhl(nameplate: pd.DataFrame) -> pd.DataFrame:

        nameplate['draft'] = nameplate['draft'].fillna(0)
        nameplate['s'] = nameplate['draft'].apply(lambda x: "QMJHL - Drafted:" in str(x))
        nameplate['q'] = nameplate['draft'].apply(lambda x: "IMPORT" in str(x))

        nameplate['Draft'] = 0
        nameplate['year'] = 0
        nameplate['round'] = 0
        nameplate['pick'] = 0
        nameplate['team'] = 0

        nameplate.loc[(nameplate['q'] == True), "Draft"] = "Import"
        nameplate.loc[(nameplate['s'] == True), "Draft"] = "QMJHL"

        nameplate.loc[(nameplate['shoots'] == ''), "shoots"] = "L"

        nameplate.loc[(nameplate.q == True), 'year'] = nameplate[nameplate.q == True]['draft'].apply(
            lambda x: x.split("IMPORT - Drafted:")[1].strip()[0:4])

        nameplate.loc[(nameplate.q == True), 'round'] = nameplate[nameplate.q == True]['draft'].apply(
            lambda x: x.split("IMPORT - Drafted:")[1].split("Round: ")[1].strip()[0:2])

        nameplate.loc[(nameplate.q == True), 'pick'] = nameplate[nameplate.q == True]['draft'].apply(
            lambda x: x.split("IMPORT - Drafted:")[1].split("(#")[1].split(')')[0].strip())

        nameplate.loc[(nameplate.q == True), 'team'] = nameplate[nameplate.q == True]['draft'].apply(
            lambda x: x.split("IMPORT - Drafted:")[1].split("(#")[1].split(")")[1].strip().split(',')[0:3])

        nameplate.loc[(nameplate.s == True), 'year'] = nameplate[nameplate.s == True]['draft'].apply(
            lambda x: x.split("QMJHL - Drafted:")[1].strip()[0:4])

        nameplate.loc[(nameplate.s == True), 'round'] = nameplate[nameplate.s == True]['draft'].apply(
            lambda x: x.split("QMJHL - Drafted:")[1].split("Round: ")[1].strip()[0:2])

        nameplate.loc[(nameplate.s == True), 'pick'] = nameplate[nameplate.s == True]['draft'].apply(
            lambda x: x.split("QMJHL - Drafted:")[1].split("(#")[1].split(')')[0].strip())

        nameplate.loc[(nameplate.s == True), 'team'] = nameplate[nameplate.s == True]['draft'].apply(
            lambda x: x.split("QMJHL - Drafted:")[1].split("(#")[1].split(")")[1].strip())

        nameplate = nameplate.drop(columns=['s', 'q', 'draft']).reset_index(drop=True)
        nameplate = nameplate.fillna(0)
        nameplate['team'] = nameplate['team'].apply(lambda x: str(x).replace("[","").strip())
        nameplate['team'] = nameplate['team'].apply(lambda x: str(x).replace("]", "").strip())

        return nameplate

    @staticmethod
    def get_draft_ohl(nameplate: pd.DataFrame) -> pd.DataFrame:
        nameplate['draft'] = nameplate['draft'].fillna(0)
        nameplate['s'] = nameplate['draft'].apply(
            lambda x: ("Round:" in str(x)) & ("OHL" in str(x)) & ("FA" not in str(x)) & ("AP" not in str(x)))
        nameplate['q'] = nameplate['draft'].apply(lambda x: "Import" in str(x))

        nameplate['Draft'] = 0
        nameplate['year'] = 0
        nameplate['round'] = 0
        nameplate['pick'] = 0
        nameplate['team'] = 0

        nameplate.loc[(nameplate['q'] == True), "Draft"] = "Import"
        nameplate.loc[(nameplate['s'] == True), "Draft"] = "OHL"

        nameplate.loc[(nameplate.q == True), 'year'] = nameplate[nameplate.q == True]['draft'].apply(
            lambda x: x.split("Import - ")[1].split("(")[1].split(")")[0].strip())
        nameplate.loc[(nameplate.q == True), 'round'] = nameplate[nameplate.q == True]['draft'].apply(
            lambda x: x.split("Import - ")[1].split("Round:")[1].split("(")[0].strip())
        nameplate.loc[(nameplate.q == True), 'pick'] = nameplate[nameplate.q == True]['draft'].apply(
            lambda x: x.split("Import - ")[1].split("Round:")[1].split("(#")[1].split(")")[0].strip())
        nameplate.loc[(nameplate.q == True), 'team'] = nameplate[nameplate.q == True]['draft'].apply(
            lambda x: x.split("Import - ")[1].split("(")[0].strip())

        nameplate.loc[(nameplate.s == True), 'year'] = nameplate[nameplate.s == True]['draft'].apply(
            lambda x: x.split("OHL - ")[1].split("(")[1].split(")")[0].strip())
        nameplate.loc[(nameplate.s == True), 'round'] = nameplate[nameplate.s == True]['draft'].apply(
            lambda x: x.split("OHL - ")[1].split("Round:")[1].split("(")[0].strip())
        nameplate.loc[(nameplate.s == True), 'pick'] = nameplate[nameplate.s == True]['draft'].apply(
            lambda x: x.split("OHL - ")[1].split("Round:")[1].split("(#")[1].split(")")[0].strip())
        nameplate.loc[(nameplate.s == True), 'team'] = nameplate[nameplate.s == True]['draft'].apply(
            lambda x: x.split("OHL - ")[1].split("(")[0].strip())

        nameplate = nameplate.drop(columns=['s', 'q', 'draft']).reset_index(drop=True)
        nameplate = nameplate.fillna(0)
        nameplate.loc[(nameplate['year'] == "WHL"), 'year'] = 2013

        return nameplate