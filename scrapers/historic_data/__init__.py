URLS_OHL = ['https://ontariohockeyleague.com/schedule/68',
            'https://ontariohockeyleague.com/schedule/63',
            'https://ontariohockeyleague.com/schedule/66',
            'https://ontariohockeyleague.com/schedule/60',
            'https://ontariohockeyleague.com/schedule/61',
            'https://ontariohockeyleague.com/schedule/56',
            'https://ontariohockeyleague.com/schedule/58',
            'https://ontariohockeyleague.com/schedule/54',
            'https://ontariohockeyleague.com/schedule/55'
            ]
URLS_QMJHL = ['https://theqmjhl.ca/schedule/196',
              'https://theqmjhl.ca/schedule/193',
              'https://theqmjhl.ca/schedule/190',
              'https://theqmjhl.ca/schedule/191',
              'https://theqmjhl.ca/schedule/187',
              'https://theqmjhl.ca/schedule/188',
              'https://theqmjhl.ca/schedule/184',
              'https://theqmjhl.ca/schedule/185',
              'https://theqmjhl.ca/schedule/181',
              'https://theqmjhl.ca/schedule/182',
              'https://theqmjhl.ca/schedule/178',
              'https://theqmjhl.ca/schedule/179'
              ]
