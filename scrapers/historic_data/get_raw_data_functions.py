import ast
import json
import random
import time
import pandas as pd
import requests
from selenium import webdriver
import numpy as np
import sys
from sqlalchemy import create_engine
from get_plates_functions import PlateScraper

sys.path.extend(['scrapers'])
from scrapers.constants import RAW, FIRST_NAMES, IDS, LAST_NAMES, JERSEY, TEAM_ID, TEAM_CODE, URLS_OHL, URLS_QMJHL, COLS

options = webdriver.ChromeOptions()
options.add_experimental_option('excludeSwitches', ['enable-logging'])

class Scraper(PlateScraper):
    def __init__(self, league):
        self.league = league

    def get_constants(self):

        START_2 = '&game_id='
        END = '&lang_code=en&fmt=json&tab=pxpverbose'
        END_PREVIEW = '&lang_code=en&fmt=json&tab=preview'
        END_GAMESUMMARY = '&lang_code=en&fmt=json&tab=gamesummary'

        if self.league == 'ohl':
            START_1 = 'https://cluster.leaguestat.com/feed/index.php?feed=gc&key=2976319eb44abe94&client_code='
            CLIENT_CODE = 'ohl'
            URLS = URLS_OHL
            HAND_URL = 'https://ontariohockeyleague.com/players/'


        if self.league == 'qmjhl':
            START_1 = 'https://cluster.leaguestat.com/feed/index.php?feed=gc&key=f322673b6bcae299&client_code='
            CLIENT_CODE = 'lhjmq'
            URLS = URLS_QMJHL
            HAND_URL = 'https://theqmjhl.ca/players/'

        return START_2,END, END_PREVIEW, END_GAMESUMMARY, START_1, CLIENT_CODE, URLS, HAND_URL

    @staticmethod
    def get_teamplate(df):

        j = []
        h = []
        for i in range(len(IDS)):
            j.append(df[TEAM_ID[i]])
            h.append(df[TEAM_CODE[i]])
        j = pd.concat(j)
        h = pd.concat(h)

        teamplate = pd.concat([j, h], axis=1).rename(columns={0: "id", 1: "team_name"}).dropna()
        teamplate = teamplate.drop_duplicates(subset=['id', 'team_name'], keep='first').reset_index().drop(
            columns=['index'])

        return teamplate

    @staticmethod
    def get_gamecenter_links(urls):
        year_game_id = []
        links = []
        for i in range(0, len(urls)):
            print(urls[i])

            k = 1
            for x in range(0, 100):  # try 4 times
                try:
                    wd = webdriver.Chrome(executable_path=r'C:\Users\Yousuf LaHaye\chromedriver.exe',options=options)
                    wd.get(urls[i])
                    div = wd.find_elements_by_xpath('//*[@title="Game Centre"]')
                    div2 = wd.find_elements_by_xpath('//*[@class="table__td table__td--schedule-away text-col"]')
                    div3 = wd.find_elements_by_xpath('//*[@class="table__td table__td--schedule-home text-col"]')
                    div4 = wd.find_elements_by_xpath('//*[@class="table__td table__td--schedule-date"]')

                except div:
                    pass
                if not div:
                    time.sleep(5)
                    print('TRY AGAIN')  # wait for 10 seconds before trying to fetch the url  again
                else:
                    break

            for j in range(0, len(div)):
                year_game_id.append({
                    "game_id": div[j].get_attribute('href').split('/')[4],
                    'game_centre_link': div[j].get_attribute('href'),
                    'date': div4[j].text,
                    'home_team': div3[j].text,
                    'away_team': div2[j].text
                })
                links.append(div[j].get_attribute('href'))

        year_game_id = pd.DataFrame(year_game_id)

        return links, year_game_id

    def get_list_of_new_urls_to_scrape(self, links, year_game_id):

        engine = create_engine('postgresql://super:super123@localhost:9999/postgres')

        import ipdb
        ipdb.set_trace()

        if self.league == 'ohl':
            query = "SELECT game_centre_link FROM ohl.raw_ohl"

        if self.league == 'qmjhl':
            query = "SELECT game_centre_link FROM qmjhl.raw_qmjhl"

        existing_links = pd.read_sql(query, engine)

        links = list(set(links).difference(existing_links['game_centre_link'].unique()))
        year_game_id = year_game_id[year_game_id.game_centre_link.isin(links)]



        return links, year_game_id

    def get_list_of_new_names_to_scrape(self,nameplate):

        engine = create_engine('postgresql://super:super123@localhost:9999/postgres')
        if self.league == 'ohl':
            query = "SELECT player_id FROM ohl.nameplate_ohl"

        if self.league == 'qmjhl':
            query = "SELECT player_id FROM qmjhl.nameplate_qmjhl"

        existing_player_ids = pd.read_sql(query, engine)
        nameplate['player_id'] = nameplate['player_id'].astype(int)
        new_ids = list(set(nameplate['player_id'].unique()).difference(existing_player_ids['player_id'].unique()))
        nameplate = nameplate[nameplate.player_id.isin(new_ids)]

        return nameplate

    @staticmethod
    def construct_df(links, year_game_id, start_1, start_2, client_code, end):
        JSON_links = []
        for i in range(len(links)):
            JSON_links.append(start_1 + client_code + start_2 + links[i].split('/')[4] + end)

        d = []
        for i in range(len(JSON_links)):
            try:
                hold_json = requests.get(JSON_links[i]).json()
                hold_df = pd.read_json(json.dumps(hold_json["GC"]['Pxpverbose']))
                d.append({"df": hold_df,
                          "game_id": links[i].split('/')[4],
                          "url_link": links[i]})
            except ValueError:
                pass

        df = pd.DataFrame(d)

        x = []
        for i in range(len(df)):
            df['df'][i]['game_id'] = df['game_id'][i]
            x.append(df['df'][i])

        df = pd.concat(x)

        df['game_id'] = df['game_id'].astype(int)
        year_game_id['game_id'] = year_game_id['game_id'].astype(int)

        df = df.merge(year_game_id, on='game_id')
        return df


    @staticmethod
    def construct_season_attr(links, start_1, start_2, client_code,end_preview, end_gamesummary):

        JSON_links_preview = []
        JSON_links_gamesummary = []
        for i in range(len(links)):
            JSON_links_preview.append(start_1 + client_code + start_2 + links[i].split('/')[4] + end_preview)
            JSON_links_gamesummary.append(start_1 + client_code + start_2 + links[i].split('/')[4] + end_gamesummary)

        master_player_ids = []
        master_first_name = []
        master_last_name = []
        master_position = []

        stats = []
        season_attrs = []
        for j in range(len(JSON_links_preview)):
            try:
                hold_json = requests.get(JSON_links_gamesummary[j]).json()
                hold_json2 = requests.get(JSON_links_preview[j]).json()
            except ValueError:
                pass

            home_roster = []
            away_roster = []
            home_goalies = []
            away_goalies = []

            try:
                y = len(hold_json["GC"]['Gamesummary']['visitor_team_lineup']['players'])
                for i in range(y):
                    try:
                        home_roster.append(hold_json["GC"]['Gamesummary']['home_team_lineup']['players'][i]['player_id'])
                        away_roster.append(hold_json["GC"]['Gamesummary']['visitor_team_lineup']['players'][i]['player_id'])

                        master_player_ids.append(hold_json["GC"]['Gamesummary']['home_team_lineup']['players'][i]['player_id'])
                        master_player_ids.append(hold_json["GC"]['Gamesummary']['visitor_team_lineup']['players'][i]['player_id'])

                        master_first_name.append(hold_json["GC"]['Gamesummary']['home_team_lineup']['players'][i]['first_name'])
                        master_first_name.append(hold_json["GC"]['Gamesummary']['visitor_team_lineup']['players'][i]['first_name'])

                        master_last_name.append(hold_json["GC"]['Gamesummary']['home_team_lineup']['players'][i]['last_name'])
                        master_last_name.append(hold_json["GC"]['Gamesummary']['visitor_team_lineup']['players'][i]['last_name'])

                        master_position.append(hold_json["GC"]['Gamesummary']['home_team_lineup']['players'][i]['position_str'])
                        master_position.append(hold_json["GC"]['Gamesummary']['visitor_team_lineup']['players'][i]['position_str'])
                    except IndexError:
                        pass

                for i in range(len(hold_json["GC"]['Gamesummary']['visitor_team_lineup']['goalies'])):
                    try:
                        home_goalies.append(hold_json["GC"]['Gamesummary']['home_team_lineup']['goalies'][i]['player_id'])
                        away_goalies.append(hold_json["GC"]['Gamesummary']['visitor_team_lineup']['goalies'][i]['player_id'])

                        master_player_ids.append(hold_json["GC"]['Gamesummary']['home_team_lineup']['goalies'][i]['player_id'])
                        master_player_ids.append(hold_json["GC"]['Gamesummary']['visitor_team_lineup']['goalies'][i]['player_id'])

                        master_first_name.append(hold_json["GC"]['Gamesummary']['home_team_lineup']['goalies'][i]['first_name'])
                        master_first_name.append(hold_json["GC"]['Gamesummary']['visitor_team_lineup']['goalies'][i]['first_name'])

                        master_last_name.append(hold_json["GC"]['Gamesummary']['home_team_lineup']['goalies'][i]['last_name'])
                        master_last_name.append(hold_json["GC"]['Gamesummary']['visitor_team_lineup']['goalies'][i]['last_name'])

                        master_position.append(hold_json["GC"]['Gamesummary']['home_team_lineup']['goalies'][i]['position_str'])
                        master_position.append(hold_json["GC"]['Gamesummary']['visitor_team_lineup']['goalies'][i]['position_str'])
                    except IndexError:
                        pass
            except TypeError:
                pass

            try:
                x = len(hold_json["GC"]['Gamesummary']['visitor_team_lineup']['players'])
                for i in range(x):

                        try:

                            stats.append({"game_id": hold_json2["GC"]['Parameters']['game_id'],
                                          "season_id": hold_json2["GC"]['Preview']['current_season']['id'],
                                          "player_id": hold_json["GC"]['Gamesummary']['visitor_team_lineup']['players'][i]['player_id'],
                                          "team_id": hold_json2["GC"]['Preview']['visitor_team']['team_id'],
                                          "plusminus": hold_json["GC"]['Gamesummary']['visitor_team_lineup']['players'][i]['plusminus'],
                                          "pim": hold_json["GC"]['Gamesummary']['visitor_team_lineup']['players'][i]['pim'],
                                          "faceoff_wins": hold_json["GC"]['Gamesummary']['visitor_team_lineup']['players'][i][
                                              'faceoff_wins'],
                                          "faceoff_attempts": hold_json["GC"]['Gamesummary']['visitor_team_lineup']['players'][i][
                                              'faceoff_attempts'],
                                          "hits": hold_json["GC"]['Gamesummary']['visitor_team_lineup']['players'][i]['hits'],
                                          "shots": hold_json["GC"]['Gamesummary']['visitor_team_lineup']['players'][i]['shots'],
                                          "shots_on": hold_json["GC"]['Gamesummary']['visitor_team_lineup']['players'][i]['shots_on'],
                                          })
                        except IndexError:
                            print('the')

                        try:

                            stats.append({"game_id": hold_json2["GC"]['Parameters']['game_id'],
                                          "season_id": hold_json2["GC"]['Preview']['current_season']['id'],
                                          "player_id": hold_json["GC"]['Gamesummary']['home_team_lineup']['players'][i]['player_id'],
                                          "team_id": hold_json2["GC"]['Preview']['home_team']['team_id'],
                                          "plusminus": hold_json["GC"]['Gamesummary']['home_team_lineup']['players'][i]['plusminus'],
                                          "pim": hold_json["GC"]['Gamesummary']['home_team_lineup']['players'][i]['pim'],
                                          "faceoff_wins": hold_json["GC"]['Gamesummary']['home_team_lineup']['players'][i][
                                              'faceoff_wins'],
                                          "faceoff_attempts": hold_json["GC"]['Gamesummary']['home_team_lineup']['players'][i][
                                              'faceoff_attempts'],
                                          "hits": hold_json["GC"]['Gamesummary']['home_team_lineup']['players'][i]['hits'],
                                          "shots": hold_json["GC"]['Gamesummary']['home_team_lineup']['players'][i]['shots'],
                                          "shots_on": hold_json["GC"]['Gamesummary']['home_team_lineup']['players'][i]['shots_on'],
                                          })
                        except IndexError:
                            pass
            except TypeError:
                pass

            season_attrs.append({"game_id": hold_json2["GC"]['Parameters']['game_id'],
                                 "season_id": hold_json2["GC"]['Preview']['current_season']['id'],
                                 "season_start_date": hold_json2["GC"]['Preview']['current_season']['start_date'],
                                 "season_end_date": hold_json2["GC"]['Preview']['current_season']['end_date'],
                                 "season_name": hold_json2["GC"]['Preview']['current_season']['season_name'],
                                 "home_roster": home_roster,
                                 "away_roster": away_roster,
                                 "home_goalies": home_goalies,
                                 "away_goalies": away_goalies,
                                 })

        master_player_ids = pd.DataFrame(master_player_ids)
        master_first_name = pd.DataFrame(master_first_name)
        master_last_name = pd.DataFrame(master_last_name)
        master_position = pd.DataFrame(master_position)

        nameplate = pd.concat([master_player_ids, master_first_name, master_last_name, master_position], axis=1)
        nameplate.columns = ['player_id', 'first_name', 'last_name', 'position']

        nameplate = nameplate.drop_duplicates(subset=['player_id'],keep='first').reset_index(drop=True)

        season_attrs = pd.DataFrame(season_attrs)
        stats = pd.DataFrame(stats)

        return nameplate, season_attrs, stats



    @staticmethod
    def merge_attrs_and_df(df, season_attrs):

        season_attrs['game_id'] = season_attrs['game_id'].astype(int)
        df = df.merge(season_attrs, how = 'left', on='game_id')

        print(df['game_id'].isna().sum())

        df['season_len'] = df['season_name'].astype(str).apply(lambda x: x.split(" ")[0].strip()).str.len()
        df['hold_year'] = df['season_name'].astype(str).apply(lambda x: x.split(" ")[0].strip().split('-')[0].strip())
        df['hold_date'] = df['hold_year'].apply(lambda x: x[2:4])

        df['year'] = np.where(df['season_len'] == 7,
                 df['hold_year'] + "-20" + (df['hold_date'].astype(int)+1).astype(str),
                 '20' + (df['hold_date'].astype(int) -1).astype(str) + "-" +df['hold_year'].astype(str))

        df['unique_game_key'] = df['game_id'].astype(str) + df['year'].astype(str) + df['date'].astype(str)

        return df

    @staticmethod
    def parse_columns(df):
        for i, j in zip(range(len(RAW)), range(len(IDS))):
            df[IDS[j]] = df[RAW[i]].apply(lambda x: ast.literal_eval(str(x))['player_id'] if x == x else x)
            df[FIRST_NAMES[j]] = df[RAW[i]].apply(lambda x: ast.literal_eval(str(x))['first_name'] if x == x else x)
            df[LAST_NAMES[j]] = df[RAW[i]].apply(lambda x: ast.literal_eval(str(x))['last_name'] if x == x else x)
            df[JERSEY[j]] = df[RAW[i]].apply(lambda x: ast.literal_eval(str(x))['jersey_number'] if x == x else x)
            df[TEAM_ID[j]] = df[RAW[i]].apply(lambda x: ast.literal_eval(str(x))['team_id'] if x == x else x)
            df[TEAM_CODE[j]] = df[RAW[i]].apply(lambda x: ast.literal_eval(str(x))['team_code'] if x == x else x)

        return df

    @staticmethod
    def get_game_state(df):

        df['game_state'] = 0

        # mask_EV = ((df['state'] == '5v5') | (df['state'] == '4v4') | (df['state'] == '3v3'))
        # mask_PP = ((df['state'] == '4v3') | (df['state'] == '5v3') | (df['state'] == '5v4') | (df['state'] == '6v4') | (
        #         df['state'] == '6v5'))
        # mask_SH = ((df['state'] == '3v4') | (df['state'] == '3v5') | (df['state'] == '4v5'))
        #
        # df['game_state'].mask(mask_EV, "EV")
        # df['game_state'].mask(mask_PP, "PP")
        # df['game_state'].mask(mask_SH, "SH")

        df['game_state'] = df['game_state'].apply(lambda x: random.choice(["EV","SH","PP"]))

        return df

    def __call__(self):

        START_2,END, END_PREVIEW, END_GAMESUMMARY, START_1, CLIENT_CODE, URLS, HAND_URL= self.get_constants()
        links, year_game_id = self.get_gamecenter_links(urls=URLS)
        #links, year_game_id = self.get_list_of_new_urls_to_scrape(links, year_game_id)

        if links is None:
            exit()
            raise ValueError('No New Games to Scrape!')

        df = self.construct_df(links,
                          year_game_id,
                          start_1=START_1,
                          start_2=START_2,
                          client_code=CLIENT_CODE,
                          end=END)

        nameplate, season_attrs, stats = self.construct_season_attr(links,
                                                               start_1=START_1,
                                                               start_2=START_2,
                                                               client_code=CLIENT_CODE,
                                                               end_preview=END_PREVIEW,
                                                               end_gamesummary=END_GAMESUMMARY)
        df = self.merge_attrs_and_df(df, season_attrs)
        df = self.parse_columns(df)
        df = self.get_game_state(df)
        teamplate = self.get_teamplate(df)

        nameplate = self.get_list_of_new_names_to_scrape(nameplate)
        nameplate.reset_index(drop=True,inplace=True)

        if self.league == 'qmjhl':
            HAND_URL = 'https://theqmjhl.ca/players/'
            if len(nameplate) > 0:
                nameplate = self.get_handedness_qmjhl(nameplate, hand=HAND_URL)
                nameplate = nameplate.drop_duplicates(subset=['player_id'], keep='first')
                nameplate = self.get_draft_qmjhl(nameplate)

        if self.league == 'ohl':
            HAND_URL = 'https://ontariohockeyleague.com/players/'
            if len(nameplate) > 0:
                nameplate = self.get_handedness_ohl(nameplate, hand=HAND_URL)
                nameplate = nameplate.drop_duplicates(subset=['player_id'], keep='first')
                nameplate = self.get_draft_ohl(nameplate)

        return df, teamplate, nameplate, stats