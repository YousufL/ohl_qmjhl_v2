import dash
import dash_table
import dash_core_components as dcc
import dash_html_components as html
#import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output, State
import plotly.graph_objs as go
import pandas as pd
import numpy as np
import base64


#set a local image as a background
image_filename = '../Data/realbackground3_clear.png'
rink = base64.b64encode(open(image_filename, 'rb').read())

### DATASET
dataset = pd.read_csv('C:/Users/Jered/Desktop/python/Data/2019-2020shotswithxg.csv')
app = dash.Dash()
#data=dataset.loc[dataset['Player Name']=="Quinton Byfield"]
data=dataset.loc[dataset['player_team']=="KGN"]

def getshotmap(data,chart_type):
    x_range, y_range = 300,300              #Range of the x and y coordinates in the table
    n,m= 12,12                              #Dimensions (n,m) that we want our contour boxes to be
    rx, ry = int(x_range/n), int(y_range/m) #Number of contour boxes in each dimension
    x_mat = np.linspace(x_range, 0,rx+1)    # Get the X bin locations figured out (reversed to 300,0 so we dont need to do -1 in the next loop)
    y_mat = np.linspace(y_range, 0,ry+1)    # Get the Y bin locations figured out (reversed to 300,0 so we dont need to do -1 in the next loop)
    fmat, gmat = np.zeros([rx, ry]), np.zeros([rx, ry]) #Assign empty zvalues to each table
    smat = {'x':[],'y':[]} #List of goal locations
    #For each bin (x,y dimension groups) get the values for the bin
    for i in range(0, rx):
        for j in range(0, ry):
            subset= data[(data['x_location']>=n*i)&(data['x_location']<(n*i+n))&(data['y_location']>=m*j)&(data['y_location']<m*j+m)]
            if len(subset)>0:
                if chart_type == 'xg':
                    gmat[i][j] =  subset['Preds'].sum()
                elif chart_type == 'shot':
                    fmat[i][j] =  len(subset)
            else:
                if chart_type == 'xg':
                    gmat[i][j] = 0
                elif chart_type == 'shot':
                    fmat[i][j] = 0
    #Record the goal locations for the scatter plot
    goals = data[data.event == 1]
    if len(goals) > 0:
        for idx in goals.index:
            smat['x'].append(x_range-data.loc[idx,'x_location'].item())
            smat['y'].append(y_range-data.loc[idx,'y_location'].item())
    return x_mat, y_mat, fmat, gmat, smat

#Pre set list options
leagues = [{'label': 'OHL','value': 'OHL'},{'label': 'QMJHL','value': 'QMJHL'}]

#Interface
app.layout = html.Div([
                    html.Div([
                    html.Div([
                    html.Div([html.H3('Select a League')],
                    style={'display':'inline-block', 'width':'30%', 'textAlign': 'left'}),
                    html.Div([html.H3(""),dcc.Dropdown(id='league',options=leagues,value='OHL')],
                    style={'display':'inline-block', 'width':'33%'})],
                    style={'display': 'flex'}),

                    html.Div([
                    html.Div([html.H3('Select a Team')],
                    style={'display':'inline-block', 'width':'30%', 'textAlign': 'left'}),
                    html.Div([html.H3(""),dcc.Dropdown(id='team_name')],
                    style={'display':'inline-block', 'width':'33%'})],
                    style={'display': 'flex'}),
                    
                    html.Div([
                    html.Div([html.H3('Select a Player')],
                    style={'display':'inline-block', 'width':'30%', 'textAlign': 'left'}),
                    html.Div([html.H3(""),dcc.Dropdown(id='player_name')],
                    style={'display':'inline-block', 'width':'33%'})],
                    style={'display': 'flex'}),
                    
                    html.Div([
                    html.Div([html.H3('Choose a Heatmap Type')],
                    style={'display':'inline-block', 'width':'30%', 'textAlign': 'left'}),
                    html.Div([html.H3(""),dcc.RadioItems(id='chart_type',
                            options=[
                                {'label': 'xG Map', 'value': 'xg'},
                                {'label': 'Shot Map', 'value': 'shot'}
                            ],
                            value='xg')],
                    style={'display':'inline-block', 'width':'33%'})],
                    style={'display': 'flex'}),
                    
                    html.Div([
                    html.Div([html.H3('Choose Goal Inclusion')],
                    style={'display':'inline-block', 'width':'30%', 'textAlign': 'left'}),
                    html.Div([html.H3(""),dcc.RadioItems(id='show_goal',
                            options=[
                                {'label': 'Show Goals', 'value': 'goal'},
                                {'label': 'Hide Goals', 'value': 'no goal'}
                            ],
                            value='no goal')],
                    style={'display':'inline-block', 'width':'33%'})],
                    style={'display': 'flex'})],
                    style={'display':'inline-block', 'width':'100%', 'border-bottom' : '2px black dashed'}),

                    html.Div([
                    html.Div([html.Div([html.H3("")],style={'display':'inline-block','width':'20%'}),
                    dcc.Graph(id='heatmap',
                        config={'displayModeBar': False},
                        style={
                            'height':800,
                            'width':800 #CHANGE THE POSITION
                                    })],
                        style={'display':'flex'})],
                    style={'display':'inline-block', 'width':'100%'})
                ], style={
                        'display':'block',
                        'padding':'8px'
                    }
                )


@app.callback([Output('team_name','options'),Output('team_name','value')],
              [Input('league','value')])
def getteams(league):
    #Get the right team list based on the league
    if league == 'OHL':
        teams = [{'label':str(year),'value':str(year)} for year in sorted(dataset['player_team'].unique())]
        value = 'LDN'
    elif league == 'QMJHL':
        teams = [{'label':str(year),'value':str(year)} for year in sorted(dataset['player_team'].unique())]
        value = 'RIM'

    return teams, value

@app.callback([Output('player_name','options'),Output('player_name','value')],
              [Input('team_name','value')])
def getplayers(team):
    #Get the list of players we need and create the options list, take first player as default
    playerset = sorted(dataset[dataset['player_team']==team]['Player Name'].unique())
    players = [{'label':str(player),'value':str(player)} for player in playerset]
    if len(playerset) > 0:
        value = playerset[0]
    else:
        value = None
    return players, value


@app.callback([Output('heatmap', 'figure')],
              [Input('team_name','value'),Input('player_name','value'),Input('chart_type','value'),Input('show_goal','value')])
def getcontours(team, player,chart_type,show_goal):
    playerdata = dataset[(dataset['player_team'] == team) & (dataset['Player Name'] == player)] #Get only player shots on team
    x_mat, y_mat, fmat, gmat, smat = getshotmap(playerdata,chart_type) #Run the function to get the data
    choosedic = {
        'xg':{
            'data':gmat,
            'title': ' All Situations xG Map (',
            'zmax':1,
            'color':'Purples'
        },
        'shot':{
            'data':fmat,
            'title': ' All Situations Shot Map (',
            'zmax':5,
            'color':'Blues'
        }} #Dictionary for values depending on what was chosen
    title = '<b>' + player + choosedic[chart_type]['title'] + team + ')<b>' #Get the title
    #Determine the data based on whether the user selects to include goals
    if show_goal == 'goal':
        gdata = [go.Contour(
                                z=choosedic[chart_type]['data'],
                                x=y_mat,
                                y=x_mat,
                                colorscale=choosedic[chart_type]['color'],
                                zmax = choosedic[chart_type]['zmax'],
                                zmin = 0,
                                opacity = 1
                                ),go.Scatter(x = smat['y'],
                                y = smat['x'],
                                mode='markers',
                                marker=dict(size=10,color='#f39c02',
                                line=dict(width=1,
                                            color='#000000')))]
    else:
        gdata = [go.Contour(
                                z=choosedic[chart_type]['data'],
                                x=y_mat,
                                y=x_mat,
                                colorscale=choosedic[chart_type]['color'],
                                zmax = choosedic[chart_type]['zmax'],
                                zmin = 0,
                                opacity = 1)]
    return [{'data':gdata,
            'layout':
                go.Layout(title=title, #CHART 1
                        titlefont=dict(size=20),
                        xaxis={'showticklabels':False, 'showgrid':False, 'ticks':"", 'fixedrange':True},
                        yaxis={'showticklabels':False, 'showgrid':False, 'ticks':"", 'fixedrange':True},
                        images = [dict(
                            source='data:image/png;base64,{}'.format(rink.decode()),
                            #source = 'Data/background.png',
                            xref= "paper",
                            yref= "paper",
                            x= 0,
                            y= 1,
                            sizex= 1,
                            sizey= 1,
                            sizing= "stretch",
                            opacity= 1,
                            visible = True,
                            layer= "above")]),
            'staticPlot': True}]

# dcc.Graph(id='shotContour',
#                         config={'displayModeBar': False},
#                         style={
#                             'height':700,
#                             'width':700
#                         },
#                         figure={'data':[
#                                     go.Contour(
#                                     z=fmat,
#                                     x=y_mat,
#                                     y=x_mat,
#                                     colorscale="Blues",
#                                     zmax = 7,
#                                     zmin = 0,
#                                     opacity = 1
#                                     )],
#                                 'layout':
#                                     go.Layout(title='<b>Kingston Frontenacs All Situations Shot Map</b>',
#                                             titlefont=dict(size=20),
#                                             xaxis={'showticklabels':False, 'showgrid':False, 'ticks':"", 'fixedrange':True},
#                                             yaxis={'showticklabels':False, 'showgrid':False, 'ticks':"", 'fixedrange':True},
#                                             images = [dict(
#                                                source='data:image/png;base64,{}'.format(rink.decode()),
#                                                #source = 'Data/background.png',
#                                                xref= "paper",
#                                                yref= "paper",
#                                                x= 0,
#                                                y= 1,
#                                                sizex= 1,
#                                                sizey= 1,
#                                                #sizing= "stretch",
#                                                opacity= 1,
#                                                visible = True,
#                                                layer= "above")]),
#                                'staticPlot': True
#                         }
#                 ),
#                 dcc.Graph(id="xgContour",
#                                 style={
#                                     'height':700,
#                                     'width':700
#                                 },
#                                 figure={'data':[
#                                             go.Contour(
#                                             z=gmat,
#                                             x=y_mat,
#                                             y=x_mat,
#                                             colorscale="purples",
#                                             zmax = 2,
#                                             zmin = 0,
#                                             opacity = 1
#                                             )],
#                                         'layout':
#                                             go.Layout(title='<b>Ottawa 67s Even Strength xG Map</b>',
#                                                     titlefont=dict(size=20),
#                                                     xaxis={'showticklabels':False, 'showgrid':False, 'ticks':"", 'fixedrange':True},
#                                                     yaxis={'showticklabels':False, 'showgrid':False, 'ticks':"", 'fixedrange':True},
#                                                     images = [dict(
#                                                        source='data:image/png;base64,{}'.format(rink.decode()),
#                                                        #source = 'Data/background.png',
#                                                        xref= "paper",
#                                                        yref= "paper",
#                                                        x= 0,
#                                                        y= 1,
#                                                        sizex= 1,
#                                                        sizey= 1,
#                                                        #sizing= "stretch",
#                                                        opacity= 1,
#                                                        visible = True,
#                                                        layer= "above")])
#                                 }
#                 ),
#                 dcc.Graph(id="scoringContour",
#                                 style={
#                                     'height':700,
#                                     'width':700
#                                 },
#                                 figure={'data':[
#                                             go.Contour(
#                                             z=smat,
#                                             x=y_mat,
#                                             y=x_mat,
#                                             colorscale="RdBu"
#                                             )],
#                                         'layout':
#                                             go.Layout(title='<b>OHL Goals, PP - Nick Robertson (2019-2020)</b>',
#                                                     titlefont=dict(size=20),
#                                                     xaxis={'showticklabels':False},
#                                                     yaxis={'showticklabels':False})
#                                 }
#                         )



if __name__ == "__main__":
    app.run_server()
