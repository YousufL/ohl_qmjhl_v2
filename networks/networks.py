#Take care of imports
import dash
import dash_table
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.graph_objs as go
import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt
import colorlover as cl
import math

import time



#Get the file for assists and team colours
#assist = pd.read_csv('Data/nhl_assists-1.csv', index_col=0)
teamcs = pd.read_csv('teamcolours.csv', index_col=0)
assist = pd.read_csv('nhl_data.csv', index_col=0)
qassist = pd.read_csv('qmjhlgoals.csv',index_col=0)
oassist = pd.read_csv('ohlgoals.csv', index_col=0)
assist = assist[~assist['Team_Name'].isin(['Canadian All-Stars','American All-Stars','Team Metropolitan','Team Atlantic','Team Central','Team Pacific'])]

#Upload the csv file with the information for true points
# tp = pd.read_csv('../../Data/tp_all.csv', encoding='utf-8', index_col = 0)
# pps = pd.read_csv('../../Data/pps.csv', index_col=0)
# tps = pd.read_csv('../../Data/tps.csv', index_col=0)
tp = pd.read_csv('tp_All_mar15.csv', encoding='utf-8', index_col = 0)
pps = pd.read_csv('pps_mar15.csv', index_col=0)
tps = pd.read_csv('tps_mar15.csv', index_col=0)

#Reorganize the tp and pp
myindex = [str(number) for number in range(1,24)]
tps = tps[myindex]
pps = pps[myindex]

#Get the list of possible team names by taking the sorted unique team names and filtering out the one off teams
#Labal and Value are necessary as a dictionary to be passed into the dropdown
teamnames = [{'label':str(team),'value':team} for team in sorted(assist.Team_Name.unique()) if team not in ['Canadian All-Stars','American All-Stars','Team Metropolitan','Team Atlantic','Team Central','Team Pacific']]
leagues = [{'label': 'NHL','value': 'NHL'},{'label': 'OHL','value': 'OHL'},{'label': 'QMJHL','value': 'QMJHL'}]
scenarios = [{'label': 'All Situations','value': 'all'},{'label': 'Even Strength','value': 'EVEN'},{'label': 'PowerPlay','value': 'PPG'}]

#Get the hard coded conditions for the Assistmen and Targets Tables
myconds = [{'if': {'column_id': 'Occurences','filter_query': '{Occurences} < 2'},
                'backgroundColor': '#f5f7f5','color': 'black'},
            {'if': {'column_id': 'Occurences','filter_query': '{Occurences} > 1 && {Occurences} < 4'},
                'backgroundColor': '#e4f5e5','color': 'black'},
            {'if': {'column_id': 'Occurences','filter_query': '{Occurences} > 3 && {Occurences} < 6'},
                'backgroundColor': '#c8efcb','color': 'black'},
            {'if': {'column_id': 'Occurences','filter_query': '{Occurences} > 5 && {Occurences} < 8'},
                'backgroundColor': '#b4e9b8','color': 'black'},
            {'if': {'column_id': 'Occurences','filter_query': '{Occurences} > 7 && {Occurences} < 10'},
                'backgroundColor': '#99e09e','color': 'black'},
            {'if': {'column_id': 'Occurences','filter_query': '{Occurences} > 9 && {Occurences} < 12'},
                'backgroundColor': '#7ed884','color': 'black'},
            {'if': {'column_id': 'Occurences','filter_query': '{Occurences} > 11'},
                'backgroundColor': '#52cb5b','color': 'black'}]

#Get the dash app and layout set
app = dash.Dash()
app.layout = html.Div([

            #Div to contain the top 3 things
            html.Div([
            html.H1('Team Network Diagram & Combo Tables'),
            html.P(html.A('For more information, please see our blog',href='https://gtanalytics.pythonanywhere.com/firstapp/BlogMain/')),
            html.P(),
            #Div for the top left selection
            html.Div([
            html.Div([
            html.Div([html.H3('Select a League')],
            style={'display':'inline-block', 'width':'30%', 'textAlign': 'left'}),
            html.Div([html.H3(""),dcc.Dropdown(id='league',options=leagues,value='NHL')],
            style={'display':'inline-block', 'width':'68%'})],
            style={'display': 'flex'}),

            html.Div([
            html.Div([html.H3('Select a Season')],
            style={'display':'inline-block', 'width':'30%', 'textAlign': 'left'}),
            html.Div([html.H3(""),dcc.Dropdown(id='league_years')],
            style={'display':'inline-block', 'width':'68%'})],
            style={'display': 'flex'})],
            style={'display':'inline-block', 'width':'49%', 'border-right' : '2px black dashed '}),

            #Div for the top right  selection
            html.Div([
            html.Div([
            html.Div([html.H3('Select a Team')],
            style={'display':'inline-block', 'width':'30%', 'textAlign': 'left','padding-left':'2%'}),
            html.Div([html.H3(""),dcc.Dropdown(id='team_name')],
            style={'display':'inline-block', 'width':'68%'})],
            style={'display': 'flex'}),

            html.Div([
            html.Div([html.H3('Select a Situation')],
            style={'display':'inline-block', 'width':'30%', 'textAlign': 'left','padding-left':'2%'}),
            html.Div([html.H3(""),dcc.Dropdown(id='scenario',options=scenarios,value='all')],
            style={'display':'inline-block', 'width':'68%'})],
            style={'display': 'flex'})],
            style={'display':'inline-block', 'width':'49%', 'border-left' : '2px black dashed '})],
            style={'border' : '8px black solid', 'backgroundColor': '#e8e8e8','text-align': 'center', 'border-bottom-width': '4px'}),

            #Div for the graph/table container
            html.Div([
            dcc.Graph(id='graph'),

            #Div for the top left under the graph (just the table title)
            html.Div(id='goalscorers',children=[html.H3(id='combotitle')],
            style={'width': '50%', 'display': 'inline-block', 'text-align': 'center'}),

            #Div for the top righ under the graph, dropdown list of players
            html.Div([html.H3(id='playtitle'), dcc.Dropdown(id='team-players')],
            style={'width': '50%', 'display': 'inline-block','text-align': 'center'}),

            #Div for the combos table
            html.Div([dash_table.DataTable(id='combos',
            style_table={'position': 'relative',
            'margin-left': '15%',
            'margin-right': '15%',
            'margin-top': '0%',
            'width': '70%'},
            style_header={
            'backgroundColor': '#000000',
            'color': 'white',
            'fontWeight': 'bold'},
            style_cell={'textAlign': 'left','border': '1px solid black'})],
            style={'width': '50%', 'display': 'inline-block', 'vertical-align':'top'}),

            #Div for the 2 player tables for assistmen & targets
            html.Div([html.H4("Player's Top Assistmen"), dash_table.DataTable(id='gtargets',
            columns = [{"name": i, "id": i} for i in ['Goal_Scorer','Primary_Assister','Occurences']],
            style_table={'position': 'relative',
            'margin-left': '15%',
            'margin-right': '15%',
            'width': '70%'},
            style_header={
            'backgroundColor': '#000000',
            'color': 'white',
            'fontWeight': 'bold'},
            style_cell={'textAlign': 'left','border': '1px solid black'},
            style_data_conditional = myconds), html.H4("Player's Top Targets"), dash_table.DataTable(id='atargets',
            columns = [{"name": i, "id": i} for i in ['Goal_Scorer','Primary_Assister','Occurences']],
            style_table={'position': 'relative',
            'margin-left': '15%',
            'margin-right': '15%',
            'width': '70%'},
            style_header={
            'backgroundColor': '#000000',
            'color': 'white',
            'fontWeight': 'bold'},
            style_cell={'textAlign': 'left','border': '1px solid black'},
            style_data_conditional = myconds)],style={'width': '48%', 'display': 'inline-block', 'text-align': 'center'}),
            html.P(),

            #Div for the true and primary points diagram
            html.Div([dcc.Graph(id='graph2')], style={'display':'inline-block','width':'75%', 'margin-bottom': '0px'}),
            html.Div([dcc.Graph(id='boxplot')], style={'display':'inline-block','width':'25%', 'margin-bottom': '0px'}),
            #Split the bottom into 3 sections
            html.Div([html.Div([html.H3('Select Depth Chart Point Type:')], style = {'width':'30%','display':'inline-block', 'text-align': 'center'}),
            html.Div([dcc.RadioItems(id='statdec',options=[
                {'label': 'Primary Points', 'value': 'pp'},
                {'label': 'True Points', 'value': 'tp'}], value='pp')],style={'width':'20%','display':'inline-block','font-weight':'bold'}),
            html.Div([html.A('To see how True Points are calculated, please see our blog',href='https://gtanalytics.pythonanywhere.com/firstapp/BlogMain/')],
            style={'width':'50%','display':'inline-block','text-align': 'center'})])],
            #Border setting for the large border
            style={"border":"8px black solid", 'background-color':'#e8e8e8','border-top-width': '4px', 'padding-bottom':'0px'}),
            ])

#####
#Get the inner fuctions
def getpoints(df):
  #Get the number of points for each player
  points = {}

  #Check to see if the players are in dic, then add points
  for index, year, team, scorer, a1, a2, stren in df.itertuples():
    if scorer in points:
      points[scorer] += 1
    else:
      points[scorer] = 1

    if a1 != 'None':
      if a1 in points:
        points[a1] += 1
      else:
        points[a1] = 1

    if a2 != 'None':
      if a2 in points:
        points[a2] += 1
      else:
        points[a2] = 1

  #Return the dictionary
  return points
#####

#####
def goalGraph(df):
    G = nx.Graph()

    #Go through every link
    for (index, year, team, scorer, a1, a2, stren) in df.itertuples():
      #Check to see if a1 and a2 are already linked to the goal scorer, and if scorer is in the graph already
      for a4 in [a1,a2]:
        #If there was no assister, continue
        if a4.lower() == "none":
          continue
        if scorer in G and a4 in G[scorer]:
            G[scorer][a4]["comgoals"] += 1
        else:
            G.add_edge(scorer, a4, comgoals=1)

    return G
#####

#####
def layitout(g, layout):
    positions = layout(g)
    nx.set_node_attributes(g, name="pos", values=positions)
#####

#####
def friendfollow(g, points):
  #Initiate the dictionaries
  bfs = {}
  followers = {}

  #Go through every node to get bf information
  for node in g.nodes():
    #Initiate the best friend and top score
    bestfriend = ""
    topscore = 0
    #Get the players best friend
    for key in g[node].keys():
      if g[node][key]['comgoals'] > topscore:
        topscore = g[node][key]['comgoals']
        bestfriend = key

    #Set the best friend
    bfs[node] = bestfriend

    #If the node isnt in followers, add them
    if node not in followers.keys():
      followers[node] = {'count': 0, 'points': 0, 'weights': 0}
    #Add a follower to best friend depending on if they are in the follower dict
    if bestfriend not in followers.keys():
      followers[bestfriend] = {'count': 1, 'points': points[node], 'weights': topscore}
    else:
      #Get the numerator (points) back, then add the weighted new points, then divide again in the end
      followers[bestfriend]['count'] += 1
      followers[bestfriend]['points'] *= followers[bestfriend]['weights']
      followers[bestfriend]['points'] += points[node]*topscore
      followers[bestfriend]['weights'] += topscore
      followers[bestfriend]['points'] /= followers[bestfriend]['weights']
  return bfs, followers
#####

#####
def graphit(g, points, colorscheme, bfs, followers):
  #Get the color scheme
  colorscheme = cl.interp(colorscheme, 250)

  #Get the max and min points and max and min
  degreecent = nx.degree_centrality(g)
  maxdeg = max(degreecent.values())
  mindeg = min(degreecent.values())

  #Get a list of all the data to be graphed
  scatters=[]

  for (node1, node2) in g.edges():
      x0, y0 = g.nodes[node1]['pos']
      x1, y1 = g.nodes[node2]['pos']
      edgewidth = g[node1][node2]['comgoals']/8
      #Get the trace
      s = go.Scatter(
              x=[x0, x1],
              y=[y0, y1],
              hoverinfo='text',
              mode='lines',
              line=dict(width=edgewidth,color='#111'))
      scatters.append(s)


  for node in g.nodes():
      #nodesize = 25*((1 + points[node] - minp)/(maxp - minp))
      nodesize = points[node]/3
      if maxdeg == mindeg:
          nodecolor = 249
      else:
          nodeColor = int(249*(degreecent[node]-mindeg)/(maxdeg-mindeg))
      xPos, yPos = g.nodes[node]['pos']

      #Add the trace
      s = go.Scatter(
              x=[xPos],
              y=[yPos],
              text="<b>Player:</b> {} <br><b>Points:</b> {} <br><b>Degree:</b> {}% <br><b>Followers:</b> {}<br><b>Avg Follower:</b> {} points<br><b>Best Friend:</b> {}".format(node, points[node],round(degreecent[node]*100,1),followers[node]['count'],round(followers[node]['points'],1),bfs[node]),
              hoverinfo='text',
              mode='markers',
              marker=dict(
                  color=colorscheme[nodeColor],
                  size=nodesize,
                  line=dict(width=1)))
      scatters.append(s)
  return scatters
#####

############################################### MAIN function #################
#Create a function to change the team options when the league value is selected
@app.callback([Output('league_years','options'),Output('league_years','value'),
               Output('team_name','options'),Output('team_name','value')],
              [Input('league','value')])
def getteams(league):
    #Get the right team list based on the league
    if league == 'OHL':
        lego = [{'label':str(year),'value':str(year)} for year in sorted(oassist.Year.unique())]
        levo = '2019-20'
        options = [{'label':str(team),'value':str(team)} for team in sorted(oassist.Team_Name.unique()) if team not in ['GAT']]
        value = 'London Knights'
    elif league == 'QMJHL':
        lego = [{'label':str(year),'value':str(year)} for year in sorted(qassist.Year.unique())]
        levo = '2019-20'
        options = [{'label':str(team),'value':str(team)} for team in sorted(qassist.Team_Name.unique()) if team not in ['Ott', 0, '0']]
        value = 'Rimouski Oceanic'
    else:
        lego = [{'label':str(year),'value':str(year)} for year in sorted(assist.Year.unique())]
        levo = '2019-20'
        options = [{'label':str(team),'value':str(team)} for team in sorted(assist.Team_Name.unique()) if team not in ['Canadian All-Stars','American All-Stars','Team Metropolitan','Team Atlantic','Team Central','Team Pacific']]
        value = 'Toronto Maple Leafs'
    return lego, levo, options, value

#Create the function to create the graph and all info once something is selected
#Leverage the decorator capabilities
@app.callback([Output('graph', 'figure'), Output('team-players', 'options'),
               Output('playtitle', 'children'), Output('combotitle', 'children')],
              [Input('league','value'),Input('team_name', 'value'), Input('scenario', 'value'), Input('league_years','value')])
def runthegraph(chosenleague, chosenteam, situation, year):
  #Get the proper data table based on league
  if chosenleague == 'OHL':
      fassist = oassist
  elif chosenleague == 'QMJHL':
      fassist = qassist
  else:
      fassist = assist

  #Get the team table based on situation
  if situation == 'all':
    #print(type(chosenteam))
    teamshots = fassist[(fassist['Team_Name'] == chosenteam) & (fassist['Year'] == year)]
    title = '<b>' + year + " " + chosenteam + ' - All situations</b>'
    ptitle = 'Player Insights - All Situations'
    ctitle = 'Top Goal Scorer/Primary Assist Combos - All Situations'
  else:
    #print(type(chosenteam))
    teamshots = fassist[(fassist['Team_Name'] == chosenteam) & (fassist['Strength'] == situation) & (fassist['Year'] == year)]
    title = '<b>' + year + " " + chosenteam + ' - ' + situation + '</b>'
    ptitle = 'Player Insights - ' + situation
    ctitle = 'Top Goal Scorer/Primary Assist Combos - ' + situation

  #Get the points, graph, layout, and file
  #Get the Points for each player
  point = getpoints(teamshots)
  #Get the actual networkx set up
  letsgo = goalGraph(teamshots)
  #Get player best friends and followers
  bf, follow = friendfollow(letsgo, point)
  #Get the layout (spring layout to favour important players)
  layitout(letsgo, nx.spring_layout)
  #return the scatters for the diagram
  scatters = graphit(letsgo, point, colorscheme = cl.scales['9']['seq']['PuBu'], bfs = bf, followers = follow)

  #Get the list of names for the player list
  #print(teamshots.Goal_Scorer_fullname.unique())
  playerlist = teamshots.Goal_Scorer_fullname.unique().tolist()
  playerlist.extend(teamshots.Assist1_fullname.unique().tolist())

  #KEEP THIS ITS FOR PAPER bgcolor teamcs.loc[chosenteam][0]
  #Return the graph and the player list
  return {
      'data': scatters,
      'layout': {'hovermode':'closest',
                'title' : title,
                'titlefont':{'color':'white', 'size': 25},
                'showlegend': False,
                'height': 750,
                'plot_bgcolor': '#f1f7f9',
                'paper_bgcolor': teamcs.loc[chosenteam][0],
                'xaxis' :{'showticklabels': False,'showgrid': True,'zeroline': True, 'mirror':True, 'showline':True},
                'yaxis' :{'showticklabels': False,'showgrid': True,'zeroline': True, 'mirror':True, 'showline':True},
                'margin':{'l':25, 'r': 25,'t': 90,'b': 25}}},[{'label':str(player),'value':player} for player in sorted(list(set(playerlist))) if player not in ['None']],ptitle, ctitle

############################################### MAIN function #################
#Function to create the combo table with player names and combo frequency and the player stuff
@app.callback([Output('combos','columns'), Output('combos','data'),
               Output('combos','style_data_conditional'), Output('gtargets','data'), Output('atargets','data')],
              [Input('league','value'), Input('team_name','value'), Input('scenario','value'), Input('team-players','value'),Input('league_years','value')])
def combostable(chosenleague, team, situation, theplayer, year):
    #Get the proper data table based on league
    if chosenleague == 'OHL':
        fassist = oassist
    elif chosenleague == 'QMJHL':
        fassist = qassist
    else:
        fassist = assist

    #Get the grouped and sorted table using the groupby and sort function by scenario
    if situation == 'all':
        evens = fassist[(fassist['Team_Name'] == team) & (fassist['Year'] == year)].groupby(['Goal_Scorer_fullname','Assist1_fullname']).count().sort_values('Assist2_fullname', ascending = False)[['Assist2_fullname']].copy()
        #evens = fassist.groupby(['Goal_Scorer_fullname','Assist1_fullname']).count().sort_values('Assist2_fullname', ascending = False)[['Assist2_fullname']].copy()
    else:
        evens = fassist[(fassist['Team_Name'] == team)&(fassist['Strength'] == situation)& (fassist['Year'] == year)].groupby(['Goal_Scorer_fullname','Assist1_fullname']).count().sort_values('Assist2_fullname', ascending = False)[['Assist2_fullname']].copy()
        #evens = fassist[(fassist['Strength'] == situation)].groupby(['Goal_Scorer_fullname','Assist1_fullname']).count().sort_values('Assist2_fullname', ascending = False)[['Assist2_fullname']].copy()
    #Get a placeholder table for all results and for the player
    finaltable = pd.DataFrame(index = range(1,len(evens.index) + 1), columns = ['Goal_Scorer', 'Primary_Assister', 'Occurences'])

    #Fill out using the groupby table to include goal scorer name and primary assister for each row
    for i in range(len(evens.index)):
      finaltable.at[i+1,'Goal_Scorer'] = evens.index[i][0]
      finaltable.at[i+1,'Primary_Assister'] = evens.index[i][1]
      finaltable.at[i+1,'Occurences'] = evens.loc[evens.index[i],'Assist2_fullname']

    #Get the 3 tables to be shown on the bottom half of the webpage
    goaltable = finaltable[finaltable['Goal_Scorer'] == theplayer].head(4)
    assisttable = finaltable[finaltable['Primary_Assister'] == theplayer].head(4)
    finaltable = finaltable.head(15)

    #KEEP THIS: teamcs.loc[team][0] FIRST 2 CONDITIONS NEESD THIS
    #Get the stuff for the conditional formatting of the table
    condition = [{'if': {'column_id': 'Goal_Scorer','filter_query': '{{Goal_Scorer}} = "{}"'.format(theplayer)},
                'backgroundColor': teamcs.loc[team][0],'color': 'white'},
            {'if': {'column_id': 'Primary_Assister','filter_query': '{{Primary_Assister}} = "{}"'.format(theplayer)},
                'backgroundColor': teamcs.loc[team][0],'color': 'white'},
            {'if': {'column_id': 'Occurences','filter_query': '{Occurences} < 2'},
                'backgroundColor': '#f5f7f5','color': 'black'},
            {'if': {'column_id': 'Occurences','filter_query': '{Occurences} > 1 && {Occurences} < 4'},
                'backgroundColor': '#e4f5e5','color': 'black'},
            {'if': {'column_id': 'Occurences','filter_query': '{Occurences} > 3 && {Occurences} < 6'},
                'backgroundColor': '#c8efcb','color': 'black'},
            {'if': {'column_id': 'Occurences','filter_query': '{Occurences} > 5 && {Occurences} < 8'},
                'backgroundColor': '#b4e9b8','color': 'black'},
            {'if': {'column_id': 'Occurences','filter_query': '{Occurences} > 7 && {Occurences} < 10'},
                'backgroundColor': '#99e09e','color': 'black'},
            {'if': {'column_id': 'Occurences','filter_query': '{Occurences} > 9 && {Occurences} < 12'},
                'backgroundColor': '#7ed884','color': 'black'},
            {'if': {'column_id': 'Occurences','filter_query': '{Occurences} > 11'},
                'backgroundColor': '#52cb5b','color': 'black'}]
    #Get the return types (Columns, Data, Conditions, Data, Data)
    return [{"name": i, "id": i} for i in finaltable.columns], finaltable.to_dict('records'), condition, goaltable.to_dict('records'), assisttable.to_dict('records')

############################################### MAIN function #################
#Function to create the True/Primary Points table at the bottom of the big screen
@app.callback([Output('graph2', 'figure'),Output('boxplot', 'figure')],
              [Input('league','value'),Input('team_name', 'value'), Input('league_years','value'), Input('statdec','value')])
def update_figure(selected_league, selected_team, selected_season, stat):
    #Depending on what was picked, get the variable names to be used
    if (stat == 'tp'):
        g,gname,a,aname,p = 'True_Goals','True Goals','True_Assists','True Assists','True'
        mytable = tps
    else:
        g,gname,a,aname,p = 'Goals','Goals','Primary_Assists','Pri. Assists','Primary'
        mytable = pps

    #Adjust the selected season to fit the data format
    selected_season = selected_season[:5] + "20" + selected_season[-2:]

    #Get the league average adjusted year (if its O or Q, adjust for it)
    if selected_league == 'OHL':
        selected_season2 = "O" + selected_season
    elif selected_league == 'QMJHL':
        selected_season2 = "Q" + selected_season
    else:
        selected_season2 = selected_season

    #Get the maximum value for the season for the y axis purposes
    max = math.ceil(tp[tp['Season'] == selected_season][str(p+'_Points')].max()/10)*10 -1

    #STACKED BAR SECTION
    #Get the table for the team and season, organized by the stat of choice
    df = tp[(tp['Team_Name'] == selected_team) & (tp['Season'] == selected_season)].sort_values(str(p+'_Points'), ascending=False).iloc[0:23,:]

    #Get the trace for the goals and assists
    trace0 = go.Scatter(x = df['Player_Name'], y = mytable.loc[selected_season2,:], name='League Avg', mode='lines+markers', marker = dict(size = 7, color = 'white', line = dict(color='black',width=1)), line = dict(color='black', width=1), hoverlabel = dict(bgcolor='black'))
    trace1 = go.Bar(x=df['Player_Name'], y=df[g], name=gname, marker_color = '#44d5d3')
    trace2 = go.Bar(x=df['Player_Name'], y=df[a], name=aname, marker_color = '#ffae80')
    zata = [trace0, trace1, trace2]

    #Get the title
    title = '<b>' + str(selected_season) + ' ' + str(selected_team) + " " + p + " Points Depth Chart (All Sits)" + '</b>'
    title2 = '<b>' + 'Team All Sits ' + p + " Points" + " Boxplot" + '</b>'

    #BOXPLOT SECTION
    #Get the table with the required team and season
    #df1 = tp[(tp['Team_Name'] == selected_team) & (tp['Season'] == selected_season)]

    #Get the boxplot data for the figure
    gata = [go.Box(y=df[str(p+'_Points')], marker_color = teamcs.loc[selected_team][0], name=selected_team)]

    return {'data': zata, 'layout': {'title': title, 'titlefont':{'color':'white', 'size': 20}, 'barmode': 'stack', 'paper_bgcolor': teamcs.loc[selected_team][0],
                                        'yaxis':{'range':[0,max], 'dtick':10, 'gridcolor':'#f0f0f0', 'color':'white','showline':True, 'mirror':True},
                                        'xaxis':{'color':'white','showline':True, 'mirror':True},
                                        'margin':{'l':25, 'r': 25,'t': 60},
                                        'plot_bgcolor': '#696969', 'hoverlabel': dict(bgcolor='white'),
                                        'legend': dict(x=1,y=1,xanchor='right',bgcolor='white')}},{'data': gata,
                                        'layout': {'title': title2,
                                        'titlefont':{'color':'white', 'size': 15},
                                        'paper_bgcolor': teamcs.loc[selected_team][0],
                                        'yaxis':{'range':[0,max], 'dtick':10, 'gridcolor':'lightgrey', 'color':'white','showline':True, 'mirror':True},
                                        'xaxis':{'color':'white','showline':True, 'mirror':True},
                                        'margin':{'l':26, 'r': 26,'t': 60}}}

#Run the server BOI
if __name__ == '__main__':
    app.run_server()

