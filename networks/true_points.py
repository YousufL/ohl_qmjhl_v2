# imports
import pandas as pd
import numpy as np

# Read Entire CSV
master = pd.read_csv("qmjhlgoals.csv", encoding='latin-1')
master = master.rename(columns={'Year':'Season'})

# Strip words
master['Goal_Scorer_fullname'].str.strip()
master['Assist1_fullname'].str.strip()
master['Assist2_fullname'].str.strip()
master['Team_Name'].str.strip()

# Dataframe of all unique players and seasons
seasons = master['Season'].unique()
df = pd.DataFrame()
for season in seasons:
  temp = master[master['Season']==season]
  column_values = temp[["Goal_Scorer_fullname", "Assist1_fullname"]].values.ravel()
  unique_values =  pd.unique(column_values)
  tdf = pd.DataFrame(unique_values)
  tdf['Season'] = season
  df = df.append(tdf)
df.columns = ['Player_Name', 'Season']

# Adding team name to dataframe
df['Team_Name'] = ''
df = df.reset_index(drop=True)
for i, row in df.iterrows():
  name = df['Player_Name'][i]
  year = df['Season'][i]
  temp = master[(master['Goal_Scorer_fullname'] == name) & (master['Season'] == year)]
  if (len(temp) == 0):
    temper = master[(master['Assist1_fullname'] == name) & (master['Season'] == year)]
    df['Team_Name'][i] = temper['Team_Name'].iloc[0]
  else:
    df['Team_Name'][i] = temp['Team_Name'].iloc[0]

# Dataframe of goals scored by player in each season
goals = pd.DataFrame()
for season in seasons:
  tdf = pd.DataFrame()
  temp = master[master['Season']==season]
  scorers = temp['Goal_Scorer_fullname'].unique()
  for scorer in scorers:
    tempo = temp[temp['Goal_Scorer_fullname'] == scorer]
    Goals = len(tempo)
    tdf = tdf.append({'Scorer': scorer, 'Goals': Goals, 'Season_x': season}, ignore_index=True)
  goals = goals.append(tdf)

# Dataframe of primary assists scored by player in each season
assists = pd.DataFrame()
for season in seasons:
  tdf = pd.DataFrame()
  temp = master[master['Season']==season]
  assisters = temp['Assist1_fullname'].unique()
  for assister in assisters:
    tempo = temp[temp['Assist1_fullname'] == assister]
    Assists = len(tempo)
    tdf = tdf.append({'Player': assister, 'Primary_Assists': Assists, 'Season_y': season}, ignore_index=True)
  assists = assists.append(tdf)

# Merging goals and assists into master stats df
stats = pd.merge(goals, assists, how='outer', left_on=['Scorer', 'Season_x'], right_on=['Player', 'Season_y'])
stats['Goals'].fillna(0, inplace=True)
stats['Primary_Assists'].fillna(0, inplace=True)
stats['Primary_Points'] = stats['Goals'] + stats['Primary_Assists']

# Fill missing data cells
stats.Player.fillna(stats.Scorer, inplace=True)
stats.Scorer.fillna(stats.Player, inplace=True)
stats.Season_x.fillna(stats.Season_y, inplace=True)
stats.Season_y.fillna(stats.Season_x, inplace=True)

# Dropping and renaming columns and setting index
stats = stats.drop(['Scorer', 'Season_x'], axis=1)
stats.rename(columns={'Season_y':'Season'}, inplace=True)
stats.set_index('Player', inplace=True)
stats = stats.drop('None')

# Add columns for True Goals and Assists
master['True_Goals'] = 0.0
master['True_Assist'] = 0.0

# Calculate and populate True Goals and True Assists
for i, row in master.iterrows():
  if i%500 == 0:
    print(str(round(float(i)/len(master),3)) + '%')
  scorer = master['Goal_Scorer_fullname'][i]
  assister = master['Assist1_fullname'][i]
  season = master['Season'][i]
  if (assister == 'None'):
    master['True_Goals'][i] = 2.0
    master['True_Assist'][i] = 0.0
  else:
    tempo = stats[stats['Season']==season]
    goals = tempo['Goals'][scorer]
    assists = tempo['Primary_Assists'][assister]
    temp = master[(master['Goal_Scorer_fullname'] == scorer) & (master['Assist1_fullname'] == assister) & (master['Season'] == season)]
    linkups = len(temp)
    g = (goals - linkups)/goals
    a = (assists - linkups)/assists
    if (g == 0):
      master['True_Goals'][i] = 0.0
      master['True_Assist'][i] = 2.0
    else:
      x = 1 + (g-a)/g
      y = 1 + (a-g)/g
      if (x < 0):
        master['True_Goals'][i] = 0.0
        master['True_Assist'][i] = 2.0
      elif (y < 0):
        master['True_Goals'][i] = 2.0
        master['True_Assist'][i] = 0.0
      else:
        master['True_Goals'][i] = x
        master['True_Assist'][i] = y

# True Goals table
ricky = pd.DataFrame()
scorers = master['Goal_Scorer_fullname'].unique()
for season in seasons:
  print(season)
  print(len(scorers))
  for scorer in scorers:
    temp = master[(master['Goal_Scorer_fullname'] == scorer) & (master['Season'] == season)]
    TG = temp['True_Goals'].sum()
    ricky = ricky.append({'Player': scorer, 'True_Goals': TG, 'Season': season}, ignore_index=True)

# True Assists table
jerry = pd.DataFrame()
passers = master['Assist1_fullname'].unique()
for season in seasons:
  print(season)
  print(len(passers))
  for passer in passers:
    temp = master[(master['Assist1_fullname'] == passer) & (master['Season'] == season)]
    TA = temp['True_Assist'].sum()
    jerry = jerry.append({'Player': passer, 'True_Assists': TA, 'Season': season}, ignore_index=True)

# Merge true goals and true assists
stassy = stats.merge(ricky, how='inner', left_on=['Player', 'Season'], right_on=['Player', 'Season'])
tp = stassy.merge(jerry, how='inner', left_on=['Player', 'Season'], right_on=['Player', 'Season'])
tp['True_Points'] = tp['True_Goals'] + tp['True_Assists']
tp = tp.merge(df, how='inner', left_on=['Player', 'Season'], right_on=['Player_Name', 'Season'])
tp = tp.drop(['Player_Name'], axis=1)
tp['Goals_Diff'] = tp['True_Goals'] - tp['Goals']
tp['Assists_Diff'] = tp['True_Assists'] - tp['Primary_Assists']
tp['Points_Diff'] = tp['True_Points'] - tp['Primary_Points']

# Export to CSV
tp.to_csv ('tp_qmjhl.csv', index = True, header=True)