import requests
import json
import pandas

#Create a table and global index
allgoals = pd.DataFrame(columns = ['Year','Team','Scorer','Assist1','Assist2','Strength'])
i = -1

for year in range(2013,2020):
  print(year)
  #Get the game cap
  if year == 2012:
    cap = 721
  elif year < 2017:
    cap = 1231
  elif year < 2019:
    cap = 1272
  else:
    cap = 1083

  #Get all games in a season
  for game in range(1,cap):
    if game % 250 == 0:
      print(game)
    if game < 10:
      filler = "000"
    elif game < 100:
      filler = "00"
    elif game < 1000:
      filler = "0"
    else: 
      filler = ""
    url = "https://statsapi.web.nhl.com/api/v1/game/" + str(year) + "02" + filler + str(game) + "/feed/live"
    #url = "https://statsapi.web.nhl.com/api/v1/game/201202" + filler + str(game) + "/feed/live"
    r = requests.get(url)
    cont = r.json()
    
    #print(game)
    #print(cont['gameData']['datetime']['dateTime'], cont['gameData']['teams']['away']['name'], cont['gameData']['teams']['home']['name']) 


    for play in cont['liveData']['plays']['allPlays']:
      if play['result']['event'] == 'Goal' and play['about']['periodType'] != "SHOOTOUT":
        #Get the metavariables
        assistcount = 0
        i += 1

        #Set the goal descriptors
        allgoals.at[i,'Year'] = year
        allgoals.at[i,'Team'] = play['team']['name']
        allgoals.at[i,'Strength'] = play['result']['strength']['code']

        #Mark all the players
        for player in play['players']:
          if player['playerType'] == 'Scorer':
            allgoals.at[i,'Scorer'] = player['player']['fullName']
            #print('Scorer', player['player']['fullName'])
          elif player['playerType'] == 'Assist':
            if assistcount == 0:
              #print('Assist1', player['player']['fullName'])
              allgoals.at[i,'Assist1'] = player['player']['fullName']
              assistcount += 1
            else: 
              #print('Assist2', player['player']['fullName'])
              allgoals.at[i,'Assist2'] = player['player']['fullName']