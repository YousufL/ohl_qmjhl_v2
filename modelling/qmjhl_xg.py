from constants import FEATURE_COLS
# from data_prep.data_wrangler import WrangleData
from sklearn.linear_model import LogisticRegression
import pandas as pd
import re
import ipdb
import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import log_loss
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_curve
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
# from pycaret.classification import *
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.model_selection import train_test_split, GridSearchCV, RandomizedSearchCV
from sklearn.model_selection import StratifiedKFold
from xgboost import XGBClassifier
import warnings
warnings.filterwarnings('ignore')
# import lightgbm as lgb
# from lightgbm import LGBMClassifier

def get_data(league, combine):
    '''
    Providing the option to train the models as ohl/qmjhl or combining the data into
    one main df
    '''

    if league == 'ohl':
        df = pd.read_csv('prelim_cleaned_data/test_model_ohl.csv')

    if league == 'qmjhl':
        df = pd.read_csv('prelim_cleaned_data/test_model_qmjhl.csv')
        df = df[(pd.to_datetime(df['new_date'].astype(str)) <= '2020-07-01')].reset_index().drop(columns=['index'])

    if combine == True:
        df = pd.read_csv('prelim_cleaned_data/test_model_ohl.csv')
        df = df[FEATURE_COLS + ['event', 'new_date']]
        df['League'] = 'ohl'

        df_qmjhl = pd.read_csv('prelim_cleaned_data/test_model_qmjhl.csv')
        df_qmjhl = df_qmjhl[FEATURE_COLS + ['event', 'new_date']]
        df_qmjhl = df_qmjhl[(pd.to_datetime(df_qmjhl['new_date'].astype(str)) <= '2020-07-01')].reset_index().drop(columns=['index'])
        df_qmjhl['League'] = 'qmjhl'

        df = pd.concat([df, df_qmjhl], axis=0)

    return df


def deal_with_columns(df, drop_first):
    '''
    Makes sure that the same columns exist. In the future, for example,if there is no
    right-shooting goalie in a game the 4 dummified colmns that deal with that case wont show up,
    so we will have a column mismatch. To deal with this we set all columns that arent in the df
    that are in FEATURE_COLS and set all values to 0

    The second part gives the option to drop the first category from the categorical variable bins
    '''
    for item in range(len(list(set(FEATURE_COLS).difference(df.columns)))):
        df[item] = 0

    if drop_first == True:
        df = df.drop(columns=drop_first)

    return df

def league_bins(df):
    df['league'] = np.where((df['League'] == 'ohl'), 1, 0)
    df = df.drop(['League'], axis=1)
    return df

def setup_train_val_test_datasets(df, split_date):
    '''
    Splitting the data into train and test sets. We want to hold out the 2019-2020 season
    as unseen data to be able to compare models more effectively
    '''

    train = df[pd.to_datetime(df['new_date'].astype(str)) <= split_date].reset_index().drop(columns=['index'])
    test = df[pd.to_datetime(df['new_date'].astype(str)) > split_date].reset_index().drop(columns=['index'])

    X_train = train[FEATURE_COLS]
    # X_train['league'] = train['league']
    y_train = train['event']

    X_test = test[FEATURE_COLS]
    # X_test['league'] = test['league']
    y_test = test['event']

    return X_train, y_train, X_test, y_test, train, test

def setup_train_val_test_datasets_walk_forward(df, year):
    '''
    Splitting the data into train and test sets. We want to hold out the 2019-2020 season
    as unseen data to be able to compare models more effectively
    '''

    train = df[df['hold_year'] < year].reset_index().drop(columns=['index'])
    test = df[df['hold_year'] == year].reset_index().drop(columns=['index'])

    X_train = train[FEATURE_COLS]
    X_train.drop(['hold_year'], axis=1)
    # X_train['league'] = train['league']
    y_train = train['event']

    X_test = test[FEATURE_COLS]
    X_test.drop(['hold_year'], axis=1)
    # X_test['league'] = test['league']
    y_test = test['event']

    train.drop(['hold_year'], axis=1)
    test.drop(['hold_year'], axis=1)

    return X_train, y_train, X_test, y_test, train, test


def get_baseline_model_and_test_scores_for_viz(X_train, y_train, X_test, y_test, test):
    '''
    Running baseline logistic regression model. Returning csv with model scores that can be used
    for prelim development of heatmaps, dashboards and shooting/goaltending talent models.
    '''

    logit = LogisticRegression(max_iter=10000)
    logit.fit(X_train, y_train)

    xg = pd.DataFrame(logit.predict_proba(X_test)[:, 1], columns=['xg'])
    test_scores_ohl = pd.concat([test, xg], axis=1)
    train_score = logit.score(X_train, y_train)
    test_score = logit.score(X_test, y_test)
    betas = pd.Series(logit.coef_[0], index=X_train.columns)
    betas = betas.append(pd.Series({"Intercept": logit.intercept_[0]}))

    print(betas)
    print('The train score is {} and the test score is {}'.format(train_score, test_score))
    print('The accuracy is {}'.format(accuracy_score(y_test, logit.predict(X_test))))
    print('The roc auc score is {}'.format(roc_auc_score(y_test, logit.predict_proba(X_test)[:, 1])))
    print('The log loss is {}'.format(log_loss(y_test, logit.predict_proba(X_test)[:, 1])))
    print('The precision is {}'.format(precision_score(y_test, logit.predict(X_test))))
    print('The recall is {}'.format(recall_score(y_test, logit.predict(X_test))))

    # Confusion Matrix
    y_pred = logit.predict(X_test)
    cfm = confusion_matrix(y_true=y_test, y_pred=y_pred)
    ax = sns.heatmap(cfm, annot=True)
    ax.set(xlabel='Predicted', ylabel='Actual');
    plt.show()

    # Goal and No Goal Histograms
    y_proba = logit.predict_proba(X_test)
    y_proba = np.concatenate((y_proba.T, np.array([y_test])))
    df_proba = pd.DataFrame(y_proba.T, columns=['prob0', 'prob1', 'True'])
    sns.distplot(df_proba[df_proba['True'] == 1].prob1, bins=30, kde=False, label='Actual value 1')
    sns.distplot(df_proba[df_proba['True'] == 0].prob1, bins=30, kde=False, label='Actual value 0')
    plt.legend()
    plt.show()

    # test_scores_ohl.to_csv('test_scores_qmjhl.csv')



def monotone_transform(X, columns_to_transform):
    X_new = X.copy()
    for column in columns_to_transform:
        X_new[column] = np.sqrt(X_new[column])
    return X_new


def new_features_and_normalize(X_train, X_val, non_binary_columns):
    # Function that normalizes the non binary data so that across all data
    # points (i.e., rows) the mean and standard deviation of a features is 0
    # and 1, respectively.

    # Define scaling function
    scaler = StandardScaler()

    # Scale the X features
    X_train[non_binary_columns] = scaler.fit_transform(X_train[non_binary_columns])
    X_val[non_binary_columns] = scaler.transform(X_val[non_binary_columns])

    return X_train, X_val, scaler

def k_means_clustering(df, n_clusters):
    # Only clustering x and y coordinates
    df_scaled = df[['x_location', 'y_location']].copy()

    # Define clustering model
    mdk_k_means = KMeans(n_init=50,
                         # number of different centroid seed initializations (number of times algorithm is run)
                         n_clusters=n_clusters,  # number of clusters (k)
                         random_state=1) # random seed for k-means algorithm


    # Fit the model to the training set
    mdk_k_means.fit(df_scaled)  # [non_binary_columns])

    # Get cluster assignments for each datapoint
    clK = mdk_k_means.labels_

    # Get the centroid of each cluster
    Centroids = mdk_k_means.cluster_centers_

    # Get distances for each point that is fit to the model
    distances_from_clusters = mdk_k_means.transform(df_scaled)

    return clK, Centroids, distances_from_clusters, mdk_k_means

def cluster_regression_1_hot_encoding_model(X_data, cluster_model, columns_to_cluster):
    # Function that encodes the cluster label that a data point belongs to
    # as a one-hot-encoded feature/label

    # Assign cluster lables to training and testing
    cluster_labels = cluster_model.predict(X_data[columns_to_cluster])

    # Number of clusters
    clusters = range(cluster_model.n_clusters)

    # Go through each cluster to make a model
    for c in clusters:
        # Get all indices that belong to cluster c
        cluster_col = 'cluster_{}'.format(c)

        # Add a new column (cluster_col) to X_data that contains a
        # 1 if the row is assigned to that cluster and a 0 o/w
        # -------------------

        X_data[cluster_col] = (cluster_labels == c) * 1

        # -------------------

    return X_data

def get_model_and_test_scores(mdl, X_train, y_train, X_test, y_test):
    mdl.fit(X_train, y_train)

    train_score = mdl.score(X_train, y_train)
    test_score = mdl.score(X_test, y_test)
    y_pred = mdl.predict(X_test)

    print('The train score is {} and the test score is {}'.format(train_score, test_score))
    print('The accuracy is {}'.format(accuracy_score(y_test, y_pred)))
    print('The roc auc score is {}'.format(roc_auc_score(y_test, mdl.predict_proba(X_test)[:, 1])))
    print('The log loss is {}'.format(log_loss(y_test, mdl.predict_proba(X_test)[:, 1])))
    print('The precision is {}'.format(precision_score(y_test, y_pred)))
    print('The recall is {}'.format(recall_score(y_test, y_pred)))

    # Confusion Matrix
    cfm = confusion_matrix(y_true=y_test, y_pred=y_pred)
    ax = sns.heatmap(cfm, annot=True)
    ax.set(xlabel='Predicted', ylabel='Actual');
    plt.show()

    # Goal and No Goal Histograms
    y_proba = mdl.predict_proba(X_test)
    y_proba = np.concatenate((y_proba.T, np.array([y_test])))
    df_proba = pd.DataFrame(y_proba.T, columns=['prob0', 'prob1', 'True'])
    sns.distplot(df_proba[df_proba['True'] == 1].prob1, bins=30, kde=False, label='Actual value 1')
    sns.distplot(df_proba[df_proba['True'] == 0].prob1, bins=30, kde=False, label='Actual value 0')
    plt.legend()
    plt.show()

def grid_search_gradient_boost(x_train, y_train):

  param_grid = {
    # "loss":["deviance", "exponential"],
    "loss": ["deviance"],
    # "learning_rate": [0.01, 0.075, 0.2],
    "learning_rate": [0.2],
    "max_depth":[3],
    "subsample":[1.0],
    # "subsample": [0.5, 0.85, 1.0],
    "n_estimators":[25]
    # "n_estimators": [5, 10, 25]
  }

  grid = GridSearchCV(GradientBoostingClassifier(), param_grid, cv=5, scoring = 'neg_log_loss', error_score=0.0)
  grid.fit(x_train, y_train)
  print(grid.best_score_)
  print(grid.best_params_)

  return grid

def grid_search_ada_boost(x_train, y_train):

  param_grid = {
    "learning_rate": [0.1, 0.5, 1],
    "algorithm":["SAMME", "SAMME.R"],
    "n_estimators":[5, 20, 50]
  }

  grid = GridSearchCV(AdaBoostClassifier(), param_grid, cv=5, scoring = 'neg_log_loss', error_score=0.0)
  grid.fit(x_train, y_train)
  print(grid.best_score_)

  return grid

def grid_search_lda(x_train, y_train):

  param_grid = {
    "solver": ['svd', 'lsqr', 'eigen'],
    # "store_covariance": ['True', 'False'],
    # "tol": [0.0001, 0.001, 0.00001]
  }

  grid = GridSearchCV(LinearDiscriminantAnalysis(), param_grid, cv=5, scoring = 'neg_log_loss', error_score=0.0)
  grid.fit(x_train, y_train)
  print(grid.best_params_)

  return grid

def grid_search_logreg(x_train, y_train):

  # solver_options = ['newton-cg', 'lbfgs', 'liblinear', 'sag', 'saga'],
  class_weight_options = [None, 'balanced', {0:0.1, 1:0.9}, {0:0.01, 1:0.99}, {0:0.001, 1:0.999}],
  max_iter_options = [10000]

  param_grid = dict(class_weight = class_weight_options, max_iter= max_iter_options)

  grid = GridSearchCV(LogisticRegression(), param_grid, cv=5, scoring = 'neg_log_loss', error_score=0.0)
  grid.fit(x_train, y_train)
  print(grid.best_params_)

  return grid

def grid_search_xgb(x_train, y_train):

  # A parameter grid for XGBoost
  params = {
      'min_child_weight': [5], #[1, 5, 10]
      # 'gamma': [0.5, 1, 1.5, 2, 5],
      # 'subsample': [0.6, 0.8, 1.0],
      # 'colsample_bytree': [0.6, 0.8, 1.0],
      'max_depth': [5] #[3, 4, 5]
  }
  xgb = XGBClassifier(learning_rate=0.02, n_estimators=600, objective='binary:logistic',
                      silent=True, nthread=1)

  folds = 5
  # param_comb = 5

  skf = StratifiedKFold(n_splits=folds, shuffle=True, random_state=1001)

  # random_search = RandomizedSearchCV(xgb, param_distributions=params, n_iter=param_comb, scoring='neg_log_loss', n_jobs=4,
  #                                    cv=skf.split(x_train, y_train), verbose=3, random_state=1001)

  grid = GridSearchCV(estimator=xgb, param_grid=params, scoring='neg_log_loss', n_jobs=4, cv=skf.split(x_train, y_train), verbose=3)

  # print('\n Best hyperparameters:')
  # print(random_search.best_params_)

  # grid = GridSearchCV(XGBClassifier(use_label_encoder=False), param_grid, cv=5, scoring = 'neg_log_loss', error_score=0.0)
  grid_mdl = grid.fit(x_train, y_train)
  print(grid_mdl.best_score_)
  print(grid_mdl.best_params_)

  return grid

def grid_search_lgbm(x_train, y_train):

    # initiate a model
    params = {
        'application': 'binary',  # for binary classification
        #     'num_class' : 1, # used for multi-classes
        'boosting': 'gbdt',  # traditional gradient boosting decision tree
        'num_iterations': 100,
        'learning_rate': 0.01,
        'num_leaves': 16,
        'device': 'cpu',  # you can use GPU to achieve faster learning
        'max_depth': 10,  # <0 means no limit
        'max_bin': 510,  # Small number of bins may reduce training accuracy but can deal with over-fitting
        'lambda_l1': 5,  # L1 regularization
        'lambda_l2': 10,  # L2 regularization
        'metric': 'binary_error',
        'subsample_for_bin': 200,  # number of samples for constructing bins
        'subsample': 1,  # subsample ratio of the training instance
        'colsample_bytree': 0.65,  # subsample ratio of columns when constructing the tree
        'min_split_gain': 0.5,  # minimum loss reduction required to make further partition on a leaf node of the tree
        'min_child_weight': 1,  # minimum sum of instance weight (hessian) needed in a leaf
        'min_child_samples': 5,  # minimum number of data needed in a leaf

    }

    # Initiate classifier to use
    mdl = lgb.LGBMClassifier(boosting_type='gbdt',
                             objective='binary',
                             n_jobs=5,
                             silent=True,
                             max_depth=params['max_depth'],
                             max_bin=params['max_bin'],
                             subsample_for_bin=params['subsample_for_bin'],
                             subsample=params['subsample'],
                             min_split_gain=params['min_split_gain'],
                             min_child_weight=params['min_child_weight'],
                             min_child_samples=params['min_child_samples'])

    # grid search
    gridParams = {
        # 'learning_rate': [0.005, 0.01],
        'n_estimators': [50], #'n_estimators': [10, 20, 24, 50]
        'num_leaves': [16],  # large num_leaves helps improve accuracy but might lead to over-fitting
        'boosting_type': ['gbdt'], # for better accuracy -> try dart 'boosting_type': ['gbdt', 'dart']
        'objective': ['binary'],
        'max_bin': [50],  # large max_bin helps improve accuracy but might slow down training progress 'max_bin': [15, 25, 50]
        'random_state': [500],
        # 'colsample_bytree' : [0.64, 0.65, 0.66],
        'subsample': [0.7],
        'reg_alpha': [1],
        'reg_lambda': [1],
        'max_depth': [12] # 'max_depth': [3, 6, 12, 24]
    }

    grid = GridSearchCV(mdl, gridParams, verbose=1, cv=4, n_jobs=-1, scoring='neg_log_loss')
    # Run the grid
    grid.fit(x_train, y_train)

    # Print the best parameters found
    print(grid.best_params_)
    print(grid.best_score_)

    return grid


df = get_data('qmjhl', False)
df = deal_with_columns(df, False)
X_train, y_train, X_test, y_test, train, test = setup_train_val_test_datasets(df, '2019-07-01')
playerframe = X_test[['player_id']].copy()
X_train = X_train.drop(['player_id', 'is_rebound', 'rebound_angle_change', 'hold_year'], axis=1)
X_test = X_test.drop(['player_id', 'is_rebound', 'rebound_angle_change', 'hold_year'], axis=1)
test = test.drop(['player_id', 'is_rebound', 'rebound_angle_change', 'hold_year'], axis=1)
X_train.rename(columns={'player_(15.0, 16.0]':'player_15.0_16.0', 'player_(16.01, 17.0]': 'player_16.01_17.0',
              'player_(17.01, 18.0]':'player_17.01_18.0', 'player_(18.01, 19.0]':'player_18.01_19.0',
              'player_(19.01, 20.0]':'player_19.01_20.0', 'goalie_(0.0, 5.0]':'goalie_0.0_5.0',
              'goalie_(15.0, 16.0]':'goalie_15.0_16.0', 'goalie_(16.01, 17.0]':'goalie_16.01_17.0',
              'goalie_(17.01, 18.0]':'goalie_17.01_18.0', 'goalie_(18.01, 19.0]':'goalie_18.01_19.0',
              'goalie_(19.01, 20.0]':'goalie_19.01_20.0', 'goalie_(20.01, 21.0]':'goalie_20.01_21.0'}, inplace=True)
X_test.rename(columns={'player_(15.0, 16.0]':'player_15.0_16.0', 'player_(16.01, 17.0]': 'player_16.01_17.0',
              'player_(17.01, 18.0]':'player_17.01_18.0', 'player_(18.01, 19.0]':'player_18.01_19.0',
              'player_(19.01, 20.0]':'player_19.01_20.0', 'goalie_(0.0, 5.0]':'goalie_0.0_5.0',
              'goalie_(15.0, 16.0]':'goalie_15.0_16.0', 'goalie_(16.01, 17.0]':'goalie_16.01_17.0',
              'goalie_(17.01, 18.0]':'goalie_17.01_18.0', 'goalie_(18.01, 19.0]':'goalie_18.01_19.0',
              'goalie_(19.01, 20.0]':'goalie_19.01_20.0', 'goalie_(20.01, 21.0]':'goalie_20.01_21.0'}, inplace=True)

# Transform shot distance and angle net seen
columns_to_transform = ['shot_distance', 'angle_net_seen']
X_train = monotone_transform(X_train, columns_to_transform)
X_test = monotone_transform(X_test, columns_to_transform)
test = monotone_transform(test, columns_to_transform)
# X_train, X_test, scaler = new_features_and_normalize(X_train, X_test,
#                                                      ['shot_distance', 'angle_net_seen',
#                                                       #'rebound_angle_change'
#                                                       ])

# # print(X_test.shape)
# # print(y_train.shape)
# # print(X_train.shape)
# # print(X_train.head())
# # get_baseline_model_and_test_scores_for_viz(X_train, y_train, X_test, y_test, test)
#
# # gradient boost
# grid_mdl = grid_search_gradient_boost(X_train, y_train)
#
# # ada boost
# # grid_mdl = grid_search_ada_boost(X_train, y_train)
#
# # lda
# # mdl = LinearDiscriminantAnalysis()
#
# logreg
# grid_mdl = grid_search_logreg(X_train, y_train)
# mdl = grid_mdl.best_estimator_
# get_baseline_model_and_test_scores_for_viz(X_train, y_train, X_test, y_test, test)
#
# # xgboost
# grid_mdl = grid_search_xgb(X_train, y_train)
# # print(grid_mdl)

#lgbm
# grid_mdl = grid_search_lgbm(X_train, y_train)
#
# # Find and score best model
# mdl = grid_mdl.best_estimator_

# lgbm model
# initiate a model
params = {
    'application': 'binary',  # for binary classification
    #     'num_class' : 1, # used for multi-classes
    'boosting': 'gbdt',  # traditional gradient boosting decision tree
    'num_iterations': 100,
    'learning_rate': 0.01,
    'num_leaves': 16,
    'device': 'cpu',  # you can use GPU to achieve faster learning
    'max_depth': 12,  # <0 means no limit
    'max_bin': 50,  # Small number of bins may reduce training accuracy but can deal with over-fitting
    'lambda_l1': 5,  # L1 regularization
    'lambda_l2': 10,  # L2 regularization
    'metric': 'binary_error',
    'subsample_for_bin': 200,  # number of samples for constructing bins
    'subsample': 0.7,  # subsample ratio of the training instance
    'colsample_bytree': 0.65,  # subsample ratio of columns when constructing the tree
    'min_split_gain': 0.5,  # minimum loss reduction required to make further partition on a leaf node of the tree
    'min_child_weight': 1,  # minimum sum of instance weight (hessian) needed in a leaf
    'min_child_samples': 5,  # minimum number of data needed in a leaf

}

# # Initiate classifier to use
# mdl = lgb.LGBMClassifier(boosting_type='gbdt',
#                          objective='binary',
#                          n_estimators=50,
#                          num_leaves=16,
#                          reg_alpha=1,
#                          reg_lambda=1,
#                          n_jobs=5,
#                          silent=True,
#                          max_depth=params['max_depth'],
#                          max_bin=params['max_bin'],
#                          subsample_for_bin=params['subsample_for_bin'],
#                          subsample=params['subsample'],
#                          min_split_gain=params['min_split_gain'],
#                          min_child_weight=params['min_child_weight'],
#                          min_child_samples=params['min_child_samples'])

# Initiate classifier to use
mdl =XGBClassifier(learning_rate=0.02,
                   n_estimators=600,
                   min_child_weight=5,
                   max_depth=5,
                   objective='binary:logistic',
                   silent=True,
                   nthread=1)

# get_model_and_test_scores(mdl, X_train, y_train, X_test, y_test)
#
# # #
# initialize model
mdl.fit(X_train, y_train)

# run preds on entire dataset
X_train, y_train, X_test, y_test, train, test = setup_train_val_test_datasets(df, '2014-07-01')

X_train = X_train.drop(['player_id', 'is_rebound', 'rebound_angle_change', 'hold_year'], axis=1)
X_test = X_test.drop(['player_id', 'is_rebound', 'rebound_angle_change', 'hold_year'], axis=1)
test = test.drop(['player_id', 'is_rebound', 'rebound_angle_change', 'hold_year'], axis=1)
X_train.rename(columns={'player_(15.0, 16.0]':'player_15.0_16.0', 'player_(16.01, 17.0]': 'player_16.01_17.0',
              'player_(17.01, 18.0]':'player_17.01_18.0', 'player_(18.01, 19.0]':'player_18.01_19.0',
              'player_(19.01, 20.0]':'player_19.01_20.0', 'goalie_(0.0, 5.0]':'goalie_0.0_5.0',
              'goalie_(15.0, 16.0]':'goalie_15.0_16.0', 'goalie_(16.01, 17.0]':'goalie_16.01_17.0',
              'goalie_(17.01, 18.0]':'goalie_17.01_18.0', 'goalie_(18.01, 19.0]':'goalie_18.01_19.0',
              'goalie_(19.01, 20.0]':'goalie_19.01_20.0', 'goalie_(20.01, 21.0]':'goalie_20.01_21.0'}, inplace=True)
X_test.rename(columns={'player_(15.0, 16.0]':'player_15.0_16.0', 'player_(16.01, 17.0]': 'player_16.01_17.0',
              'player_(17.01, 18.0]':'player_17.01_18.0', 'player_(18.01, 19.0]':'player_18.01_19.0',
              'player_(19.01, 20.0]':'player_19.01_20.0', 'goalie_(0.0, 5.0]':'goalie_0.0_5.0',
              'goalie_(15.0, 16.0]':'goalie_15.0_16.0', 'goalie_(16.01, 17.0]':'goalie_16.01_17.0',
              'goalie_(17.01, 18.0]':'goalie_17.01_18.0', 'goalie_(18.01, 19.0]':'goalie_18.01_19.0',
              'goalie_(19.01, 20.0]':'goalie_19.01_20.0', 'goalie_(20.01, 21.0]':'goalie_20.01_21.0'}, inplace=True)

# get predictions
preds = mdl.predict_proba(X_test)

# create a prediction dataframe
df_test = pd.DataFrame()
df_test['xG'] = preds[:,1]

# merge with test df
preds_all = pd.merge(df, df_test, left_index=True, right_index=True)

# # send to csv
# preds_all.to_csv('qmjhl_data/qmjhl_xg.csv', index=False)

ipdb.set_trace()

# # merge predictions with playerframe
# preds = pd.merge(playerframe, df_test, left_index=True, right_index=True)
# ipdb.set_trace()
#
# # groupby id
# grouped = preds.groupby(['player_id']).sum()
# print(grouped.head(10))
#
# # get ohl and qmjhl player names
# nameplates_ohl = pd.read_csv('ohl_data/ohl_nameplate.csv')
# nameplates_qmjhl = pd.read_csv('qmjhl_data/qmjhl_nameplate.csv')
# nameplates = pd.concat([nameplates_qmjhl, nameplates_ohl], ignore_index=True, sort=False)
#
# # merge player id and names
# names = nameplates[['name', 'player_id']].copy()
# merged = pd.merge(left=grouped, right=names, how='left', left_on='player_id', right_on='player_id')
# merged = merged.sort_values(by='xG', ascending=False)
# merged['league'] = np.where((merged['player_id'] > 10000), 'QMJHL', 'OHL')
# print(merged.head(10))
#
# # groupby xG averages
# means = merged.groupby(['league'])['xG'].mean()
# print(means)
#
# # Walk forward validation
# years = [2016, 2017, 2018, 2019]
#
# # Define a result table as a DataFrame
# result_table = pd.DataFrame(columns=['fpr','tpr','auc', 'log_loss'])
#
# for year in years:
#     X_train, y_train, X_test, y_test, train, test = setup_train_val_test_datasets_walk_forward(df, year)
#     X_train.rename(columns={'player_(15.0, 16.0]': 'player_15.0_16.0', 'player_(16.01, 17.0]': 'player_16.01_17.0',
#                             'player_(17.01, 18.0]': 'player_17.01_18.0', 'player_(18.01, 19.0]': 'player_18.01_19.0',
#                             'player_(19.01, 20.0]': 'player_19.01_20.0', 'goalie_(0.0, 5.0]': 'goalie_0.0_5.0',
#                             'goalie_(15.0, 16.0]': 'goalie_15.0_16.0', 'goalie_(16.01, 17.0]': 'goalie_16.01_17.0',
#                             'goalie_(17.01, 18.0]': 'goalie_17.01_18.0', 'goalie_(18.01, 19.0]': 'goalie_18.01_19.0',
#                             'goalie_(19.01, 20.0]': 'goalie_19.01_20.0', 'goalie_(20.01, 21.0]': 'goalie_20.01_21.0'},
#                    inplace=True)
#     X_test.rename(columns={'player_(15.0, 16.0]': 'player_15.0_16.0', 'player_(16.01, 17.0]': 'player_16.01_17.0',
#                            'player_(17.01, 18.0]': 'player_17.01_18.0', 'player_(18.01, 19.0]': 'player_18.01_19.0',
#                            'player_(19.01, 20.0]': 'player_19.01_20.0', 'goalie_(0.0, 5.0]': 'goalie_0.0_5.0',
#                            'goalie_(15.0, 16.0]': 'goalie_15.0_16.0', 'goalie_(16.01, 17.0]': 'goalie_16.01_17.0',
#                            'goalie_(17.01, 18.0]': 'goalie_17.01_18.0', 'goalie_(18.01, 19.0]': 'goalie_18.01_19.0',
#                            'goalie_(19.01, 20.0]': 'goalie_19.01_20.0', 'goalie_(20.01, 21.0]': 'goalie_20.01_21.0'},
#                   inplace=True)
#     X_train = X_train.drop(['player_id', 'hold_year','is_rebound', 'rebound_angle_change'], axis=1)
#     X_test = X_test.drop(['player_id', 'hold_year', 'is_rebound', 'rebound_angle_change'], axis=1)
#     test = test.drop(['player_id', 'hold_year', 'is_rebound', 'rebound_angle_change'], axis=1)
#     ipdb.set_trace()
#     # initialize model
#     mdl.fit(X_train, y_train)
#
#     yproba = mdl.predict_proba(X_test)[::, 1]
#     fpr, tpr, _ = roc_curve(y_test, yproba)
#     auc = roc_auc_score(y_test, yproba)
#     # ipdb.set_trace()
#     log_loss_score = log_loss(y_test, yproba)
#     precision_scores = precision_score(y_test, mdl.predict(X_test))
#     accuracy_scores = accuracy_score(y_test, mdl.predict(X_test))
#
#     result_table = result_table.append({'test year': year,
#                                         'fpr': fpr,
#                                         'tpr': tpr,
#                                         'auc': auc,
#                                         'precision': precision_scores,
#                                         'accuracy': accuracy_scores,
#                                         'log_loss': log_loss_score}, ignore_index=True)
# print(result_table)
# ipdb.set_trace()
#
# # Pycaret model testing
# # # Do not split train and test for pycaret
# # df = df[[FEATURE_COLS] + ['event']]
# # # Set up data for pycaret
# # clf = setup(df, target='event', session_id=1)
# # # Compare performance of various machine learning models
# # print(compare_models(sort='AUC'))

# Clustering Code
# non_binary_columns = ['x_location', 'y_location']
# X_train, X_val, scaler = new_features_and_normalize(X_train, X_test, non_binary_columns)
# clK, Centroids, distances_from_clusters, mdk_k_means = k_means_clustering(X_train, 3)
# get_baseline_model_and_test_scores_for_viz(X_train, y_train, X_test, y_test, test)

# # Create elbow plot
# number_of_clusters = list(range(1, 10))
# average_distance = []
# ipdb.set_trace()
# for k in number_of_clusters:  # Run algorithm for 1 to 10 clusters
#     # Redefine the number of clusters in the model, and for the model
#
#     clK, Centroids, distances_from_clusters = k_means_clustering(X_train, k)
#
#     # Get y values (avg distance) for the plot
#
#     point_to_cluter_distance = np.min(distances_from_clusters, axis=1)
#     average_distance.extend([(point_to_cluter_distance).mean()])
#
#     # -------------------

# plt.figure(figsize=(4, 6))
# plt.scatter(number_of_clusters, average_distance)
# plt.ylabel('Sum of centroid to point euclidien distances')
# plt.xlabel('Number of clusters')
# plt.title('KNN elbow plot')
# plt.show()


# Using clusters as features
# clK, Centroids, distances_from_clusters, mdk_k_means = k_means_clustering(X_train, 3)
# X_train_scaled_clustered = cluster_regression_1_hot_encoding_model(X_train.copy(), mdk_k_means, ['x_location',
#                                                                                                  'y_location'])
# X_val_scaled_clustered = cluster_regression_1_hot_encoding_model(X_val.copy(), mdk_k_means, ['x_location',
#                                                                                             'y_location'])
#
# X_train = X_train_scaled_clustered.drop(columns=['x_location', 'y_location'])
# X_test  = X_val_scaled_clustered.drop(columns=['x_location', 'y_location'])
# get_baseline_model_and_test_scores_for_viz(X_train, y_train, X_test, y_test, test)
