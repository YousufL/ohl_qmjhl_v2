from constants import FEATURE_COLS
import pandas as pd
import ipdb
import numpy as np
from xgboost import XGBClassifier
import warnings
warnings.filterwarnings('ignore')

def get_data(league, combine):
    '''
    Providing the option to train the models as ohl/qmjhl or combining the data into
    one main df
    '''

    if league == 'ohl':
        df = pd.read_csv('prelim_cleaned_data/test_model_ohl.csv')

    if league == 'qmjhl':
        df = pd.read_csv('prelim_cleaned_data/test_model_qmjhl.csv')
        df = df[(pd.to_datetime(df['new_date'].astype(str)) <= '2020-07-01')].reset_index().drop(columns=['index'])

    if combine == True:
        df = pd.read_csv('prelim_cleaned_data/test_model_ohl.csv')
        df = df[FEATURE_COLS + ['event', 'new_date']]
        df['League'] = 'ohl'

        df_qmjhl = pd.read_csv('prelim_cleaned_data/test_model_qmjhl.csv')
        df_qmjhl = df_qmjhl[FEATURE_COLS + ['event', 'new_date']]
        df_qmjhl = df_qmjhl[(pd.to_datetime(df_qmjhl['new_date'].astype(str)) <= '2020-07-01')].reset_index().drop(columns=['index'])
        df_qmjhl['League'] = 'qmjhl'

        df = pd.concat([df, df_qmjhl], axis=0)

    return df

def deal_with_columns(df, drop_first):
    '''
    Makes sure that the same columns exist. In the future, for example,if there is no
    right-shooting goalie in a game the 4 dummified colmns that deal with that case wont show up,
    so we will have a column mismatch. To deal with this we set all columns that arent in the df
    that are in FEATURE_COLS and set all values to 0

    The second part gives the option to drop the first category from the categorical variable bins
    '''
    for item in range(len(list(set(FEATURE_COLS).difference(df.columns)))):
        df[item] = 0

    if drop_first == True:
        df = df.drop(columns=drop_first)

    return df

def setup_train_val_test_datasets(df, split_date):
    '''
    Splitting the data into train and test sets. We want to hold out the 2019-2020 season
    as unseen data to be able to compare models more effectively
    '''

    train = df[pd.to_datetime(df['new_date'].astype(str)) <= split_date].reset_index().drop(columns=['index'])
    test = df[pd.to_datetime(df['new_date'].astype(str)) > split_date].reset_index().drop(columns=['index'])

    X_train = train[FEATURE_COLS]
    # X_train['league'] = train['league']
    y_train = train['event']

    X_test = test[FEATURE_COLS]
    # X_test['league'] = test['league']
    y_test = test['event']

    return X_train, y_train, X_test, y_test, train, test

def monotone_transform(X, columns_to_transform):
    X_new = X.copy()
    for column in columns_to_transform:
        X_new[column] = np.sqrt(X_new[column])
    return X_new

def prepare_df_qmjhl(X_train, X_test, test):
    X_train = X_train.drop(['player_id', 'is_rebound', 'rebound_angle_change', 'hold_year'], axis=1)
    X_test = X_test.drop(['player_id', 'is_rebound', 'rebound_angle_change', 'hold_year'], axis=1)
    test = test.drop(['player_id', 'is_rebound', 'rebound_angle_change', 'hold_year'], axis=1)
    X_train.rename(columns={'player_(15.0, 16.0]': 'player_15.0_16.0', 'player_(16.01, 17.0]': 'player_16.01_17.0',
                            'player_(17.01, 18.0]': 'player_17.01_18.0', 'player_(18.01, 19.0]': 'player_18.01_19.0',
                            'player_(19.01, 20.0]': 'player_19.01_20.0', 'goalie_(0.0, 5.0]': 'goalie_0.0_5.0',
                            'goalie_(15.0, 16.0]': 'goalie_15.0_16.0', 'goalie_(16.01, 17.0]': 'goalie_16.01_17.0',
                            'goalie_(17.01, 18.0]': 'goalie_17.01_18.0', 'goalie_(18.01, 19.0]': 'goalie_18.01_19.0',
                            'goalie_(19.01, 20.0]': 'goalie_19.01_20.0', 'goalie_(20.01, 21.0]': 'goalie_20.01_21.0'},
                   inplace=True)
    X_test.rename(columns={'player_(15.0, 16.0]': 'player_15.0_16.0', 'player_(16.01, 17.0]': 'player_16.01_17.0',
                           'player_(17.01, 18.0]': 'player_17.01_18.0', 'player_(18.01, 19.0]': 'player_18.01_19.0',
                           'player_(19.01, 20.0]': 'player_19.01_20.0', 'goalie_(0.0, 5.0]': 'goalie_0.0_5.0',
                           'goalie_(15.0, 16.0]': 'goalie_15.0_16.0', 'goalie_(16.01, 17.0]': 'goalie_16.01_17.0',
                           'goalie_(17.01, 18.0]': 'goalie_17.01_18.0', 'goalie_(18.01, 19.0]': 'goalie_18.01_19.0',
                           'goalie_(19.01, 20.0]': 'goalie_19.01_20.0', 'goalie_(20.01, 21.0]': 'goalie_20.01_21.0'},
                  inplace=True)

    # Transform shot distance and angle net seen
    columns_to_transform = ['shot_distance', 'angle_net_seen']
    X_train = monotone_transform(X_train, columns_to_transform)
    X_test = monotone_transform(X_test, columns_to_transform)
    test = monotone_transform(test, columns_to_transform)

    return X_train, X_test, test

df = get_data('qmjhl', False)
df = deal_with_columns(df, False)
X_train, y_train, X_test, y_test, train, test = setup_train_val_test_datasets(df, '2019-07-01')
X_train, X_test, test = prepare_df_qmjhl(X_train, X_test, test)


# Initiate classifier to use
mdl =XGBClassifier(learning_rate=0.02,
                   n_estimators=600,
                   min_child_weight=5,
                   max_depth=5,
                   objective='binary:logistic',
                   silent=True,
                   nthread=1)

# initialize model
mdl.fit(X_train, y_train)

# run preds on entire dataset
X_train, y_train, X_test, y_test, train, test = setup_train_val_test_datasets(df, '2014-07-01')
X_train, X_test, test = prepare_df_qmjhl(X_train, X_test, test)

# get predictions
preds = mdl.predict_proba(X_test)

# create a prediction dataframe
df_test = pd.DataFrame()
df_test['xG'] = preds[:,1]

# merge with test df
preds_all = pd.merge(df, df_test, left_index=True, right_index=True)

# send to csv
preds_all.to_csv('qmjhl_data/qmjhl_xg.csv', index=False)

ipdb.set_trace()