import pickle
import pandas as pd
from xgboost import XGBClassifier

from modelling.constants import FEATURE_COLS, RENAME_DICT, OHL_DROP, QMJHL_DROP, COLUMNS_TO_TRANSFORM
import numpy as np

class Model:
    def __init__(self, df, train, league):
        self.df = df
        self.train = train
        self.league = league

    def load_model_data(self):

        df_ = self.df.copy()
        df_ = df_[FEATURE_COLS + ['event', 'new_date']]

        return df_

    @staticmethod
    def deal_with_columns(df, drop_first):

        for item in range(len(list(set(FEATURE_COLS).difference(df.columns)))):
            df[item] = 0

        if drop_first == True:
            df = df.drop(columns=drop_first)

        return df

    @staticmethod
    def get_x_y(df):

        X = df[FEATURE_COLS]
        y = df['event']

        return X,y

    @staticmethod
    def monotone_transform(X, columns_to_transform):

        X_new = X.copy()
        for column in columns_to_transform:
            X_new[column] = np.sqrt(X_new[column])
        return X_new

    def prepare_df(self, X):

        if self.league == 'qmjhl':
            X = X.drop(QMJHL_DROP, axis=1)

        if self.league =='ohl':
            X = X.drop(OHL_DROP, axis=1)

        X.rename(columns=RENAME_DICT,
                       inplace=True)

        # Transform shot distance and angle net seen
        X = self.monotone_transform(X, COLUMNS_TO_TRANSFORM)

        return X


    @staticmethod
    def fit_model(X, y):

        model = XGBClassifier(learning_rate=0.02,
                            n_estimators=600,
                            min_child_weight=5,
                            max_depth=5,
                            objective='binary:logistic',
                            silent=True,
                            nthread=1)

        model.fit(X, y)

        return model

    @staticmethod
    def predict_xg(model, X):

        preds = model.predict_proba(X)
        df_test = pd.DataFrame()
        df_test['xG'] = preds[:, 1]

        return df_test

    def merge_xg_results(self,df_test):

        preds_all = pd.merge(self.df, df_test, left_index=True, right_index=True)

        return preds_all

    def __call__(self):

        df = self.load_model_data()
        df = self.deal_with_columns(df, False)
        X,y = self.get_x_y(df)
        X = self.prepare_df(X)

        if self.train == True:
            model = self.fit_model(X, y)
            pickle.dump(model, open(f"{self.league}_model.pickle.dat", "wb"))

        if self.train == False:
            model = pickle.load(open(f"{self.league}_model.pickle.dat", "rb"))

        df_test = self.predict_xg(model, X)
        df = self.merge_xg_results(df_test)

        return df