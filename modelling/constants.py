# Will add game_state as variable after game_state script is finalized
import pandas as pd

SHOTS = ['game_id', 'event', 'goalie_id', 'goalie_team_id', 'player_id', 'player_team_id',
         'quality', 's_', 'shot_quality_description', 'shot_type', 'shot_type_description',
         'time', 'time_formatted', 'x_location', 'y_location', 'game_ID', 'home', 'period', 'goal_type', 'home_team']

NET_SIZE = 6 * (300 / 85)
POST1_COORD = 150 - (NET_SIZE / 2)
POST2_COORD = 150 + (NET_SIZE / 2)

SCORE_STATE_DICT = {
    0: "Even",
    1: "Up_One",
    2: "Up_Two",
    3: "Up_Three",
    4: "Up_Four",
    -1: "Down_One",
    -2: "Down_Two",
    -3: "Down_Three",
    -4: "Down_Four",
}

FIRST = ['Sep', 'Oct', 'Nov', 'Dec']

MONTH_DICT = {
    "Jan": '01',
    "Feb": '02',
    "Mar": '03',
    "Apr": '04',
    "May": '05',
    "Jun": '06',
    "Jul": '07',
    "Aug": '08',
    "Sep": '09',
    "Oct": '10',
    "Nov": '11',
    "Dec": '12',
}

BINS = pd.IntervalIndex.from_tuples([(0, 5), (15, 16), (16.01, 17),
                                     (17.01, 18), (18.01, 19),
                                     (19.01, 20), (20.01, 21)])

FEATURE_COLS = [
    # 'x_location', 'y_location',
    # # 'shot_distance', 'angle_net_seen', 'shot_angle',
    'player_id', 'hold_year',
    'shot_distance', 'angle_net_seen',
    'is_rebound', 'rebound_angle_change',
    '1.0', '2.0', '3.0', '4.0',
    #1.0, 2.0, 3.0, 4.0,
    # "LLL", "LLR", "LRL", "LRR", "RLL", "RLR", "RRL", "RRR",
    # "OOO",
    'player_(15.0, 16.0]', 'player_(16.01, 17.0]', 'player_(17.01, 18.0]', 'player_(18.01, 19.0]',
    'player_(19.01, 20.0]',
    'goalie_(0.0, 5.0]', 'goalie_(15.0, 16.0]', 'goalie_(16.01, 17.0]', 'goalie_(17.01, 18.0]', 'goalie_(18.01, 19.0]',
    'goalie_(19.01, 20.0]', 'goalie_(20.01, 21.0]',
    "Even", "Up_One", "Up_Two", "Up_Three", "Up_Four", "Down_One", "Down_Two", "Down_Three", "Down_Four"
]
# "EV","SH","PP"

drop_first = ['1.0', 'LLL', 'Even']


RENAME_DICT = {'player_(15.0, 16.0]': 'player_15.0_16.0', 'player_(16.01, 17.0]': 'player_16.01_17.0',
                                'player_(17.01, 18.0]': 'player_17.01_18.0',
                                'player_(18.01, 19.0]': 'player_18.01_19.0',
                                'player_(19.01, 20.0]': 'player_19.01_20.0', 'goalie_(0.0, 5.0]': 'goalie_0.0_5.0',
                                'goalie_(15.0, 16.0]': 'goalie_15.0_16.0', 'goalie_(16.01, 17.0]': 'goalie_16.01_17.0',
                                'goalie_(17.01, 18.0]': 'goalie_17.01_18.0',
                                'goalie_(18.01, 19.0]': 'goalie_18.01_19.0',
                                'goalie_(19.01, 20.0]': 'goalie_19.01_20.0',
                                'goalie_(20.01, 21.0]': 'goalie_20.01_21.0'}

OHL_DROP = ['player_id', 'hold_year']
QMJHL_DROP = ['player_id', 'is_rebound', 'rebound_angle_change', 'hold_year']

COLUMNS_TO_TRANSFORM = ['shot_distance', 'angle_net_seen']