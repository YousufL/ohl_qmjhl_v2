from sqlalchemy import create_engine
import sys
sys.path.extend(['frontend/tables/goalies/data_prep'])
sys.path.extend(['frontend/tables/players'])
sys.path.extend(['frontend/tables/teams/data_prep'])

from goalie_table_functions import GoalieTable
from player_table_functions import PlayerTable
from team_table_functions import TeamTable


engine = create_engine('postgresql://super:super123@localhost:9999/postgres')

teams = TeamTable()()
players = PlayerTable()()
goalies = GoalieTable()()

teams.to_sql('teams', con=engine, if_exists='replace')
players.to_sql('players', con=engine, if_exists='replace')
goalies.to_sql('goalies', con=engine, if_exists='replace')