import sys
from sqlalchemy import create_engine

from modelling.model import Model
sys.path.extend(['scrapers/historic_data'])
sys.path.extend(['data_prep'])
engine = create_engine('postgresql://super:super123@localhost:9999/postgres')
import pandas as pd
from data_wrangler import WrangleData
from get_raw_data_functions import Scraper

for league in ['qmjhl','ohl']:

    df, teamplate, nameplate, stats = Scraper(league = league)()

    if len(nameplate) > 0:
        if ['level_0'] in nameplate.columns:
            nameplate = nameplate.drop(columns  = ['level_0'])
        nameplate.to_sql(f'nameplate_{league}', con=engine, if_exists='append', schema = league)
    query_1 = f"SELECT * FROM {league}.nameplate_{league};"
    nameplate = pd.read_sql(query_1, engine)

    df, goalie_gp = WrangleData(league=league, df=df, nameplate=nameplate, teamplate=teamplate)()
    df = Model(df = df, train=True, league=league)()

    df.to_sql(f'raw_{league}', con=engine, if_exists='replace', schema = league)
    teamplate.to_sql(f'teamplate_{league}', con=engine, if_exists='replace', schema = league)
    stats.to_sql(f'stats_{league}', con=engine, if_exists='replace', schema = league)
    goalie_gp.to_sql(f'goalie_gp_{league}', con=engine, if_exists='replace', schema=league)